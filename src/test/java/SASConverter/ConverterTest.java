/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified extensively by Alexander Mueller in 2019.
*/

package SASConverter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class ConverterTest
{

    private String Read_file(String input_file) throws IOException
    {
        return Files.readString(Paths.get(input_file));
    }

    private void removeSourceInformation(JSONArray input) throws JSONException
    {
        for(int x=0; x < input.length(); x++)
        {
            input.getJSONObject(x).remove("sourceInformation");
            if(input.getJSONObject(x).has("command"))
            {
                if(input.getJSONObject(x).getString("command").equals("LoopOverList"))
                {
                    removeSourceInformation(input.getJSONObject(x).getJSONArray("commands"));
                }
                if(input.getJSONObject(x).getString("command").equals("IfRows"))
                {
                    removeSourceInformation(input.getJSONObject(x).getJSONArray("thenCommands"));
                    if(input.getJSONObject(x).has("elseCommands"))
                    {
                        removeSourceInformation(input.getJSONObject(x).getJSONArray("elseCommands"));
                    }
                }
            }
        }
    }

    private JSONArray Text_To_JSON(String input_file) throws JSONException, IOException {

        String input_file_string = Read_file(input_file);

        // Convert to JSON
        return new JSONArray(input_file_string);

    }

    private JSONArray Test_Command(String input_file, HashMap<String, List<String>> file_variables, HashMap<String, List<String>> dataset_variables) throws JSONException, IOException, NoSuchAlgorithmException {

            // Read in SAS file
            //String input_file_string = Read_file(input_file);
            //System.out.println(input_file_string);

            // Instantiate converter object and send in SAS
            Converter converter = new Converter();
            String all_text = converter.convertSAS(Read_file(input_file), file_variables, dataset_variables).toString();

            // Turn String to JSON
            JSONObject obj = new JSONObject(all_text);

            // Obtain only "Commands" object
            JSONArray commands = obj.getJSONArray("commands");
            //System.out.println(commands.toString(4));

            // Remove SourceInformation object
            removeSourceInformation(commands);

            return commands;
    }

    @Test
    public void test_AppendDatasets() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        HashMap<String, List<String>> file_variables = new HashMap<>();
        List<String> gss_regions = Arrays.asList("GSS_region_1.sas7bdat", "GSS_region_2.sas7bdat", "GSS_region_3.sas7bdat",
                                                "GSS_region_4.sas7bdat", "GSS_region_5.sas7bdat", "GSS_region_6.sas7bdat",
                                                "GSS_region_7.sas7bdat", "GSS_region_8.sas7bdat", "GSS_region_9.sas7bdat");
        for(String gss_region : gss_regions)
        {
            file_variables.put(gss_region, Arrays.asList("V1", "V2", "V3"));
        }
        String[] messages =
        {
            "Testing append originating from a SET command...",
            "Testing append originating from an APPEND procedure..."
        };
        dataset_variables.put("gss3", Arrays.asList("V1", "V2", "V3"));
        dataset_variables.put("gss2", Arrays.asList("V1", "V2", "V3"));
        runTestCase("AppendDatasets", 2, messages, file_variables, dataset_variables);
    }

    @Test
    public void test_attrib() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        String[] messages =
        {
            "Testing format, label, and TRANSCODE attributes with both single and multi variable statments..."
        };
        dataset_variables.put("gss2", Arrays.asList("age", "abany", "abc", "absingle", "divlaw", "divlawy"));
        runTestCase("attrib", 1, messages, new HashMap<>(), dataset_variables);
    }

    @Test
    public void test_Collapse() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        String[] messages =
        {
            "Testing collapse with default statistics and no by groups...",
            "Testing collapse with explicitly named statistics and no by groups...",
            // TODO: test case with no VAR statement, no explicitly named analysis variables, and multiple by groups
            "Testing collapse with named statistics, one by group, and explicit analysis variables...",
            "Testing collapse with by groups listed in class statement and default types...",
            "Testing collapse with class statement and types according to ways statement...",
            "Testing collapse with only overall aggregates (from TYPES statement with empty list)...",
            "Testing collapse with explicitly defined 1-way and 2-way aggregates...",
            "Testing collapse with a combination of TYPES and WAYS statements..."
        };
        dataset_variables.put("hogwarts", Arrays.asList("Firstname", "Lastname", "Role", "House", "Age", "Year", "Income"));
        runTestCase("Collapse", 8, messages, new HashMap<>(), dataset_variables);
    }

    @Test
    public void test_Comment() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing single line comment starting with * and ending with ; ...",
            "Testing single line comment starting with /* and ending with */ ...",
            "Testing multi line comment starting with * and ending with ; ...",
            "Testing multi line comment starting with /* and ending with */ ..."
        };
        runTestCase("Comment", 4, messages, new HashMap<>(), new HashMap<>());
    }

    @Test
    public void test_Compute() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        String[] messages =
        {
            "Testing compute with one parenthetical, one function call, and two arithmetic operations...",
            "Same as above, but testing a different function call...",
            "Testing compute base case with just arithmetic...",
            "Testing multi-layered sequence of function calls, parentheticals, and arithmetic operations..."
        };
        dataset_variables.put("gss2", Arrays.asList("age"));
        runTestCase("Compute", 4, messages, new HashMap<>(), dataset_variables);
    }

    @Test
    public void test_dataset_options() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        String[] messages =
        {
            "Testing lone DROP=, KEEP=, and RENAME= dataset options on the output...",
            "Testing multiple output datasets, each with their own dataset option...",
            "Testing multiple dataset options on the same output dataset...",
            "Testing the order in which drops, keeps, and renames are applied with statements vs. dataset options...",
            "Same as above, but a more rigorous test that includes both input and output dataset options..."
        };
        dataset_variables.put("hogwarts", Arrays.asList("Firstname", "Lastname", "Level", "School", "Grade", "Age", "Gender", "Score"));
        runTestCase("dataset_options", 5, messages, new HashMap<>(), dataset_variables);
    }

    @Test
    public void test_DropVariables() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> file_variables = new HashMap<>();
        String[] messages =
        {
            "Testing single variable deletion...",
            "Testing multi variable deletion...",
            "Testing variable range deletion..."
        };
        file_variables.put("C2M_36797_0001_DATA.sas7bdat", Arrays.asList("abany", "abc", "absingle", "age", "age_comp", "age_rec"));
        runTestCase("DropVariables", 3, messages, file_variables, new HashMap<>());
    }

    @Test
    public void test_IfRows() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        String[] messages =
        {
            "Testing simple IF/THEN/ELSE syntax..."
        };
        dataset_variables.put("gss2", Arrays.asList("y", "z"));
        runTestCase("IfRows", 1, messages, new HashMap<>(), dataset_variables);
    }

    @Test
    public void test_KeepCases() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        String[] messages =
        {
            "Testing the base cases for all SAS statements or dataset options that result in KeepCases...",
            "Testing more complicated WHERE expression logic, including operators specific to WHERE expressions...",
            "Testing a maximally convoluted scenario for reordering of computes, drops, keeps, renames, and wheres..."
        };
        dataset_variables.put("gss", Arrays.asList("x", "y", "z", "a", "b", "c", "d", "e", "f", "g"));
        dataset_variables.put("gss2", Arrays.asList("x", "y", "z", "a", "b", "c", "d", "e", "f", "g"));
        dataset_variables.put("case", Arrays.asList("name", "A", "B", "C", "D"));
        runTestCase("KeepCases", 3, messages, new HashMap<>(), dataset_variables);
    }

    @Test
    public void test_KeepVariables() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> file_variables = new HashMap<>();
        String[] messages =
        {
            "Testing single variable keep...",
            "Testing multi variable keep...",
            "Testing variable range keep..."
        };
        file_variables.put("C2M_36797_0001_DATA.sas7bdat", Arrays.asList("abany", "abc", "absingle", "age", "age_comp", "age_rec"));
        runTestCase("KeepVariables", 3, messages, file_variables, new HashMap<>());
    }

    // not testing this until discussion about loop expansion has come to a conclusion
    /*@Test
    public void test_LoopOverList() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing loop over variables..."
            // TODO: test case for loop over values
        };
        runTestCase("LoopOverList", 1, messages);
    }*/

    // not testing this until it's done
    /*@Test
    public void test_merge_modify_update() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        dataset_variables.put("master", Arrays.asList("ID", "Name", "Winnings", "Age"));
        dataset_variables.put("transaction", Arrays.asList("ID", "Name", "Winnings", "Reliability"));
        // TODO: add message string and call to runTestCase()
    }*/

    @Test
    public void test_MergeDatasets() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> file_variables = new HashMap<>();
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        String[] messages =
        {
            "Testing MERGE with a by group...",
            "Testing the different types of merging (e.g. Sequential vs. SASmatchMerge)..."
        };
        file_variables.put("gss_1st_half.sas7bdat", Arrays.asList("firsthalf"));
        file_variables.put("gss_2nd_half.sas7bdat", Arrays.asList("secondhalf"));
        dataset_variables.put("Mrgdat1", Arrays.asList("id1", "type", "subtype", "varx"));
        dataset_variables.put("Mrgdat2", Arrays.asList("id2", "type", "subtype", "vary", "id1"));
        dataset_variables.put("mrgdat11", Arrays.asList("id3", "type", "varx"));
        dataset_variables.put("mrgdat12", Arrays.asList("id4", "type", "vary"));
        runTestCase("MergeDatasets", 2, messages, file_variables, dataset_variables);
    }

    @Test
    public void test_SetMissingValues() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing single missing value...",
            "Testing multiple missing values..."
        };
        runTestCase("SetMissingValues", 2, messages, new HashMap<>(), new HashMap<>());
    }

    @Test
    public void test_Rename() throws JSONException, IOException, NoSuchAlgorithmException
    {
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        String[] messages =
        {
            "Testing single rename...",
            "Testing multiple renames..."
        };
        dataset_variables.put("gss2", Arrays.asList("age", "age_rec"));
        runTestCase("Rename", 2, messages, new HashMap<>(), dataset_variables);
    }

    // not testing this until there's something to test
    /*@Test
    public void test_SetValueLabels() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing the setting of labels for values..."
        };
        //runTestCase("SetValueLabels", 1, messages);
    }*/

    @Test
    public void test_SetDisplayFormat() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        String[] messages =
        {
            "Testing character and numeric display formats with multiple variables, including default formats...",
            "Testing format dissociation..."
        };
        dataset_variables.put("gss2", Arrays.asList("abany", "absingle", "age"));
        dataset_variables.put("rtest", Arrays.asList("x"));
        runTestCase("SetDisplayFormat", 2, messages, new HashMap<>(), dataset_variables);
    }

    @Test
    public void test_SetVariableLabel() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> file_variables = new HashMap<>();
        String[] messages =
        {
            "Testing single variable label...",
            "Testing multi variable label..."
        };
        file_variables.put("C2M_36797_0001_DATA.sas7bdat", Arrays.asList("age", "age_comp", "age_rec"));
        runTestCase("SetVariableLabel", 2, messages, file_variables, new HashMap<>());
    }

    @Test
    public void integration_test() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        String[] messages =
        {
            "Testing all types of comments amongst compute, drop, keep, label, and rename...",
            "Testing unquoted labels, comments in strings, and multiline assignment with * ..."
            // TODO: incorporate integration tests 2 and 3
        };
        dataset_variables.put("gss2", Arrays.asList("age", "age_10"));
        runTestCase("integration", 2, messages, new HashMap<>(), dataset_variables);
    }

    private void runTestCase(String file_pattern, int num_test_cases, String[] messages,
                             HashMap<String, List<String>> file_variables, HashMap<String, List<String>> dataset_variables) throws IOException, NoSuchAlgorithmException, JSONException
    {
        String sas_path = "/home/alecastle/IdeaProjects/sas__sdtl/src/test/java/SASConverter/SAS_files/";
        String sdtl_path = "/home/alecastle/IdeaProjects/sas__sdtl/src/test/java/SASConverter/SDTL_files/";

        for(int x=0; x < num_test_cases; x++)
        {
            System.out.print(messages[x]); // describe the test case
            JSONArray program_output = Test_Command(sas_path + file_pattern + "_" + x + ".txt", file_variables, dataset_variables); // run SAS through Converter
            JSONArray correct_output = Text_To_JSON(sdtl_path + file_pattern + "_" + x + "_SDTL.json"); // get correct SDTL from test case
            assertEquals(program_output.toString(4), correct_output.toString(4)); // print assertion error if the two don't match
            System.out.println("PASSED");
        }
    }

}