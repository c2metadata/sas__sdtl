/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified by Alexander Mueller in 2019.
*/

package Models;

public class VariableSymbolExpression extends VariableReferenceBase
{
    public String variableName;

    public VariableSymbolExpression(String name)
    {
        this.$type = "VariableSymbolExpression";
        this.variableName = name;
    }

    // two VariableSymbolExpressions are considered equal when they have the same variable name
    @Override
    public boolean equals(Object object)
    {
        if(!(object instanceof VariableSymbolExpression))
            return false;
        if(object == this)
            return true;
        return ((VariableSymbolExpression) object).variableName.equals(this.variableName);
    }

    // therefore, we can rely on java.lang.String's implementation for the hash code
    @Override
    public int hashCode()
    {
        return variableName.hashCode();
    }
}
