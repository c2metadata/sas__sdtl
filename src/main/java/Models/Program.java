/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified by Alexander Mueller in 2019.
*/

package Models;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Program
{
    public String iD;
    public String sourceFileName;
    public String sourceLanguage;
    public String scriptMD5;
    public String scriptSHA1;
    public Date sourceFileLastUpdate;
    public Long sourceFileSize;
    public Integer lineCount;
    public Integer commandCount;
    public List<Message> messages;
    public String parser;
    public String parserVersion;
    public String modelVersion;
    public Date modelCreatedTime;
    public List<TransformBase> commands;

    public Program()
    {
        this.iD = "";
        this.sourceFileName = "";
        this.sourceLanguage = "";
        this.scriptMD5 = "";
        this.scriptSHA1 = "";
        this.sourceFileLastUpdate = new Date();//TODO: make this do what it's supposed to do
        this.sourceFileSize = (long)-1;
        this.lineCount = -1;
        this.commandCount = -1;
        this.messages = new ArrayList<Message>();
        this.parser = "";
        this.parserVersion = "";
        this.modelVersion = "";
        this.modelCreatedTime = new Date();
        this.commands = new ArrayList<TransformBase>();
    }

    @Override
    public String toString()
    {
        // Create SDTL JSON from Program Object
        Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
        return gson.toJson(this);
    }
}
