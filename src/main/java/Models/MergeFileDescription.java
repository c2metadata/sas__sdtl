/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Alexander Mueller in 2019.
 */

package Models;

import java.util.ArrayList;
import java.util.List;

public class MergeFileDescription
{
    public String fileName;
    public String mergeType;
    public String mergeFlagVariable;
    public List<RenamePair> renameVariables;
    public String update;
    public Boolean newRow;
    public List<VariableReferenceBase> keepVariables;
    public List<VariableReferenceBase> dropVariables;
    public ExpressionBase keepCasesCondition;
    public String $type;

    public MergeFileDescription()
    {
        this.$type = "MergeFileDescription";
    }

    // copy constructor
    public MergeFileDescription(MergeFileDescription merge)
    {
        this.$type = merge.$type;
        this.fileName = merge.fileName;
        this.mergeType = merge.mergeType;
        this.mergeFlagVariable = merge.mergeFlagVariable;
        this.renameVariables = merge.renameVariables;
        this.update = merge.update;
        this.keepVariables = merge.keepVariables;
        this.dropVariables = merge.dropVariables;
        this.keepCasesCondition = merge.keepCasesCondition;
    }

    public MergeFileDescription(String file,
                                String merge_type,
                                String overlap,
                                boolean new_row,
                                List<TransformBase> option_commands,
                                List<VariableReferenceBase> dropVariables)
    {
        this();
        this.fileName = file;
        this.mergeType = merge_type;
        this.update = overlap;
        this.newRow = new_row;
        this.dropVariables = dropVariables;
        for(TransformBase option : option_commands)
        {
            if(option instanceof DropVariables)
                this.dropVariables = ((DropVariables) option).variables;
            if(option instanceof KeepVariables)
                this.keepVariables = ((KeepVariables) option).variables;
            if(option instanceof Rename)
                this.renameVariables = ((Rename) option).renames;
            if(option instanceof KeepCases)
            {
                this.keepCasesCondition = (((KeepCases) option).condition);
            }
        }
    }
}
