/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Alexander Mueller in 2019.
*/

package Models;

import java.util.*;

// singleton class meant to represent the current data step
public class TempDataStep
{
    private class TempFileDescription
    {
        private List<VariableReferenceBase> keeps;
        private List<VariableReferenceBase> drops;
        private List<RenamePair> renames;
        private String filename;

        private TempFileDescription(List<VariableReferenceBase> keeps, List<VariableReferenceBase> drops, List<RenamePair> renames, String filename)
        {
            this.keeps = keeps;
            this.drops = drops;
            this.renames = renames;
            this.filename = filename;
        }
    }

    private String output_dataset;
    public ArrayList<TransformBase> commandTracker = new ArrayList<>();
    public ExpressionBase last_where;   // the WHERE expression from the most recent WHERE statement (used for SAME AND)
    public HashMap<String, List<String>> file_variables;
    public HashMap<String, List<String>> dataset_variables;
    private List<String> active_variables = new ArrayList<>();
    private List<String> prev_active_vars = new ArrayList<>();

    private int nests = 0;      // the current level of nesting (only applies to do blocks and if/then/else blocks)
    private int active = 0;     // the current number of active dataframes (i.e. how many dataframes are in memory)
    private int prev_active = 0;    // the value of active before any dataset options are applied on the output
    private int last_input = -1;    // the current index of the most recent input command (SET, APPEND, or MERGE)
    private String input_dataset;
    private List<String> output_datasets;

    private boolean has_options = false;    // only set to true if the DATA statement has options
    private boolean input_consumed = false; // only set to true if the input has been consumed into an active dataframe
    private List<RenamePair> renames = new ArrayList<>();   // list of renames to apply before the next input
    private Set<VariableReferenceBase> keeps = new LinkedHashSet<>(); // set of all variables being kept before rename
    private Set<VariableReferenceBase> drops = new LinkedHashSet<>(); // set of all variables being dropped before rename

    public TempDataStep(String datum, HashMap<String, List<String>> file_variables, HashMap<String, List<String>> dataset_variables)
    {
        this.output_dataset = datum;
        this.file_variables = file_variables;
        this.dataset_variables = dataset_variables;
    }
    public TempDataStep(List<String> data, HashMap<String, List<String>> file_variables, HashMap<String, List<String>> dataset_variables)
    {
        this.output_datasets = data;
        this.file_variables = file_variables;
        this.dataset_variables = dataset_variables;
    }

    // this method is meant to be called right before the current instance is overwritten
    // in order to record the name of the data set produced most recently by the previous data step
    public String getLastData()
    {
        if(this.output_dataset != null)
            return this.output_dataset;
        return this.output_datasets.get(this.output_datasets.size() - 1);
    }

    // return the list of dataframes produced by this data step (or a String if there's only one)
    public Object dataframesProduced()
    {
        return this.output_dataset != null ? this.output_dataset : this.output_datasets;
    }

    // TODO: check for invalid variable operations per command
    public void addCommandToTracker(TransformBase command)
    {//System.out.println("a new " + command.$type + " command is being added, active is " + active + ", prev_active is " + prev_active + ", and nests is " + nests);
        // consume and produce dataframes based on semantics of nearby commands
        switch(command.$type)
        {
            case "Analysis":
            case "Comment":
            case "Invalid":
            case "Message":
            case "Unsupported":
                // none of these commands consume or produce dataframes, so nothing should be done here
                break;
            case "Load":
                if(!renames.isEmpty())
                {
                    applyRenames();
                }
                //System.out.println(this.file_variables);
                //System.out.println(((Load) command).fileName);
                this.active_variables.addAll(this.file_variables.get(getFilenameFromPath(((Load) command).fileName)));
                input_consumed = true;
                command.producesDataframe = "_activeDataframe_" + active;
                if(nests == 0)
                    last_input = commandTracker.size();
                break;
            case "KeepCases":
            {
                TransformBase input_command = commandTracker.get(last_input);
                boolean where_option = false;
                switch(input_command.$type)
                {
                    case "TempData":
                        where_option = ((TempData) input_command).where != null;
                        break;
                    case "AppendDatasets":
                        break;
                    case "MergeDatasets":
                        where_option = ((MergeDatasets) input_command).has_where();
                        break;
                }
                if(where_option)
                {
                    commandTracker.add(new Invalid("Error: WHERE statements are ignored for datasets with WHERE= options on input."));
                    return;
                }
                if(input_consumed)
                {
                    command.consumesDataframe = input_command.producesDataframe;
                    active++;//System.out.println("after increment in KeepCases case, active is " + active + ", prev_active is " + prev_active);
                    command.producesDataframe = "_activeDataframe_" + active;
                }
                else
                {
                    command.consumesDataframe = input_dataset;
                    command.producesDataframe = "_activeDataframe_" + active;
                }
                last_input++;
                commandTracker.add(last_input, command);
                for(int x=(last_input + 1); x < commandTracker.size(); x++)
                {
                    TransformBase after_command = commandTracker.get(x);
                    if(after_command.consumesDataframe instanceof String)
                    {
                        if(((String) after_command.consumesDataframe).contains("_activeDataframe_"))
                        {
                            int consumed = Integer.parseInt(((String) after_command.consumesDataframe).substring(17));
                            after_command.consumesDataframe = "_activeDataframe_" + (consumed + 1);
                        }
                        else
                        {
                            String consumed = (String) after_command.consumesDataframe;
                            after_command.consumesDataframe = command.producesDataframe;
                            if(consumed.equals(after_command.producesDataframe))
                            {
                                after_command.producesDataframe = command.producesDataframe;
                                continue;
                            }
                        }
                    }
                    else
                    {
                        // TODO: determine whether this is in scope
                    }
                    if(after_command.producesDataframe instanceof String)
                    {
                        int produced = Integer.parseInt(((String) after_command.producesDataframe).substring(17));
                        after_command.producesDataframe = "_activeDataframe_" + (produced + 1);
                    }
                    else
                    {
                        // TODO: determine whether this is in scope
                    }
                }
                input_consumed = true;
                return;
            }
            case "KeepVariables":
                keeps.addAll(((KeepVariables) command).variables);
                return;
            case "DropVariables":
                // KEEP and DROP both result in new active dataframes since they affect what variables are valid later
                drops.addAll(((DropVariables) command).variables);
                return;
            case "Rename":
                // store in case there's a KEEP statement to place this after in order to preserve order of operations
                renames.addAll(((Rename) command).renames);
                return;
            case "MergeDatasets":
            case "AppendDatasets":
                // if there were any renames to apply, apply them here, before the next input is merged/appended
                applyRenames();
                // since variables could be renamed, dropped, or kept on input, this affects what variables are active
                applyPerFileChanges(command);
                // increment the active dataframe since these commands produce a new one
                if(input_consumed)//{
                    active++;//System.out.println("after increment in merge/append case, active is " + active + ", prev_active is " + prev_active);}
                else
                    input_consumed = true;
                // consumesDataframe is set within the tree visitor because it depends on whether the files
                // being merged/appended are actual files or dataframes produced by previous data steps
                command.producesDataframe = "_activeDataframe_" + active;
                if(nests == 0)
                    last_input = commandTracker.size();
                break;
            case "LoopOverList": // TODO: consider doing loop expansion here
                nests++;
                for(TransformBase sub_command : ((LoopOverList) command).commands)
                {
                    addCommandToTracker(sub_command);
                }
                nests--;
                command.consumesDataframe = input_consumed ? "_activeDataframe_" + active : input_dataset;
                command.producesDataframe = command.consumesDataframe;
                break;
            case "IfRows":
                nests++;
                for(TransformBase sub_command : ((IfRows) command).thenCommands)
                {
                    addCommandToTracker(sub_command);
                }
                if(((IfRows) command).elseCommands != null)
                {
                    for(TransformBase sub_command : ((IfRows) command).elseCommands)
                    {
                        addCommandToTracker(sub_command);
                    }
                }
                nests--;
                command.consumesDataframe = input_consumed ? "_activeDataframe_" + active : input_dataset;
                command.producesDataframe = command.consumesDataframe;
                break;
            case "TempData":
                applyRenames();
                if(!((TempData) command).is_input)
                {
                    if(!(commandTracker.get(commandTracker.size() - 1) instanceof TempData))
                    {
                        prev_active = active;
                        prev_active_vars = active_variables;
                    }
                }
                applyDatasetOptions((TempData) command);
                break;
            case "Execute":
                applyRenames();
                if(!(dataframesProduced() instanceof String))
                {
                    // for now, don't add an Execute statement if there are multiple outputs
                    return;
                }
                if (commandTracker.get(commandTracker.size() - 1) instanceof Save)
                {
                    // no dataframes are consumed by Execute if a file was just written to
                    break;
                }
                else
                {
                    //command.consumesDataframe = input_consumed ? ("_activeDataframe_" + active) : input_dataset;
                    //command.producesDataframe = dataframesProduced();
                }
                break;
            case "Compute":
            {
                String new_name = ((VariableSymbolExpression) ((Compute) command).variable).variableName;
                if(!active_variables.contains(new_name))
                    active_variables.add(new_name);
                if(nests == 0)
                {
                    command.consumesDataframe = input_consumed ? "_activeDataframe_" + active : input_dataset;
                    command.producesDataframe = command.consumesDataframe;
                }
                break;
            }
            case "SortCases":
                // if the previous command was AppendDatasets, then this sort is artificial
                if(commandTracker.get(commandTracker.size() - 1) instanceof AppendDatasets)
                {
                    last_input = commandTracker.size();
                }
                // not breaking because SortCases follows the default behavior w.r.t. dataframes consumed and produced
            default:
                // if the command is not a sub command, the default is for it to consume and produce the same dataframe
                if(nests == 0)
                {
                    command.consumesDataframe = input_consumed ? "_activeDataframe_" + active : input_dataset;
                    command.producesDataframe = command.consumesDataframe;
                }
                break;
        }

        // this command shouldn't be added to the tracker if it is a sub command of a loop or if statement
        if(nests == 0)
            commandTracker.add(command);
    }

    private void produceNewDataframe(TransformBase command)
    {
        // if another command has produced an active dataframe, consume the current one and produce a new one
        if(input_consumed)
        {
            command.consumesDataframe = "_activeDataframe_" + active;
            active++;//System.out.println("after increment in produceNewDataframe(), active is " + active + ", prev_active is " + prev_active);
            command.producesDataframe = "_activeDataframe_" + active;
        }
        // otherwise consume the input and produce a new active dataframe
        else
        {
            command.consumesDataframe = input_dataset;
            command.producesDataframe = "_activeDataframe_" + active;
            input_consumed = true;
        }
    }

    private void applyPerFileChanges(TransformBase command)
    {
        List<TempFileDescription> temp_files = new ArrayList<>();
        List<String> temp_active_variables = new ArrayList<>();
        switch(command.$type)
        {
            case "AppendDatasets":
                for(AppendFileDescription file : ((AppendDatasets) command).appendFiles)
                {
                    temp_files.add(new TempFileDescription(file.keepVariables, file.dropVariables, file.renameVariables, file.fileName));
                }
                break;
            case "MergeDatasets":
                for(MergeFileDescription file : ((MergeDatasets) command).mergeFiles)
                {
                    temp_files.add(new TempFileDescription(file.keepVariables, file.dropVariables, file.renameVariables, file.fileName));
                }
                break;
        }
        for(TempFileDescription temp_file : temp_files)
        {
            String just_filename = getFilenameFromPath(temp_file.filename);
            List<String> file_variables_before;

            // if the filename does not contain .sas7bdat, it's an in-memory dataframe
            System.out.println(just_filename);
            System.out.println(file_variables);
            if(!just_filename.contains(".sas7bdat"))
                file_variables_before = this.dataset_variables.get(just_filename);
            else
                file_variables_before = this.file_variables.get(just_filename);

            if(temp_file.drops != null)
            {
                for(VariableReferenceBase dropped : temp_file.drops)
                {
                    file_variables_before.remove(((VariableSymbolExpression) dropped).variableName);
                }
            }
            if(temp_file.keeps != null)
            {
                file_variables_before = new ArrayList<>();
                for(VariableReferenceBase kept : temp_file.keeps)
                {
                    file_variables_before.add(((VariableSymbolExpression) kept).variableName);
                }
            }
            if(temp_file.renames != null)
            {
                for(RenamePair rename : temp_file.renames)
                {
                    int old_name_index = file_variables_before.indexOf(rename.oldVariable.variableName);
                    if(old_name_index != -1)
                    {
                        file_variables_before.set(old_name_index, rename.newVariable.variableName);
                    }
                }
            }

            for(String file_variable : file_variables_before)
            {
                if(!this.active_variables.contains(file_variable))
                    this.active_variables.add(file_variable);
            }
        }
    }

    private void applyRenames()
    {
        // if the renames, keeps, and drops lists are all empty, there's nothing to do here
        if(drops.isEmpty() && keeps.isEmpty() && renames.isEmpty())
            return;

        DropVariables delete = new DropVariables();
        KeepVariables keep = new KeepVariables();

        // apply the union of all drops before applying the quasi-union of all renames
        if(!drops.isEmpty())
        {
            delete.variables.addAll(drops);
            produceNewDataframe(delete);
            changeVariables(drops, true);
        }

        // apply the union of all keeps before applying the quasi-union of all renames
        if(!keeps.isEmpty())
        {
            keep.variables.addAll(keeps);
            produceNewDataframe(keep);
            changeVariables(keeps, false);

            if(!drops.isEmpty())
            {
                if(!Collections.disjoint(keeps, drops))
                {
                    keep.messageText = "Warning: DROP and KEEP statements with overlapping variable lists may cause undefined behavior";
                    delete.messageText = "Warning: DROP and KEEP statements with overlapping variable lists may cause undefined behavior";
                }
                else
                {
                    keep.messageText = "Warning: DROP and KEEP statements in the same data step with mutually exclusive variable lists result in the DROP being completely ignored.";
                    delete.messageText = "Warning: DROP and KEEP statements in the same data step with mutually exclusive variable lists result in the DROP being completely ignored.";
                }
                commandTracker.add(delete);
            }
            commandTracker.add(keep);
        }
        else if(!drops.isEmpty())
        {
            commandTracker.add(delete);
        }

        if(!renames.isEmpty())
        {
            Rename rename = new Rename();
            List<String> old_names = new ArrayList<>();
            for(RenamePair pair : renames)
            {
                if(old_names.contains(pair.oldVariable.variableName))
                {
                    // TODO: fix this!
                    rename.messageText = "Warning: if any old variable name appears in more than one RENAME, only the last one applies.";
                }
                else
                {
                    rename.renames.add(pair);
                }
                old_names.add(pair.oldVariable.variableName);
                int old_name_index = active_variables.indexOf(pair.oldVariable.variableName);
                if(old_name_index != -1)
                {
                    active_variables.set(old_name_index, pair.newVariable.variableName);
                }
            }
            produceNewDataframe(rename);
            commandTracker.add(rename);
        }

        // clear all collections to avoid repeatedly adding the same objects
        drops = new HashSet<>();
        keeps = new HashSet<>();
        renames = new ArrayList<>();
    }

    private void changeVariables(Collection<VariableReferenceBase> variables, boolean drop)
    {
        for(VariableReferenceBase variable : variables)
        {
            switch(variable.$type)
            {
                case "VariableSymbolExpression":
                    if(drop)
                    {
                        active_variables.remove(((VariableSymbolExpression) variable).variableName);
                    }
                    else
                    {
                        active_variables.clear();
                        active_variables.add(((VariableSymbolExpression) variable).variableName);
                    }
                    break;
                case "VariableRangeExpression":
                {
                    String start = (((VariableRangeExpression) variable).first);
                    String end = (((VariableRangeExpression) variable).last);
                    List<String> range = new ArrayList<>();
                    boolean started = false;
                    for(String variable_name : active_variables)
                    {
                        if(variable_name.equals(start))
                            started = true;
                        if(started)
                        {
                            range.add(variable_name);
                            if(variable_name.equals(end))
                                break;
                        }
                    }
                    if(drop)
                    {
                        active_variables.removeAll(range);
                    }
                    else
                    {
                        active_variables.clear();
                        active_variables.addAll(range);
                    }
                    break;
                }
                case "VariableListExpression":
                    changeVariables(((VariableListExpression) variable).variables, drop);
                    break;
                default:
                    active_variables.clear();
            }
        }
    }

    // consume and produce dataframes for the dataset options (e.g. DROP=, KEEP=) from the given DATA or SET statement
    private void applyDatasetOptions(TempData data)
    {//System.out.println(data);

        List<TransformBase> commands_to_add = new ArrayList<>();

        List<String> output_variables = prev_active_vars;

        boolean multiple = !(dataframesProduced() instanceof String);

        if(data.is_input)
        {
            input_dataset = data.dataset_name;
            input_consumed = false;
        }
        else
        {
            has_options = has_options || data.has_options;
        }

        if(data.has_options)
        {
            if(data.keep != null)
            {
                if(data.delete != null)
                {
                    ArrayList<String> variable_keeps = variablesToStrings(data.keep.variables);
                    ArrayList<String> variable_drops = variablesToStrings(data.delete.variables);
                    if((overlap(variable_drops, variable_keeps)
                    || (variable_drops.isEmpty() || variable_keeps.isEmpty()))) // empty indicates AllVariablesExpression
                    {
                        commandTracker.add(new Invalid("Error: KEEP and DROP lists overlap"));
                    }
                    else
                    {
                        data.keep.messageText = "Warning: when both DROP= and KEEP= are present on the same dataset, DROP= is completely ignored.";
                        commands_to_add.add(data.keep);
                    }
                }
                else
                {
                    commands_to_add.add(data.keep);
                    if(data.is_input)
                    {
                        active_variables = new ArrayList<>();
                        for(VariableReferenceBase kept : data.keep.variables)
                        {
                            active_variables.add(((VariableSymbolExpression) kept).variableName);
                        }
                    }
                    else
                    {
                        output_variables = new ArrayList<>();
                        for(VariableReferenceBase kept : data.keep.variables)
                        {
                            output_variables.add(((VariableSymbolExpression) kept).variableName);
                        }
                    }
                }
            }
            else if(data.delete != null)
            {
                commands_to_add.add(data.delete);
                if(data.is_input)
                {
                    for(VariableReferenceBase dropped : data.delete.variables)
                    {
                        active_variables.remove(((VariableSymbolExpression) dropped).variableName);
                    }
                }
                else
                {
                    for(VariableReferenceBase dropped : data.delete.variables)
                    {
                        output_variables.remove(((VariableSymbolExpression) dropped).variableName);
                    }
                }
            }
            if(data.rename != null)
            {
                commands_to_add.add(data.rename);
                if(data.is_input)
                {
                    for(RenamePair rename : data.rename.renames)
                    {
                        int old_name_index = active_variables.indexOf(rename.oldVariable.variableName);
                        if(old_name_index != -1)
                        {
                            active_variables.set(old_name_index, rename.newVariable.variableName);
                        }
                    }
                }
                else
                {
                    for(RenamePair rename : data.rename.renames)
                    {
                        int old_name_index = output_variables.indexOf(rename.oldVariable.variableName);
                        if(old_name_index != -1)
                        {
                            output_variables.set(old_name_index, rename.newVariable.variableName);
                        }
                    }
                }
            }
            if(data.where != null)
            {
                commands_to_add.add(data.where);
            }
        }

        if(!data.is_input)
        {
            if(data.has_filepath)
            {
                Save save = new Save();
                save.fileName = data.dataset_name;
                commands_to_add.add(save);
                file_variables.put(save.fileName, output_variables);
            }
            else
            {
                dataset_variables.put(data.dataset_name, output_variables);
            }
        }

        if(commands_to_add.isEmpty())
        {
            //input_consumed = false;
        }
        else if(commands_to_add.size() == 1)
        {
            TransformBase command = commands_to_add.get(0);

            if(input_consumed)
            {
                command.consumesDataframe = "_activeDataframe_" + prev_active;
            }
            else
            {
                command.consumesDataframe = input_dataset;
            }

            if(!data.is_input)
            {
                command.producesDataframe = data.dataset_name;
            }
            else
            {
                if(multiple)
                {
                    command.producesDataframe = data.dataset_name;
                }
                else
                {
                    if(input_consumed)//{
                        active++;//System.out.println("after increment in applyDatasetOptions(), active is " + active + ", prev_active is " + prev_active);}
                    command.producesDataframe = "_activeDataframe_" + active;
                }
            }
            if(!multiple)
                input_consumed = true;

            commandTracker.add(command);
        }
        else
        {
            for(int x=0; x < commands_to_add.size(); x++)
            {
                TransformBase command = commands_to_add.get(x);//System.out.println("a " + command.$type + " command added to " + data.dataset_name);

                if(input_consumed)
                {
                    if(x == 0)
                    {
                        command.consumesDataframe = "_activeDataframe_" + prev_active;
                    }
                    else
                    {
                        command.consumesDataframe = "_activeDataframe_" + active;
                    }
                    if(data.is_input || (x < (commands_to_add.size() - 1)))
                        active++;//System.out.println("after increment in applyDatasetOptions(), active is " + active + ", prev_active is " + prev_active);
                }
                else
                {
                    command.consumesDataframe = input_dataset;
                    //input_consumed = true;
                }

                if(x == commands_to_add.size() - 1)
                {
                    if(!data.is_input)
                    {
                        command.producesDataframe = data.dataset_name;
                    }
                    else
                    {
                        if(multiple)
                        {
                            command.producesDataframe = data.dataset_name;
                        }
                        else
                        {
                            command.producesDataframe = "_activeDataframe_" + active;
                            //active++;
                        }
                    }
                }
                else
                {
                    command.producesDataframe = "_activeDataframe_" + active;
                }

                commandTracker.add(command);
                input_consumed = true;
            }
        }

        if(data.is_input)
            last_input = commandTracker.size();
    }

    // make sure the last relevant command produces the output dataframes of the DATA statement
    // if no such command is found, then no output dataframes are produced by any command
    public void produceOutputDataframes()
    {
        if(has_options)
            return;
        for(int x=commandTracker.size() - 1; x >= 0; x--)
        {
            TransformBase command = commandTracker.get(x);
            switch(command.$type)
            {
                case "Analysis":
                case "Comment":
                case "Execute":
                case "Invalid":
                case "Message":
                case "TempData":
                case "Unsupported":
                    continue;
                default:
                    command.producesDataframe = dataframesProduced();
                    return;
            }
        }
    }

    public TempArray getArray(String name)
    {
        // iterate backward through command tracker until most recent ARRAY statement with the given name
        for(int x = commandTracker.size() - 1; x >= 0; x--)
        {
            if(commandTracker.get(x) instanceof TempArray)
            {
                TempArray array = (TempArray) commandTracker.get(x);

                if(array.name.equals(name))
                    return array;
            }
        }

        return null;
    }

    // convert a list of VariableReferenceBase to a list of variable names
    private ArrayList<String> variablesToStrings(List<VariableReferenceBase> variables)
    {
        ArrayList<String> strings = new ArrayList<>();

        for(VariableReferenceBase variable : variables)
        {
            switch(variable.$type)
            {
                case "VariableSymbolExpression":
                    strings.add(((VariableSymbolExpression) variable).variableName);
                    break;
                case "VariableListExpression":
                    strings.addAll(variablesToStrings(((VariableListExpression) variable).variables));
                    break;
                case "VariableRangeExpression": // this isn't quite right, but what else can we do without the data?
                    strings.add(((VariableRangeExpression) variable).first);
                    strings.add(((VariableRangeExpression) variable).last);
                    break;
                case "AllVariablesExpression":
                case "AllNumericVariablesExpression":
                case "AllTextVariablesExpression":
                    // in any of these cases, the keep and drop lists definitely overlap, so we need a shocking return value
                    return new ArrayList<>();
            }
        }

        return strings;
    }

    private String getFilenameFromPath(String path)
    {
        String[] path_array = path.contains("/") ? path.split("/") : path.split("\\\\");
        return path_array[path_array.length - 1];
    }

    // return true if the lists contain any common elements
    private boolean overlap(List<String> first, List<String> second)
    {
        for(String x : first)
        {
            if(second.contains(x))
                return true;
        }
        return false;
    }
}
