/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified by Alexander Mueller in 2019.
*/

package Models;


import java.util.ArrayList;
import java.util.List;

public class MergeDatasets extends TransformBase
{
    public List<MergeFileDescription> mergeFiles;
    public List<VariableReferenceBase> mergeByVariables;

    public MergeDatasets()
    {
        this.$type = "MergeDatasets";
        this.command = "MergeDatasets";
        this.mergeFiles = new ArrayList<>();
    }

    public void mergeFile(String file,
                          String merge_type,
                          String overlap,
                          boolean new_row,
                          List<TransformBase> option_commands,
                          List<VariableReferenceBase> dropVariables)
    {
        if(!dropVariables.isEmpty())
            this.mergeFiles.add(new MergeFileDescription(file, merge_type, overlap, new_row, option_commands, dropVariables));
        else
            this.mergeFiles.add(new MergeFileDescription(file, merge_type, overlap, new_row, option_commands, null));
    }

    public boolean has_where()
    {
        for(MergeFileDescription file : mergeFiles)
        {
            if(file.keepCasesCondition != null)
                return true;
        }
        return false;
    }
}
