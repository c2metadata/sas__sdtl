/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Alexander Mueller in 2019.
*/

package Models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

// note: this class merely exists as a temporary storage and retrieval mechanism for a TYPES statement in SAS,
// due to the baffling nature of how type groupings are assigned and ordered in the output data set
public class TempTypeList
{
    public List<String> class_variables;
    public List<TempType> types;
    public String $type;
    public TempTypeList(List<String> classes)
    {
        this.$type = "TempTypeList";
        this.class_variables = classes;
        this.types = new ArrayList<TempType>();
    }

    public static List<List<String>> expandStarGrouping(List<String> first, List<String> second)
    {
        ArrayList<List<String>> product = new ArrayList<>();
        for (String x : first)
        {
            for (String y : second)
            {
                ArrayList<String> element = new ArrayList<>();
                element.add(x);
                element.add(y);

                product.add(element);
            }
        }
        return product;
    }

    @Override
    public String toString()
    {
        StringBuilder class_string = new StringBuilder("CLASSES: ");
        for(TempType type : types)
        {
            class_string.append("'");
            for(String type_class : type.classes)
            {
                class_string.append(type_class);
            }
            class_string.append("', ");
        }

        StringBuilder typestring = new StringBuilder("TYPES: ");
        for(TempType type : types)
        {
            typestring.append("'");
            for(String type_class : type.classes)
            {
                typestring.append(type_class);
            }
            typestring.append(" ");
            typestring.append(type.type);
            typestring.append("', ");
        }

        return class_string.toString() + "\n" + typestring.toString();
    }

    public void addFromWaysList(List<Integer> ways)
    {
        for(int way : ways)
        {
            List<List<String>> aggregates = getN_WayAggregates(way);
            for(List<String> aggregate : aggregates)
            {
                addType(aggregate);
            }
        }
    }

    public void addType(List<String> classes)
    {
        int type = 0;
        for(String class_variable : classes)
        {
            int index = class_variables.indexOf(class_variable);
            if(index >= 0)
            {
                type += getTypeIndex(index);
            }
        }

        types.add(new TempType(classes, type));
    }

    public void generateAllTypes()
    {
        ArrayList<Integer> ways = new ArrayList<>();
        for(int x=0; x <= class_variables.size(); x++)
        {
            ways.add(x);
        }
        addFromWaysList(ways);
    }

    public List<List<String>> getSortedTypes()
    {
        if(types.isEmpty())
            return new ArrayList<>();
        //
        types.sort(new TypeComparator());

        ArrayList<List<String>> sorted_types = new ArrayList<>();
        for(TempType type : types)
        {
            sorted_types.add(type.classes);
        }

        return sorted_types;
    }

    private int getTypeIndex(int variable_index)
    {
        return (int) Math.pow(2, class_variables.size() - variable_index);
    }

    private List<List<String>> getN_WayAggregates(int n)
    {
        ArrayList<List<String>> aggregates = new ArrayList<>();

        // if n is 0, return an empty list to represent an aggregation over all rows
        if(n == 0)
        {
            ArrayList<String> aggregate = new ArrayList<>();
            aggregates.add(aggregate);
            return aggregates;
        }

        // if n is 1, return one list for each class variable to represent all 1-way aggregates
        if(n == 1)
        {
            for(String class_variable : class_variables)
            {
                ArrayList<String> aggregate = new ArrayList<>();
                aggregate.add(class_variable);
                aggregates.add(aggregate);
            }
            return aggregates;
        }

        // if n is equal to the size of the class variable list, return a list containing it as that's the only possibility
        if(n == class_variables.size())
        {
            aggregates.add(class_variables);
            return aggregates;
        }

        // otherwise generate each possible combination of n indeces and return each list of class vars at those indeces
        int[] combination = new int[n];
        for(int x=0; x < n; x++)
        {
            combination[x] = x;
        }

        do
        {
            ArrayList<String> aggregate = new ArrayList<>();
            for(int x : combination)
            {
                aggregate.add(class_variables.get(x));
            }
            aggregates.add(aggregate);
        }
        while((combination = getCombination(combination, class_variables.size() - 1)) != null);

        return aggregates;
    }

    // generate the next lexicographical combination from the previous one. For example, if max is 5:
    //
    //      [1, 2, 3] -> [1, 2, 4]
    //      [1, 2, 5] -> [1, 3, 4]
    //      [1, 4, 5] -> [2, 3, 4]
    //      [3, 4, 5] -> null
    private int[] getCombination(int[] prev, int max)
    {
        int[] next = Arrays.copyOf(prev, prev.length);

        int index = next.length;

        while(index > 0)
        {
            if(next[index - 1] < max)
            {
                next[index - 1]++;
                for(int x = index; x < next.length; x++)
                {
                    next[x] = next[x - 1] + 1;
                }
                return next;
            }
            max--;
            index--;
        }
        return null;
    }

    private class TypeComparator implements Comparator<TempType>
    {
        @Override
        public int compare(TempType first, TempType second)
        {
            return first.type - second.type;
        }
    }
}