/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified by Alexander Mueller in 2019.
*/

package Models;

import java.util.ArrayList;
import java.util.List;

public class FunctionCallExpression extends ExpressionBase
{
    public String function;
    public Boolean isSdtlName;
    public List<FunctionArgument> arguments;

    public FunctionCallExpression()
    {
        this.$type = "FunctionCallExpression";
        this.function = "";
        this.isSdtlName = true;
        this.arguments = new ArrayList<FunctionArgument>();
    }

    public FunctionCallExpression(String name)
    {
        this();
        this.function = name;
    }

    public FunctionCallExpression(int n_args)
    {
        this();
        for(int x=0; x < n_args; x++)
        {
            FunctionArgument arg = new FunctionArgument(x + 1);
            this.arguments.add(arg);
        }
    }

    public FunctionCallExpression(int n_args, String name)
    {
        this(n_args);
        this.function = name;
    }

    public void addArgument(ExpressionBase value)
    {
        FunctionArgument arg = new FunctionArgument(this.arguments.size() + 1);
        arg.argumentValue = value;
        this.arguments.add(arg);
    }

    public void setArgumentValue(int index, ExpressionBase value)
    {
        this.arguments.get(index - 1).argumentValue = value;
    }

    public void setName(String name)
    {
        this.function = name;
    }

    public void makeUserDefined()
    {
        this.isSdtlName = false;
    }

}
