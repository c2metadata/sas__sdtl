/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Alexander Mueller in 2019.
*/

package Models;

import java.util.ArrayList;
import java.util.List;

// NOTE: this class doesn't actually represent anything in SDTL
// it exists merely to make the handling of consumes and produces dataframe more convenient
public class TempData extends TransformBase
{
    public String dataset_name;
    public boolean is_input;
    public boolean has_filepath;
    public boolean has_options;
    public KeepVariables keep;
    public DropVariables delete;
    public Rename rename;
    public KeepCases where;
    public List<SetDatasetProperty> properties;
    public TempData(String name, boolean input)
    {
        this.$type = "TempData";
        this.dataset_name = name;
        this.has_filepath = (name.contains("/") || name.contains("\\"));
        this.is_input = input;
        this.properties = new ArrayList<>();
        this.has_options = false;
    }

    @Override
    public String toString()
    {
        String output = "";
        if(dataset_name != null)
            output = output + "Dataset name: " + dataset_name + "\n";

        output = output + "dataset is input: " + is_input + "\n" + "dataset has filepath: " + has_filepath + "\n" + "dataset has options: " + has_options + "\n";

        if(has_options)
        {
            if(keep != null)
                output = output + "variables kept: " + keep.variables + "\n";
            if(delete != null)
                output = output + "variables dropped: " + delete.variables + "\n";
            if(rename != null)
                output = output + "variables renamed: " + rename.renames + "\n";
            if(where != null)
                output = output + "cases kept: " + where.condition + "\n";
            output = output + "dataset properties: " + properties + "\n";
        }
        return output;
    }
}
