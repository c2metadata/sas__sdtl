/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Alexander Mueller in 2019.
 */

package Models;

import java.util.List;

public class AppendFileDescription
{
    public String fileName;
    public List<RenamePair> renameVariables;
    public List<VariableReferenceBase> keepVariables;
    public List<VariableReferenceBase> dropVariables;
    public String force;
    public String $type;
    public ExpressionBase keepCasesCondition;

    public AppendFileDescription(String name)
    {
        this.$type = "AppendFileDescription";
        this.fileName = name;
    }

    public AppendFileDescription(String name, List<TransformBase> option_commands)
    {
        this(name);
        for (TransformBase option : option_commands) {
            if (option instanceof DropVariables)
                this.dropVariables = ((DropVariables) option).variables;
            if (option instanceof KeepVariables)
                this.keepVariables = ((KeepVariables) option).variables;
            if (option instanceof Rename)
                this.renameVariables = ((Rename) option).renames;
            if (option instanceof KeepCases)
                this.keepCasesCondition = ((KeepCases) option).condition;
        }
    }

    // copy constructor
    public AppendFileDescription(AppendFileDescription append)
    {
        this.$type = append.$type;
        this.fileName = append.fileName;
        this.renameVariables = append.renameVariables;
        this.keepVariables = append.keepVariables;
        this.dropVariables = append.dropVariables;
        this.force = append.force;
        this.keepCasesCondition = append.keepCasesCondition;
    }
}
