/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified by Alexander Mueller in 2019 (only by adding this notice).
*/

package Models;

import java.util.ArrayList;
import java.util.List;

public class CommandList extends TransformBase
{
    public List<TransformBase> commands = new ArrayList<TransformBase>();

    public CommandList()
    {
        this.commands = new ArrayList<TransformBase>();
    }

    public CommandList(List<TransformBase> commands)
    {
        this.commands = commands;
    }

    @Override
    public Boolean isCommandList() {
        return true;
    }

    public void add(TransformBase command) { commands.add(command); }
}
