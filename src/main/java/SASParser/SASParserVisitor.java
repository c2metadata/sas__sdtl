// Generated from SASParser.g4 by ANTLR 4.7.1
package SASParser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SASParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SASParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SASParser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(SASParser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLine(SASParser.LineContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#data_step}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitData_step(SASParser.Data_stepContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#data_step_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitData_step_block(SASParser.Data_step_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#run_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRun_statement(SASParser.Run_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#data_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitData_statement(SASParser.Data_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#data_arguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitData_arguments(SASParser.Data_argumentsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#data_set_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitData_set_name(SASParser.Data_set_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#data_set_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitData_set_options(SASParser.Data_set_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#drop_equals_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDrop_equals_option(SASParser.Drop_equals_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#keep_equals_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeep_equals_option(SASParser.Keep_equals_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#label_equals_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLabel_equals_option(SASParser.Label_equals_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#rename_equals_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRename_equals_option(SASParser.Rename_equals_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#where_equals_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhere_equals_option(SASParser.Where_equals_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(SASParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#set_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSet_statement(SASParser.Set_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#set_arguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSet_arguments(SASParser.Set_argumentsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#set_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSet_options(SASParser.Set_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#end_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnd_option(SASParser.End_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#key_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKey_option(SASParser.Key_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#indsname_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndsname_option(SASParser.Indsname_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#nobs_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNobs_option(SASParser.Nobs_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#open_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpen_option(SASParser.Open_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#point_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPoint_option(SASParser.Point_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#rename_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRename_statement(SASParser.Rename_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#rename_pair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRename_pair(SASParser.Rename_pairContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#select_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_statement(SASParser.Select_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#when_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhen_statement(SASParser.When_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#otherwise_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOtherwise_statement(SASParser.Otherwise_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#merge_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMerge_statement(SASParser.Merge_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#modify_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModify_statement(SASParser.Modify_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#update_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUpdate_statement(SASParser.Update_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#change_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChange_options(SASParser.Change_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#modify_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModify_options(SASParser.Modify_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#curobs_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCurobs_option(SASParser.Curobs_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#update_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUpdate_option(SASParser.Update_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#key_reset_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKey_reset_option(SASParser.Key_reset_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#where_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhere_statement(SASParser.Where_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#compound_where}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_where(SASParser.Compound_whereContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#where_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhere_expression(SASParser.Where_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#where_operation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhere_operation(SASParser.Where_operationContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#assignment_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment_statement(SASParser.Assignment_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#label_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLabel_statement(SASParser.Label_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#label_part}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLabel_part(SASParser.Label_partContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#drop_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDrop_statement(SASParser.Drop_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#keep_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeep_statement(SASParser.Keep_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#array_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_statement(SASParser.Array_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#array_subscript}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_subscript(SASParser.Array_subscriptContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#array_index}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_index(SASParser.Array_indexContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#array_elements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_elements(SASParser.Array_elementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#initial_value_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitial_value_list(SASParser.Initial_value_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#constant_sublist}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant_sublist(SASParser.Constant_sublistContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#do_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDo_statement(SASParser.Do_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#do_spec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDo_spec(SASParser.Do_specContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#if_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_statement(SASParser.If_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#by_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBy_statement(SASParser.By_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#libname_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLibname_statement(SASParser.Libname_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#libref_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLibref_options(SASParser.Libref_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#engine_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEngine_options(SASParser.Engine_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_freq_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_freq_statement(SASParser.Proc_freq_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_freq_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_freq_options(SASParser.Proc_freq_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#exact_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExact_statement(SASParser.Exact_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#output_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOutput_statement(SASParser.Output_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#output_dataset}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOutput_dataset(SASParser.Output_datasetContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#output_part}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOutput_part(SASParser.Output_partContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#output_variables}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOutput_variables(SASParser.Output_variablesContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#output_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOutput_name(SASParser.Output_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#statistic_keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatistic_keyword(SASParser.Statistic_keywordContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#tables_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTables_statement(SASParser.Tables_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#tables_requests}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTables_requests(SASParser.Tables_requestsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#test_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTest_statement(SASParser.Test_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#weight_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWeight_statement(SASParser.Weight_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_means_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_means_statement(SASParser.Proc_means_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_summary_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_summary_statement(SASParser.Proc_summary_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_means_summary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_means_summary(SASParser.Proc_means_summaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#collapse_statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollapse_statements(SASParser.Collapse_statementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_means_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_means_options(SASParser.Proc_means_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#types_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypes_statement(SASParser.Types_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#star_grouping}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStar_grouping(SASParser.Star_groupingContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#type_grouping}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_grouping(SASParser.Type_groupingContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#ways_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWays_statement(SASParser.Ways_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#class_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_statement(SASParser.Class_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#freq_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFreq_statement(SASParser.Freq_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#id_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId_statement(SASParser.Id_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#var_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_statement(SASParser.Var_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_print_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_print_statement(SASParser.Proc_print_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_print_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_print_options(SASParser.Proc_print_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_sort_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_sort_statement(SASParser.Proc_sort_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#sort_direction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSort_direction(SASParser.Sort_directionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#ascending}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAscending(SASParser.AscendingContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#descending}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDescending(SASParser.DescendingContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#collating_sequence_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollating_sequence_option(SASParser.Collating_sequence_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_sort_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_sort_options(SASParser.Proc_sort_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#missing_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMissing_statement(SASParser.Missing_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#comment_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComment_statement(SASParser.Comment_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#delete_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDelete_statement(SASParser.Delete_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#format_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormat_statement(SASParser.Format_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#display_format}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDisplay_format(SASParser.Display_formatContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#default_format}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefault_format(SASParser.Default_formatContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#format}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormat(SASParser.FormatContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#attrib_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttrib_statement(SASParser.Attrib_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#attribute}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttribute(SASParser.AttributeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#attributes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributes(SASParser.AttributesContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#filename_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFilename_statement(SASParser.Filename_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#device_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDevice_type(SASParser.Device_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#encoding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEncoding(SASParser.EncodingContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#filename_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFilename_options(SASParser.Filename_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#openv_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpenv_options(SASParser.Openv_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_step}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_step(SASParser.Proc_stepContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_statement(SASParser.Proc_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_freq_optional_statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_freq_optional_statements(SASParser.Proc_freq_optional_statementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_print_optional_statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_print_optional_statements(SASParser.Proc_print_optional_statementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_datasets_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_datasets_statement(SASParser.Proc_datasets_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_datasets_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_datasets_options(SASParser.Proc_datasets_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#alter_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlter_option(SASParser.Alter_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#details_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDetails_option(SASParser.Details_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#gennum_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGennum_option(SASParser.Gennum_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#memtype_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMemtype_option(SASParser.Memtype_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_datasets_statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_datasets_statements(SASParser.Proc_datasets_statementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#age_arguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAge_arguments(SASParser.Age_argumentsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_append_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_append_statement(SASParser.Proc_append_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#append_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAppend_statement(SASParser.Append_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#audit_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAudit_statement(SASParser.Audit_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#audit_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAudit_options(SASParser.Audit_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#log_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLog_option(SASParser.Log_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#change_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChange_statement(SASParser.Change_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#contents_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContents_statement(SASParser.Contents_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#contents_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContents_options(SASParser.Contents_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#copy_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCopy_statement(SASParser.Copy_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#copy_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCopy_options(SASParser.Copy_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#copy_statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCopy_statements(SASParser.Copy_statementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_datasets_delete}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_datasets_delete(SASParser.Proc_datasets_deleteContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#exchange_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExchange_statement(SASParser.Exchange_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_datasets_modify}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_datasets_modify(SASParser.Proc_datasets_modifyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_modify_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_modify_options(SASParser.Proc_modify_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#password_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPassword_option(SASParser.Password_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#password_modification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPassword_modification(SASParser.Password_modificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#modify_statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModify_statements(SASParser.Modify_statementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#constraint_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstraint_name(SASParser.Constraint_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstraint(SASParser.ConstraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#on_delete}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOn_delete(SASParser.On_deleteContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#on_update}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOn_update(SASParser.On_updateContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#index_spec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex_spec(SASParser.Index_specContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#rebuild_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRebuild_statement(SASParser.Rebuild_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#repair_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepair_statement(SASParser.Repair_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_datasets_save}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_datasets_save(SASParser.Proc_datasets_saveContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_format_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_format_statement(SASParser.Proc_format_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_format_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_format_options(SASParser.Proc_format_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#format_entry}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormat_entry(SASParser.Format_entryContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#format_statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormat_statements(SASParser.Format_statementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#exclude_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExclude_statement(SASParser.Exclude_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#invalue_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInvalue_statement(SASParser.Invalue_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#informat_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInformat_options(SASParser.Informat_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_format_select}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_format_select(SASParser.Proc_format_selectContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#value_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue_statement(SASParser.Value_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#format_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormat_options(SASParser.Format_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#value_range_set}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue_range_set(SASParser.Value_range_setContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#informatted_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInformatted_value(SASParser.Informatted_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#existing_format}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExisting_format(SASParser.Existing_formatContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#proc_transpose_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProc_transpose_statement(SASParser.Proc_transpose_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#transpose_options}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTranspose_options(SASParser.Transpose_optionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#transpose_statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTranspose_statements(SASParser.Transpose_statementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(SASParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#unary_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_expression(SASParser.Unary_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#binary_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinary_expression(SASParser.Binary_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#prefix_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrefix_operator(SASParser.Prefix_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#infix_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInfix_operator(SASParser.Infix_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#operand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperand(SASParser.OperandContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#comparison}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparison(SASParser.ComparisonContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(SASParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#arguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArguments(SASParser.ArgumentsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#argument}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument(SASParser.ArgumentContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#logical_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_expression(SASParser.Logical_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#binary_logic}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinary_logic(SASParser.Binary_logicContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#parenthetical}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenthetical(SASParser.ParentheticalContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#parenthetical_logic}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenthetical_logic(SASParser.Parenthetical_logicContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant(SASParser.ConstantContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#value_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue_list(SASParser.Value_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#value_or_range}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue_or_range(SASParser.Value_or_rangeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#value_range_operation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue_range_operation(SASParser.Value_range_operationContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#numeric_value_range}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumeric_value_range(SASParser.Numeric_value_rangeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#character_value_range}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharacter_value_range(SASParser.Character_value_rangeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#variable_or_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable_or_list(SASParser.Variable_or_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#of_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOf_list(SASParser.Of_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#variable_range_1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable_range_1(SASParser.Variable_range_1Context ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#variable_range_2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable_range_2(SASParser.Variable_range_2Context ctx);
	/**
	 * Visit a parse tree produced by {@link SASParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(SASParser.VariableContext ctx);
}