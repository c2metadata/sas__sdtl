// Generated from SASParser.g4 by ANTLR 4.7.1
package SASParser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SASParser}.
 */
public interface SASParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SASParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(SASParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(SASParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#line}.
	 * @param ctx the parse tree
	 */
	void enterLine(SASParser.LineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#line}.
	 * @param ctx the parse tree
	 */
	void exitLine(SASParser.LineContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#data_step}.
	 * @param ctx the parse tree
	 */
	void enterData_step(SASParser.Data_stepContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#data_step}.
	 * @param ctx the parse tree
	 */
	void exitData_step(SASParser.Data_stepContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#data_step_block}.
	 * @param ctx the parse tree
	 */
	void enterData_step_block(SASParser.Data_step_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#data_step_block}.
	 * @param ctx the parse tree
	 */
	void exitData_step_block(SASParser.Data_step_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#run_statement}.
	 * @param ctx the parse tree
	 */
	void enterRun_statement(SASParser.Run_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#run_statement}.
	 * @param ctx the parse tree
	 */
	void exitRun_statement(SASParser.Run_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#data_statement}.
	 * @param ctx the parse tree
	 */
	void enterData_statement(SASParser.Data_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#data_statement}.
	 * @param ctx the parse tree
	 */
	void exitData_statement(SASParser.Data_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#data_arguments}.
	 * @param ctx the parse tree
	 */
	void enterData_arguments(SASParser.Data_argumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#data_arguments}.
	 * @param ctx the parse tree
	 */
	void exitData_arguments(SASParser.Data_argumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#data_set_name}.
	 * @param ctx the parse tree
	 */
	void enterData_set_name(SASParser.Data_set_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#data_set_name}.
	 * @param ctx the parse tree
	 */
	void exitData_set_name(SASParser.Data_set_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#data_set_options}.
	 * @param ctx the parse tree
	 */
	void enterData_set_options(SASParser.Data_set_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#data_set_options}.
	 * @param ctx the parse tree
	 */
	void exitData_set_options(SASParser.Data_set_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#drop_equals_option}.
	 * @param ctx the parse tree
	 */
	void enterDrop_equals_option(SASParser.Drop_equals_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#drop_equals_option}.
	 * @param ctx the parse tree
	 */
	void exitDrop_equals_option(SASParser.Drop_equals_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#keep_equals_option}.
	 * @param ctx the parse tree
	 */
	void enterKeep_equals_option(SASParser.Keep_equals_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#keep_equals_option}.
	 * @param ctx the parse tree
	 */
	void exitKeep_equals_option(SASParser.Keep_equals_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#label_equals_option}.
	 * @param ctx the parse tree
	 */
	void enterLabel_equals_option(SASParser.Label_equals_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#label_equals_option}.
	 * @param ctx the parse tree
	 */
	void exitLabel_equals_option(SASParser.Label_equals_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#rename_equals_option}.
	 * @param ctx the parse tree
	 */
	void enterRename_equals_option(SASParser.Rename_equals_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#rename_equals_option}.
	 * @param ctx the parse tree
	 */
	void exitRename_equals_option(SASParser.Rename_equals_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#where_equals_option}.
	 * @param ctx the parse tree
	 */
	void enterWhere_equals_option(SASParser.Where_equals_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#where_equals_option}.
	 * @param ctx the parse tree
	 */
	void exitWhere_equals_option(SASParser.Where_equals_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(SASParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(SASParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#set_statement}.
	 * @param ctx the parse tree
	 */
	void enterSet_statement(SASParser.Set_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#set_statement}.
	 * @param ctx the parse tree
	 */
	void exitSet_statement(SASParser.Set_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#set_arguments}.
	 * @param ctx the parse tree
	 */
	void enterSet_arguments(SASParser.Set_argumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#set_arguments}.
	 * @param ctx the parse tree
	 */
	void exitSet_arguments(SASParser.Set_argumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#set_options}.
	 * @param ctx the parse tree
	 */
	void enterSet_options(SASParser.Set_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#set_options}.
	 * @param ctx the parse tree
	 */
	void exitSet_options(SASParser.Set_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#end_option}.
	 * @param ctx the parse tree
	 */
	void enterEnd_option(SASParser.End_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#end_option}.
	 * @param ctx the parse tree
	 */
	void exitEnd_option(SASParser.End_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#key_option}.
	 * @param ctx the parse tree
	 */
	void enterKey_option(SASParser.Key_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#key_option}.
	 * @param ctx the parse tree
	 */
	void exitKey_option(SASParser.Key_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#indsname_option}.
	 * @param ctx the parse tree
	 */
	void enterIndsname_option(SASParser.Indsname_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#indsname_option}.
	 * @param ctx the parse tree
	 */
	void exitIndsname_option(SASParser.Indsname_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#nobs_option}.
	 * @param ctx the parse tree
	 */
	void enterNobs_option(SASParser.Nobs_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#nobs_option}.
	 * @param ctx the parse tree
	 */
	void exitNobs_option(SASParser.Nobs_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#open_option}.
	 * @param ctx the parse tree
	 */
	void enterOpen_option(SASParser.Open_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#open_option}.
	 * @param ctx the parse tree
	 */
	void exitOpen_option(SASParser.Open_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#point_option}.
	 * @param ctx the parse tree
	 */
	void enterPoint_option(SASParser.Point_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#point_option}.
	 * @param ctx the parse tree
	 */
	void exitPoint_option(SASParser.Point_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#rename_statement}.
	 * @param ctx the parse tree
	 */
	void enterRename_statement(SASParser.Rename_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#rename_statement}.
	 * @param ctx the parse tree
	 */
	void exitRename_statement(SASParser.Rename_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#rename_pair}.
	 * @param ctx the parse tree
	 */
	void enterRename_pair(SASParser.Rename_pairContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#rename_pair}.
	 * @param ctx the parse tree
	 */
	void exitRename_pair(SASParser.Rename_pairContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#select_statement}.
	 * @param ctx the parse tree
	 */
	void enterSelect_statement(SASParser.Select_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#select_statement}.
	 * @param ctx the parse tree
	 */
	void exitSelect_statement(SASParser.Select_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#when_statement}.
	 * @param ctx the parse tree
	 */
	void enterWhen_statement(SASParser.When_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#when_statement}.
	 * @param ctx the parse tree
	 */
	void exitWhen_statement(SASParser.When_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#otherwise_statement}.
	 * @param ctx the parse tree
	 */
	void enterOtherwise_statement(SASParser.Otherwise_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#otherwise_statement}.
	 * @param ctx the parse tree
	 */
	void exitOtherwise_statement(SASParser.Otherwise_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#merge_statement}.
	 * @param ctx the parse tree
	 */
	void enterMerge_statement(SASParser.Merge_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#merge_statement}.
	 * @param ctx the parse tree
	 */
	void exitMerge_statement(SASParser.Merge_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#modify_statement}.
	 * @param ctx the parse tree
	 */
	void enterModify_statement(SASParser.Modify_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#modify_statement}.
	 * @param ctx the parse tree
	 */
	void exitModify_statement(SASParser.Modify_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#update_statement}.
	 * @param ctx the parse tree
	 */
	void enterUpdate_statement(SASParser.Update_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#update_statement}.
	 * @param ctx the parse tree
	 */
	void exitUpdate_statement(SASParser.Update_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#change_options}.
	 * @param ctx the parse tree
	 */
	void enterChange_options(SASParser.Change_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#change_options}.
	 * @param ctx the parse tree
	 */
	void exitChange_options(SASParser.Change_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#modify_options}.
	 * @param ctx the parse tree
	 */
	void enterModify_options(SASParser.Modify_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#modify_options}.
	 * @param ctx the parse tree
	 */
	void exitModify_options(SASParser.Modify_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#curobs_option}.
	 * @param ctx the parse tree
	 */
	void enterCurobs_option(SASParser.Curobs_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#curobs_option}.
	 * @param ctx the parse tree
	 */
	void exitCurobs_option(SASParser.Curobs_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#update_option}.
	 * @param ctx the parse tree
	 */
	void enterUpdate_option(SASParser.Update_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#update_option}.
	 * @param ctx the parse tree
	 */
	void exitUpdate_option(SASParser.Update_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#key_reset_option}.
	 * @param ctx the parse tree
	 */
	void enterKey_reset_option(SASParser.Key_reset_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#key_reset_option}.
	 * @param ctx the parse tree
	 */
	void exitKey_reset_option(SASParser.Key_reset_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#where_statement}.
	 * @param ctx the parse tree
	 */
	void enterWhere_statement(SASParser.Where_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#where_statement}.
	 * @param ctx the parse tree
	 */
	void exitWhere_statement(SASParser.Where_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#compound_where}.
	 * @param ctx the parse tree
	 */
	void enterCompound_where(SASParser.Compound_whereContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#compound_where}.
	 * @param ctx the parse tree
	 */
	void exitCompound_where(SASParser.Compound_whereContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#where_expression}.
	 * @param ctx the parse tree
	 */
	void enterWhere_expression(SASParser.Where_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#where_expression}.
	 * @param ctx the parse tree
	 */
	void exitWhere_expression(SASParser.Where_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#where_operation}.
	 * @param ctx the parse tree
	 */
	void enterWhere_operation(SASParser.Where_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#where_operation}.
	 * @param ctx the parse tree
	 */
	void exitWhere_operation(SASParser.Where_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#assignment_statement}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_statement(SASParser.Assignment_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#assignment_statement}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_statement(SASParser.Assignment_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#label_statement}.
	 * @param ctx the parse tree
	 */
	void enterLabel_statement(SASParser.Label_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#label_statement}.
	 * @param ctx the parse tree
	 */
	void exitLabel_statement(SASParser.Label_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#label_part}.
	 * @param ctx the parse tree
	 */
	void enterLabel_part(SASParser.Label_partContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#label_part}.
	 * @param ctx the parse tree
	 */
	void exitLabel_part(SASParser.Label_partContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#drop_statement}.
	 * @param ctx the parse tree
	 */
	void enterDrop_statement(SASParser.Drop_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#drop_statement}.
	 * @param ctx the parse tree
	 */
	void exitDrop_statement(SASParser.Drop_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#keep_statement}.
	 * @param ctx the parse tree
	 */
	void enterKeep_statement(SASParser.Keep_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#keep_statement}.
	 * @param ctx the parse tree
	 */
	void exitKeep_statement(SASParser.Keep_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#array_statement}.
	 * @param ctx the parse tree
	 */
	void enterArray_statement(SASParser.Array_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#array_statement}.
	 * @param ctx the parse tree
	 */
	void exitArray_statement(SASParser.Array_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#array_subscript}.
	 * @param ctx the parse tree
	 */
	void enterArray_subscript(SASParser.Array_subscriptContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#array_subscript}.
	 * @param ctx the parse tree
	 */
	void exitArray_subscript(SASParser.Array_subscriptContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#array_index}.
	 * @param ctx the parse tree
	 */
	void enterArray_index(SASParser.Array_indexContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#array_index}.
	 * @param ctx the parse tree
	 */
	void exitArray_index(SASParser.Array_indexContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#array_elements}.
	 * @param ctx the parse tree
	 */
	void enterArray_elements(SASParser.Array_elementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#array_elements}.
	 * @param ctx the parse tree
	 */
	void exitArray_elements(SASParser.Array_elementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#initial_value_list}.
	 * @param ctx the parse tree
	 */
	void enterInitial_value_list(SASParser.Initial_value_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#initial_value_list}.
	 * @param ctx the parse tree
	 */
	void exitInitial_value_list(SASParser.Initial_value_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#constant_sublist}.
	 * @param ctx the parse tree
	 */
	void enterConstant_sublist(SASParser.Constant_sublistContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#constant_sublist}.
	 * @param ctx the parse tree
	 */
	void exitConstant_sublist(SASParser.Constant_sublistContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#do_statement}.
	 * @param ctx the parse tree
	 */
	void enterDo_statement(SASParser.Do_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#do_statement}.
	 * @param ctx the parse tree
	 */
	void exitDo_statement(SASParser.Do_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#do_spec}.
	 * @param ctx the parse tree
	 */
	void enterDo_spec(SASParser.Do_specContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#do_spec}.
	 * @param ctx the parse tree
	 */
	void exitDo_spec(SASParser.Do_specContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement(SASParser.If_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement(SASParser.If_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#by_statement}.
	 * @param ctx the parse tree
	 */
	void enterBy_statement(SASParser.By_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#by_statement}.
	 * @param ctx the parse tree
	 */
	void exitBy_statement(SASParser.By_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#libname_statement}.
	 * @param ctx the parse tree
	 */
	void enterLibname_statement(SASParser.Libname_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#libname_statement}.
	 * @param ctx the parse tree
	 */
	void exitLibname_statement(SASParser.Libname_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#libref_options}.
	 * @param ctx the parse tree
	 */
	void enterLibref_options(SASParser.Libref_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#libref_options}.
	 * @param ctx the parse tree
	 */
	void exitLibref_options(SASParser.Libref_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#engine_options}.
	 * @param ctx the parse tree
	 */
	void enterEngine_options(SASParser.Engine_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#engine_options}.
	 * @param ctx the parse tree
	 */
	void exitEngine_options(SASParser.Engine_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_freq_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_freq_statement(SASParser.Proc_freq_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_freq_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_freq_statement(SASParser.Proc_freq_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_freq_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_freq_options(SASParser.Proc_freq_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_freq_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_freq_options(SASParser.Proc_freq_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#exact_statement}.
	 * @param ctx the parse tree
	 */
	void enterExact_statement(SASParser.Exact_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#exact_statement}.
	 * @param ctx the parse tree
	 */
	void exitExact_statement(SASParser.Exact_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#output_statement}.
	 * @param ctx the parse tree
	 */
	void enterOutput_statement(SASParser.Output_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#output_statement}.
	 * @param ctx the parse tree
	 */
	void exitOutput_statement(SASParser.Output_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#output_dataset}.
	 * @param ctx the parse tree
	 */
	void enterOutput_dataset(SASParser.Output_datasetContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#output_dataset}.
	 * @param ctx the parse tree
	 */
	void exitOutput_dataset(SASParser.Output_datasetContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#output_part}.
	 * @param ctx the parse tree
	 */
	void enterOutput_part(SASParser.Output_partContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#output_part}.
	 * @param ctx the parse tree
	 */
	void exitOutput_part(SASParser.Output_partContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#output_variables}.
	 * @param ctx the parse tree
	 */
	void enterOutput_variables(SASParser.Output_variablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#output_variables}.
	 * @param ctx the parse tree
	 */
	void exitOutput_variables(SASParser.Output_variablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#output_name}.
	 * @param ctx the parse tree
	 */
	void enterOutput_name(SASParser.Output_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#output_name}.
	 * @param ctx the parse tree
	 */
	void exitOutput_name(SASParser.Output_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#statistic_keyword}.
	 * @param ctx the parse tree
	 */
	void enterStatistic_keyword(SASParser.Statistic_keywordContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#statistic_keyword}.
	 * @param ctx the parse tree
	 */
	void exitStatistic_keyword(SASParser.Statistic_keywordContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#tables_statement}.
	 * @param ctx the parse tree
	 */
	void enterTables_statement(SASParser.Tables_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#tables_statement}.
	 * @param ctx the parse tree
	 */
	void exitTables_statement(SASParser.Tables_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#tables_requests}.
	 * @param ctx the parse tree
	 */
	void enterTables_requests(SASParser.Tables_requestsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#tables_requests}.
	 * @param ctx the parse tree
	 */
	void exitTables_requests(SASParser.Tables_requestsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#test_statement}.
	 * @param ctx the parse tree
	 */
	void enterTest_statement(SASParser.Test_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#test_statement}.
	 * @param ctx the parse tree
	 */
	void exitTest_statement(SASParser.Test_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#weight_statement}.
	 * @param ctx the parse tree
	 */
	void enterWeight_statement(SASParser.Weight_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#weight_statement}.
	 * @param ctx the parse tree
	 */
	void exitWeight_statement(SASParser.Weight_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_means_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_means_statement(SASParser.Proc_means_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_means_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_means_statement(SASParser.Proc_means_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_summary_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_summary_statement(SASParser.Proc_summary_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_summary_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_summary_statement(SASParser.Proc_summary_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_means_summary}.
	 * @param ctx the parse tree
	 */
	void enterProc_means_summary(SASParser.Proc_means_summaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_means_summary}.
	 * @param ctx the parse tree
	 */
	void exitProc_means_summary(SASParser.Proc_means_summaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#collapse_statements}.
	 * @param ctx the parse tree
	 */
	void enterCollapse_statements(SASParser.Collapse_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#collapse_statements}.
	 * @param ctx the parse tree
	 */
	void exitCollapse_statements(SASParser.Collapse_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_means_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_means_options(SASParser.Proc_means_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_means_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_means_options(SASParser.Proc_means_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#types_statement}.
	 * @param ctx the parse tree
	 */
	void enterTypes_statement(SASParser.Types_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#types_statement}.
	 * @param ctx the parse tree
	 */
	void exitTypes_statement(SASParser.Types_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#star_grouping}.
	 * @param ctx the parse tree
	 */
	void enterStar_grouping(SASParser.Star_groupingContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#star_grouping}.
	 * @param ctx the parse tree
	 */
	void exitStar_grouping(SASParser.Star_groupingContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#type_grouping}.
	 * @param ctx the parse tree
	 */
	void enterType_grouping(SASParser.Type_groupingContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#type_grouping}.
	 * @param ctx the parse tree
	 */
	void exitType_grouping(SASParser.Type_groupingContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#ways_statement}.
	 * @param ctx the parse tree
	 */
	void enterWays_statement(SASParser.Ways_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#ways_statement}.
	 * @param ctx the parse tree
	 */
	void exitWays_statement(SASParser.Ways_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#class_statement}.
	 * @param ctx the parse tree
	 */
	void enterClass_statement(SASParser.Class_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#class_statement}.
	 * @param ctx the parse tree
	 */
	void exitClass_statement(SASParser.Class_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#freq_statement}.
	 * @param ctx the parse tree
	 */
	void enterFreq_statement(SASParser.Freq_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#freq_statement}.
	 * @param ctx the parse tree
	 */
	void exitFreq_statement(SASParser.Freq_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#id_statement}.
	 * @param ctx the parse tree
	 */
	void enterId_statement(SASParser.Id_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#id_statement}.
	 * @param ctx the parse tree
	 */
	void exitId_statement(SASParser.Id_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#var_statement}.
	 * @param ctx the parse tree
	 */
	void enterVar_statement(SASParser.Var_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#var_statement}.
	 * @param ctx the parse tree
	 */
	void exitVar_statement(SASParser.Var_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_print_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_print_statement(SASParser.Proc_print_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_print_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_print_statement(SASParser.Proc_print_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_print_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_print_options(SASParser.Proc_print_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_print_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_print_options(SASParser.Proc_print_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_sort_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_sort_statement(SASParser.Proc_sort_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_sort_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_sort_statement(SASParser.Proc_sort_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#sort_direction}.
	 * @param ctx the parse tree
	 */
	void enterSort_direction(SASParser.Sort_directionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#sort_direction}.
	 * @param ctx the parse tree
	 */
	void exitSort_direction(SASParser.Sort_directionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#ascending}.
	 * @param ctx the parse tree
	 */
	void enterAscending(SASParser.AscendingContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#ascending}.
	 * @param ctx the parse tree
	 */
	void exitAscending(SASParser.AscendingContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#descending}.
	 * @param ctx the parse tree
	 */
	void enterDescending(SASParser.DescendingContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#descending}.
	 * @param ctx the parse tree
	 */
	void exitDescending(SASParser.DescendingContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#collating_sequence_option}.
	 * @param ctx the parse tree
	 */
	void enterCollating_sequence_option(SASParser.Collating_sequence_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#collating_sequence_option}.
	 * @param ctx the parse tree
	 */
	void exitCollating_sequence_option(SASParser.Collating_sequence_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_sort_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_sort_options(SASParser.Proc_sort_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_sort_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_sort_options(SASParser.Proc_sort_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#missing_statement}.
	 * @param ctx the parse tree
	 */
	void enterMissing_statement(SASParser.Missing_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#missing_statement}.
	 * @param ctx the parse tree
	 */
	void exitMissing_statement(SASParser.Missing_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#comment_statement}.
	 * @param ctx the parse tree
	 */
	void enterComment_statement(SASParser.Comment_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#comment_statement}.
	 * @param ctx the parse tree
	 */
	void exitComment_statement(SASParser.Comment_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#delete_statement}.
	 * @param ctx the parse tree
	 */
	void enterDelete_statement(SASParser.Delete_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#delete_statement}.
	 * @param ctx the parse tree
	 */
	void exitDelete_statement(SASParser.Delete_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#format_statement}.
	 * @param ctx the parse tree
	 */
	void enterFormat_statement(SASParser.Format_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#format_statement}.
	 * @param ctx the parse tree
	 */
	void exitFormat_statement(SASParser.Format_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#display_format}.
	 * @param ctx the parse tree
	 */
	void enterDisplay_format(SASParser.Display_formatContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#display_format}.
	 * @param ctx the parse tree
	 */
	void exitDisplay_format(SASParser.Display_formatContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#default_format}.
	 * @param ctx the parse tree
	 */
	void enterDefault_format(SASParser.Default_formatContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#default_format}.
	 * @param ctx the parse tree
	 */
	void exitDefault_format(SASParser.Default_formatContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#format}.
	 * @param ctx the parse tree
	 */
	void enterFormat(SASParser.FormatContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#format}.
	 * @param ctx the parse tree
	 */
	void exitFormat(SASParser.FormatContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#attrib_statement}.
	 * @param ctx the parse tree
	 */
	void enterAttrib_statement(SASParser.Attrib_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#attrib_statement}.
	 * @param ctx the parse tree
	 */
	void exitAttrib_statement(SASParser.Attrib_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#attribute}.
	 * @param ctx the parse tree
	 */
	void enterAttribute(SASParser.AttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#attribute}.
	 * @param ctx the parse tree
	 */
	void exitAttribute(SASParser.AttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#attributes}.
	 * @param ctx the parse tree
	 */
	void enterAttributes(SASParser.AttributesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#attributes}.
	 * @param ctx the parse tree
	 */
	void exitAttributes(SASParser.AttributesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#filename_statement}.
	 * @param ctx the parse tree
	 */
	void enterFilename_statement(SASParser.Filename_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#filename_statement}.
	 * @param ctx the parse tree
	 */
	void exitFilename_statement(SASParser.Filename_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#device_type}.
	 * @param ctx the parse tree
	 */
	void enterDevice_type(SASParser.Device_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#device_type}.
	 * @param ctx the parse tree
	 */
	void exitDevice_type(SASParser.Device_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#encoding}.
	 * @param ctx the parse tree
	 */
	void enterEncoding(SASParser.EncodingContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#encoding}.
	 * @param ctx the parse tree
	 */
	void exitEncoding(SASParser.EncodingContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#filename_options}.
	 * @param ctx the parse tree
	 */
	void enterFilename_options(SASParser.Filename_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#filename_options}.
	 * @param ctx the parse tree
	 */
	void exitFilename_options(SASParser.Filename_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#openv_options}.
	 * @param ctx the parse tree
	 */
	void enterOpenv_options(SASParser.Openv_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#openv_options}.
	 * @param ctx the parse tree
	 */
	void exitOpenv_options(SASParser.Openv_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_step}.
	 * @param ctx the parse tree
	 */
	void enterProc_step(SASParser.Proc_stepContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_step}.
	 * @param ctx the parse tree
	 */
	void exitProc_step(SASParser.Proc_stepContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_statement(SASParser.Proc_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_statement(SASParser.Proc_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_freq_optional_statements}.
	 * @param ctx the parse tree
	 */
	void enterProc_freq_optional_statements(SASParser.Proc_freq_optional_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_freq_optional_statements}.
	 * @param ctx the parse tree
	 */
	void exitProc_freq_optional_statements(SASParser.Proc_freq_optional_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_print_optional_statements}.
	 * @param ctx the parse tree
	 */
	void enterProc_print_optional_statements(SASParser.Proc_print_optional_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_print_optional_statements}.
	 * @param ctx the parse tree
	 */
	void exitProc_print_optional_statements(SASParser.Proc_print_optional_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_datasets_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_datasets_statement(SASParser.Proc_datasets_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_datasets_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_datasets_statement(SASParser.Proc_datasets_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_datasets_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_datasets_options(SASParser.Proc_datasets_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_datasets_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_datasets_options(SASParser.Proc_datasets_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#alter_option}.
	 * @param ctx the parse tree
	 */
	void enterAlter_option(SASParser.Alter_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#alter_option}.
	 * @param ctx the parse tree
	 */
	void exitAlter_option(SASParser.Alter_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#details_option}.
	 * @param ctx the parse tree
	 */
	void enterDetails_option(SASParser.Details_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#details_option}.
	 * @param ctx the parse tree
	 */
	void exitDetails_option(SASParser.Details_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#gennum_option}.
	 * @param ctx the parse tree
	 */
	void enterGennum_option(SASParser.Gennum_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#gennum_option}.
	 * @param ctx the parse tree
	 */
	void exitGennum_option(SASParser.Gennum_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#memtype_option}.
	 * @param ctx the parse tree
	 */
	void enterMemtype_option(SASParser.Memtype_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#memtype_option}.
	 * @param ctx the parse tree
	 */
	void exitMemtype_option(SASParser.Memtype_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_datasets_statements}.
	 * @param ctx the parse tree
	 */
	void enterProc_datasets_statements(SASParser.Proc_datasets_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_datasets_statements}.
	 * @param ctx the parse tree
	 */
	void exitProc_datasets_statements(SASParser.Proc_datasets_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#age_arguments}.
	 * @param ctx the parse tree
	 */
	void enterAge_arguments(SASParser.Age_argumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#age_arguments}.
	 * @param ctx the parse tree
	 */
	void exitAge_arguments(SASParser.Age_argumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_append_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_append_statement(SASParser.Proc_append_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_append_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_append_statement(SASParser.Proc_append_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#append_statement}.
	 * @param ctx the parse tree
	 */
	void enterAppend_statement(SASParser.Append_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#append_statement}.
	 * @param ctx the parse tree
	 */
	void exitAppend_statement(SASParser.Append_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#audit_statement}.
	 * @param ctx the parse tree
	 */
	void enterAudit_statement(SASParser.Audit_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#audit_statement}.
	 * @param ctx the parse tree
	 */
	void exitAudit_statement(SASParser.Audit_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#audit_options}.
	 * @param ctx the parse tree
	 */
	void enterAudit_options(SASParser.Audit_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#audit_options}.
	 * @param ctx the parse tree
	 */
	void exitAudit_options(SASParser.Audit_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#log_option}.
	 * @param ctx the parse tree
	 */
	void enterLog_option(SASParser.Log_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#log_option}.
	 * @param ctx the parse tree
	 */
	void exitLog_option(SASParser.Log_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#change_statement}.
	 * @param ctx the parse tree
	 */
	void enterChange_statement(SASParser.Change_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#change_statement}.
	 * @param ctx the parse tree
	 */
	void exitChange_statement(SASParser.Change_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#contents_statement}.
	 * @param ctx the parse tree
	 */
	void enterContents_statement(SASParser.Contents_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#contents_statement}.
	 * @param ctx the parse tree
	 */
	void exitContents_statement(SASParser.Contents_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#contents_options}.
	 * @param ctx the parse tree
	 */
	void enterContents_options(SASParser.Contents_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#contents_options}.
	 * @param ctx the parse tree
	 */
	void exitContents_options(SASParser.Contents_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#copy_statement}.
	 * @param ctx the parse tree
	 */
	void enterCopy_statement(SASParser.Copy_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#copy_statement}.
	 * @param ctx the parse tree
	 */
	void exitCopy_statement(SASParser.Copy_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#copy_options}.
	 * @param ctx the parse tree
	 */
	void enterCopy_options(SASParser.Copy_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#copy_options}.
	 * @param ctx the parse tree
	 */
	void exitCopy_options(SASParser.Copy_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#copy_statements}.
	 * @param ctx the parse tree
	 */
	void enterCopy_statements(SASParser.Copy_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#copy_statements}.
	 * @param ctx the parse tree
	 */
	void exitCopy_statements(SASParser.Copy_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_datasets_delete}.
	 * @param ctx the parse tree
	 */
	void enterProc_datasets_delete(SASParser.Proc_datasets_deleteContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_datasets_delete}.
	 * @param ctx the parse tree
	 */
	void exitProc_datasets_delete(SASParser.Proc_datasets_deleteContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#exchange_statement}.
	 * @param ctx the parse tree
	 */
	void enterExchange_statement(SASParser.Exchange_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#exchange_statement}.
	 * @param ctx the parse tree
	 */
	void exitExchange_statement(SASParser.Exchange_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_datasets_modify}.
	 * @param ctx the parse tree
	 */
	void enterProc_datasets_modify(SASParser.Proc_datasets_modifyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_datasets_modify}.
	 * @param ctx the parse tree
	 */
	void exitProc_datasets_modify(SASParser.Proc_datasets_modifyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_modify_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_modify_options(SASParser.Proc_modify_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_modify_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_modify_options(SASParser.Proc_modify_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#password_option}.
	 * @param ctx the parse tree
	 */
	void enterPassword_option(SASParser.Password_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#password_option}.
	 * @param ctx the parse tree
	 */
	void exitPassword_option(SASParser.Password_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#password_modification}.
	 * @param ctx the parse tree
	 */
	void enterPassword_modification(SASParser.Password_modificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#password_modification}.
	 * @param ctx the parse tree
	 */
	void exitPassword_modification(SASParser.Password_modificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#modify_statements}.
	 * @param ctx the parse tree
	 */
	void enterModify_statements(SASParser.Modify_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#modify_statements}.
	 * @param ctx the parse tree
	 */
	void exitModify_statements(SASParser.Modify_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#constraint_name}.
	 * @param ctx the parse tree
	 */
	void enterConstraint_name(SASParser.Constraint_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#constraint_name}.
	 * @param ctx the parse tree
	 */
	void exitConstraint_name(SASParser.Constraint_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#constraint}.
	 * @param ctx the parse tree
	 */
	void enterConstraint(SASParser.ConstraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#constraint}.
	 * @param ctx the parse tree
	 */
	void exitConstraint(SASParser.ConstraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#on_delete}.
	 * @param ctx the parse tree
	 */
	void enterOn_delete(SASParser.On_deleteContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#on_delete}.
	 * @param ctx the parse tree
	 */
	void exitOn_delete(SASParser.On_deleteContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#on_update}.
	 * @param ctx the parse tree
	 */
	void enterOn_update(SASParser.On_updateContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#on_update}.
	 * @param ctx the parse tree
	 */
	void exitOn_update(SASParser.On_updateContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#index_spec}.
	 * @param ctx the parse tree
	 */
	void enterIndex_spec(SASParser.Index_specContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#index_spec}.
	 * @param ctx the parse tree
	 */
	void exitIndex_spec(SASParser.Index_specContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#rebuild_statement}.
	 * @param ctx the parse tree
	 */
	void enterRebuild_statement(SASParser.Rebuild_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#rebuild_statement}.
	 * @param ctx the parse tree
	 */
	void exitRebuild_statement(SASParser.Rebuild_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#repair_statement}.
	 * @param ctx the parse tree
	 */
	void enterRepair_statement(SASParser.Repair_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#repair_statement}.
	 * @param ctx the parse tree
	 */
	void exitRepair_statement(SASParser.Repair_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_datasets_save}.
	 * @param ctx the parse tree
	 */
	void enterProc_datasets_save(SASParser.Proc_datasets_saveContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_datasets_save}.
	 * @param ctx the parse tree
	 */
	void exitProc_datasets_save(SASParser.Proc_datasets_saveContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_format_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_format_statement(SASParser.Proc_format_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_format_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_format_statement(SASParser.Proc_format_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_format_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_format_options(SASParser.Proc_format_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_format_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_format_options(SASParser.Proc_format_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#format_entry}.
	 * @param ctx the parse tree
	 */
	void enterFormat_entry(SASParser.Format_entryContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#format_entry}.
	 * @param ctx the parse tree
	 */
	void exitFormat_entry(SASParser.Format_entryContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#format_statements}.
	 * @param ctx the parse tree
	 */
	void enterFormat_statements(SASParser.Format_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#format_statements}.
	 * @param ctx the parse tree
	 */
	void exitFormat_statements(SASParser.Format_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#exclude_statement}.
	 * @param ctx the parse tree
	 */
	void enterExclude_statement(SASParser.Exclude_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#exclude_statement}.
	 * @param ctx the parse tree
	 */
	void exitExclude_statement(SASParser.Exclude_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#invalue_statement}.
	 * @param ctx the parse tree
	 */
	void enterInvalue_statement(SASParser.Invalue_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#invalue_statement}.
	 * @param ctx the parse tree
	 */
	void exitInvalue_statement(SASParser.Invalue_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#informat_options}.
	 * @param ctx the parse tree
	 */
	void enterInformat_options(SASParser.Informat_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#informat_options}.
	 * @param ctx the parse tree
	 */
	void exitInformat_options(SASParser.Informat_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_format_select}.
	 * @param ctx the parse tree
	 */
	void enterProc_format_select(SASParser.Proc_format_selectContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_format_select}.
	 * @param ctx the parse tree
	 */
	void exitProc_format_select(SASParser.Proc_format_selectContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#value_statement}.
	 * @param ctx the parse tree
	 */
	void enterValue_statement(SASParser.Value_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#value_statement}.
	 * @param ctx the parse tree
	 */
	void exitValue_statement(SASParser.Value_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#format_options}.
	 * @param ctx the parse tree
	 */
	void enterFormat_options(SASParser.Format_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#format_options}.
	 * @param ctx the parse tree
	 */
	void exitFormat_options(SASParser.Format_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#value_range_set}.
	 * @param ctx the parse tree
	 */
	void enterValue_range_set(SASParser.Value_range_setContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#value_range_set}.
	 * @param ctx the parse tree
	 */
	void exitValue_range_set(SASParser.Value_range_setContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#informatted_value}.
	 * @param ctx the parse tree
	 */
	void enterInformatted_value(SASParser.Informatted_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#informatted_value}.
	 * @param ctx the parse tree
	 */
	void exitInformatted_value(SASParser.Informatted_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#existing_format}.
	 * @param ctx the parse tree
	 */
	void enterExisting_format(SASParser.Existing_formatContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#existing_format}.
	 * @param ctx the parse tree
	 */
	void exitExisting_format(SASParser.Existing_formatContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_transpose_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_transpose_statement(SASParser.Proc_transpose_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_transpose_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_transpose_statement(SASParser.Proc_transpose_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#transpose_options}.
	 * @param ctx the parse tree
	 */
	void enterTranspose_options(SASParser.Transpose_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#transpose_options}.
	 * @param ctx the parse tree
	 */
	void exitTranspose_options(SASParser.Transpose_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#transpose_statements}.
	 * @param ctx the parse tree
	 */
	void enterTranspose_statements(SASParser.Transpose_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#transpose_statements}.
	 * @param ctx the parse tree
	 */
	void exitTranspose_statements(SASParser.Transpose_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(SASParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(SASParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void enterUnary_expression(SASParser.Unary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void exitUnary_expression(SASParser.Unary_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#binary_expression}.
	 * @param ctx the parse tree
	 */
	void enterBinary_expression(SASParser.Binary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#binary_expression}.
	 * @param ctx the parse tree
	 */
	void exitBinary_expression(SASParser.Binary_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#prefix_operator}.
	 * @param ctx the parse tree
	 */
	void enterPrefix_operator(SASParser.Prefix_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#prefix_operator}.
	 * @param ctx the parse tree
	 */
	void exitPrefix_operator(SASParser.Prefix_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#infix_operator}.
	 * @param ctx the parse tree
	 */
	void enterInfix_operator(SASParser.Infix_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#infix_operator}.
	 * @param ctx the parse tree
	 */
	void exitInfix_operator(SASParser.Infix_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#operand}.
	 * @param ctx the parse tree
	 */
	void enterOperand(SASParser.OperandContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#operand}.
	 * @param ctx the parse tree
	 */
	void exitOperand(SASParser.OperandContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#comparison}.
	 * @param ctx the parse tree
	 */
	void enterComparison(SASParser.ComparisonContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#comparison}.
	 * @param ctx the parse tree
	 */
	void exitComparison(SASParser.ComparisonContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(SASParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(SASParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#arguments}.
	 * @param ctx the parse tree
	 */
	void enterArguments(SASParser.ArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#arguments}.
	 * @param ctx the parse tree
	 */
	void exitArguments(SASParser.ArgumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#argument}.
	 * @param ctx the parse tree
	 */
	void enterArgument(SASParser.ArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#argument}.
	 * @param ctx the parse tree
	 */
	void exitArgument(SASParser.ArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#logical_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_expression(SASParser.Logical_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#logical_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_expression(SASParser.Logical_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#binary_logic}.
	 * @param ctx the parse tree
	 */
	void enterBinary_logic(SASParser.Binary_logicContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#binary_logic}.
	 * @param ctx the parse tree
	 */
	void exitBinary_logic(SASParser.Binary_logicContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#parenthetical}.
	 * @param ctx the parse tree
	 */
	void enterParenthetical(SASParser.ParentheticalContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#parenthetical}.
	 * @param ctx the parse tree
	 */
	void exitParenthetical(SASParser.ParentheticalContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#parenthetical_logic}.
	 * @param ctx the parse tree
	 */
	void enterParenthetical_logic(SASParser.Parenthetical_logicContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#parenthetical_logic}.
	 * @param ctx the parse tree
	 */
	void exitParenthetical_logic(SASParser.Parenthetical_logicContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(SASParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(SASParser.ConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#value_list}.
	 * @param ctx the parse tree
	 */
	void enterValue_list(SASParser.Value_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#value_list}.
	 * @param ctx the parse tree
	 */
	void exitValue_list(SASParser.Value_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#value_or_range}.
	 * @param ctx the parse tree
	 */
	void enterValue_or_range(SASParser.Value_or_rangeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#value_or_range}.
	 * @param ctx the parse tree
	 */
	void exitValue_or_range(SASParser.Value_or_rangeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#value_range_operation}.
	 * @param ctx the parse tree
	 */
	void enterValue_range_operation(SASParser.Value_range_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#value_range_operation}.
	 * @param ctx the parse tree
	 */
	void exitValue_range_operation(SASParser.Value_range_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#numeric_value_range}.
	 * @param ctx the parse tree
	 */
	void enterNumeric_value_range(SASParser.Numeric_value_rangeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#numeric_value_range}.
	 * @param ctx the parse tree
	 */
	void exitNumeric_value_range(SASParser.Numeric_value_rangeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#character_value_range}.
	 * @param ctx the parse tree
	 */
	void enterCharacter_value_range(SASParser.Character_value_rangeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#character_value_range}.
	 * @param ctx the parse tree
	 */
	void exitCharacter_value_range(SASParser.Character_value_rangeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#variable_or_list}.
	 * @param ctx the parse tree
	 */
	void enterVariable_or_list(SASParser.Variable_or_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#variable_or_list}.
	 * @param ctx the parse tree
	 */
	void exitVariable_or_list(SASParser.Variable_or_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#of_list}.
	 * @param ctx the parse tree
	 */
	void enterOf_list(SASParser.Of_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#of_list}.
	 * @param ctx the parse tree
	 */
	void exitOf_list(SASParser.Of_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#variable_range_1}.
	 * @param ctx the parse tree
	 */
	void enterVariable_range_1(SASParser.Variable_range_1Context ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#variable_range_1}.
	 * @param ctx the parse tree
	 */
	void exitVariable_range_1(SASParser.Variable_range_1Context ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#variable_range_2}.
	 * @param ctx the parse tree
	 */
	void enterVariable_range_2(SASParser.Variable_range_2Context ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#variable_range_2}.
	 * @param ctx the parse tree
	 */
	void exitVariable_range_2(SASParser.Variable_range_2Context ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(SASParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(SASParser.VariableContext ctx);
}