/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified extensively by Alexander Mueller in 2019.
*/

package SASConverter;

import Models.*;
import SASParser.SASParserBaseVisitor;
import SASParser.SASParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.*;

public class TreeVisitor extends SASParserBaseVisitor<TransformBase>
{
    private HashMap<String, List<String>> file_variables;
    private HashMap<String, List<String>> dataset_variables;
    private HashMap<String, String> librefs = new HashMap<>();

    private TempDataStep data_step;
    private String last_data;   // the output of the most recent data step (useful for SET statements without arguments)

    public TreeVisitor(HashMap<String, List<String>> file_variables, HashMap<String, List<String>> dataset_variables)
    {
        this.file_variables = file_variables;
        this.dataset_variables = dataset_variables;
    }

    // remove the special characters that delineate the comment in SAS
    private String trimComment(String comment)
    {
        if(comment.startsWith("/*"))
            return comment.substring(2, comment.length() - 2);
        else
            return comment.substring(1, comment.length() - 1);
    }

    private String getFileExtension(String filename)
    {
        // TODO: check if file_variables contains a key of the form filename.extension
        return ".sas7bdat";
    }

    private String getFilenameFromPath(String path)
    {
        String[] path_array = path.contains("/") ? path.split("/") : path.split("\\\\");
        return path_array[path_array.length - 1];
    }

    private String getFileFromDSName(SASParser.Data_set_nameContext data_set_name)
    {
        // construct the full path to the file being loaded (unless libref is WORK)
        String file = data_set_name.dataset.getText();
        String libref = data_set_name.libref.getText().toUpperCase();

        String file_full = file + getFileExtension(file);

        if(!libref.equals("WORK"))
        {
            // if the directory contained Windows path separators, use those;
            // otherwise use the standard *nix path separators
            String directory = librefs.get(libref);
            return directory.contains("\\") ? directory + "\\" + file_full
                                            : directory + "/" + file_full;
        }
        else
        {
            return file;
        }
    }

    private static String removeQuotes(String s)
    {
        if(s.charAt(0) == '\"' || s.charAt(0) == '\'')
        {
            if(s.charAt(s.length() - 1) == '\"' || s.charAt(s.length() - 1) == '\'')
                return s.substring(1, s.length() - 1);
            else
                return s.substring(1);
        }
        else
            return s;
    }

    // returns a String containing the characters in common between first and second before the first difference
    private static String intersection(String first, String second)
    {
        String intersect = "";

        int first_length = first.length();
        int second_length = second.length();

        for(int x=0; (x < first_length) | (x < second_length); x++)
        {
            if(first.charAt(x) == second.charAt(x))
                intersect = intersect + first.charAt(x);
            else
                return intersect;
        }
        return intersect;
    }

    private List<ExpressionBase> handleValuesList(SASParser.Initial_value_listContext ctx)
    {
        ArrayList<ExpressionBase> values = new ArrayList<>();

        // if the list looks like (5 5 5 5 5 5 5 5 5 5)
        if(ctx.constant_sublist() != null)
        {
            for(int x=0; x < ctx.constant_sublist().constant().size(); x++)
            {
                values.add(parseConstant(ctx.constant_sublist().constant(x)));
            }
            return values;
        }

        int multiplier = Integer.parseInt(ctx.constant_iter_value.getText());

        // if the list looks like (10*5)
        if(ctx.constant_value != null)
        {
            for(int x=0; x < multiplier; x++)
            {
                ExpressionBase constant = parseConstant(ctx.constant_value);
                values.add(constant);
            }
            return values;
        }

        // if the list looks like (2*(5 2*(5 5)))

        List<ExpressionBase> inner_values = handleValuesList(ctx.initial_value_list());

        for(int x=0; x < multiplier; x++)
        {
            values.addAll(inner_values);
        }
        return values;
    }

    // for documentation on star group expansion, navigate to src/main/antlr/SASParser.g4 and search for "star_grouping"
    private List<List<String>> handleStarGrouping(SASParser.Star_groupingContext ctx)
    {
        ArrayList<String> left = new ArrayList<>();
        ArrayList<String> right = new ArrayList<>();
        ArrayList<List<String>> product = new ArrayList<>();

        if(ctx.left_star != null)
        {
            if(ctx.right_star != null)
            {
                List<List<String>> left_group = handleStarGrouping(ctx.left_star);
                List<List<String>> right_group = handleStarGrouping(ctx.right_star);

                for(List<String> left_strings : left_group)
                {
                    for(List<String> right_strings : right_group)
                    {
                        product.addAll(TempTypeList.expandStarGrouping(left_strings, right_strings));
                    }
                }
            }
            else
            {
                if(ctx.right_type != null)
                {
                    for(int x=0; x < ctx.right_type.variable().size(); x++)
                    {
                        right.add(ctx.right_type.variable(x).getText());
                    }
                }
                else
                {
                    right.add(ctx.right_var.getText());
                }

                List<List<String>> left_group = handleStarGrouping(ctx.left_star);
                for (List<String> strings : left_group)
                {
                    product.addAll(TempTypeList.expandStarGrouping(strings, right));
                }
            }
            return product;
        }
        else if(ctx.right_star != null)
        {
            if(ctx.left_type != null)
            {
                for(int x=0; x < ctx.left_type.variable().size(); x++)
                {
                    left.add(ctx.left_type.variable(x).getText());
                }
            }
            else
            {
                left.add(ctx.left_var.getText());
            }

            List<List<String>> right_group = handleStarGrouping(ctx.right_star);
            for (List<String> strings : right_group)
            {
                product.addAll(TempTypeList.expandStarGrouping(left, strings));
            }

            return product;
        }
        else
        {
            if(ctx.left_type != null)
            {
                for(int x=0; x < ctx.left_type.variable().size(); x++)
                {
                    left.add(ctx.left_type.variable(x).getText());
                }
            }
            else
            {
                left.add(ctx.left_var.getText());
            }

            if(ctx.right_type != null)
            {
                for(int x=0; x < ctx.right_type.variable().size(); x++)
                {
                    right.add(ctx.right_type.variable(x).getText());
                }
            }
            else
            {
                right.add(ctx.right_var.getText());
            }

            return TempTypeList.expandStarGrouping(left, right);
        }
    }

    public void print_Command_Tracker()
    {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String json = gson.toJson(this.data_step.commandTracker);

        try
        {
            JSONArray jsonArray = new JSONArray(json);
            System.out.println(jsonArray.toString(4));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }



    @Override
    public TransformBase visitParse(SASParser.ParseContext ctx)
    {
        return super.visitParse(ctx);
    }

    @Override
    public TransformBase visitChildren(RuleNode node)
    {
        CommandList commandList = new CommandList();

        int n = node.getChildCount();
        for (int i = 0; i < n; i++)
        {
            if (!shouldVisitNextChild(node, commandList))
            {
                break;
            }

            ParseTree c = node.getChild(i);
            TransformBase childResult = c.accept(this);

            if (childResult != null)
            {
                // Don't call AggregateResult if childResult is commandList
                if (!childResult.isCommandList())
                {
                    AggregateResult(commandList, childResult);
                }
                else
                {
                    CommandList resultList = (CommandList) childResult;
                    for (TransformBase b : resultList.commands)
                    {
                        if (b != null)
                        {
                            commandList.add(b);
                        }
                    }
                }
            }

        }

        return commandList;
    }

    private void AggregateResult(TransformBase aggregate, TransformBase nextResult)
    {
        CommandList mainList = (CommandList) aggregate;

        if (mainList != null)
        {
            mainList.add(nextResult);
        }
    }

    private List<String> expandVariableRange(String base, String first, String last)
    {
        ArrayList<String> files = new ArrayList<>();

        int start = Integer.parseInt(first);
        int end = Integer.parseInt(last);

        // for all values of x between start and end, add datax to the list of datasets
        for(int x=start; x <= end; x++)
        {
            // if the first dataset had leading zeroes, add the appropriate number of leading zeroes
            // to intermediate dataset names by comparing lengths
            if(first.startsWith("0"))
            {
                if(first.length() == last.length())
                {
                    String value = String.valueOf(x);
                    if(value.length() < first.length())
                    {
                        StringBuilder zeroes = new StringBuilder();
                        for(int y=0; y < (first.length() - value.length()); y++)
                        {
                            zeroes.append("0");
                        }
                        files.add(base + zeroes + value);
                    }
                }
            }
            else 
            {
                files.add(base + String.valueOf(x));
            }
        }

        return files;
    }

    // turn a list of variable names into a VariableListExpression
    private VariableListExpression makeVariableList(List<String> variables)
    {
        ArrayList<VariableReferenceBase> list = new ArrayList<>();

        for(String variable : variables)
        {
            list.add(new VariableSymbolExpression(variable));
        }

        return new VariableListExpression(list);
    }

    // Don't actually create a separate SDTL statement for ARRAY, but parse and store the array name and column names
    public TransformBase visitArray_statement(SASParser.Array_statementContext ctx)
    {
        TempArray array = new TempArray();

        array.name = ctx.array_name.getText();

        // store column names in a list of variables
        List<VariableReferenceBase> variables = new ArrayList<>();
        for(int x=0; x < ctx.array_elements().variable().size(); x++)
        {
            variables.add(new VariableSymbolExpression(ctx.array_elements().variable(x).getText()));
        }

        // store initial values in a list of expressions
        if(ctx.initial_value_list() != null)
        {
            List<ExpressionBase> values = handleValuesList(ctx.initial_value_list());
            array.values = new ValueListExpression(values);
        }

        array.variables = new VariableListExpression(variables);

        return array;
    }

    // Create Compute SDTL
    public TransformBase visitAssignment_statement(SASParser.Assignment_statementContext ctx)
    {
        Compute result = new Compute();

        if(ctx.variable() == null)
            return makeInvalid("Assignment statement is missing variable", ctx);
        if(ctx.expression() == null)
            return makeInvalid("Assignment statement is missing expression", ctx);

        result.variable = new VariableSymbolExpression(ctx.variable().getText());
        result.expression = parseExpression(ctx.expression());

        recordSourceInformation(result, ctx);
        return result;
    }

    // Parse ATTRIB (an ATTRIB statement can do several different things, each of which has a different SDTL command)
    public TransformBase visitAttrib_statement(SASParser.Attrib_statementContext ctx)
    {
        CommandList result = new CommandList();

        if(ctx.attributes().size() > 0)
        {
            for(int x=0; x < ctx.attributes().size(); x++)
            {
                ArrayList<VariableReferenceBase> variables = new ArrayList<>();

                // add all variables, variable ranges, or variable lists
                for(int y=0; y < ctx.attributes(x).variable_or_list().size(); y++)
                {
                    variables.add(parseVariableOrList(ctx.attributes(x).variable_or_list(y)));
                }

                // add the appropriate command for each attribute
                for(int y=0; y < ctx.attributes(x).attribute().size(); y++)
                {
                    if(ctx.attributes(x).attribute(y).FORMAT() != null)
                    {
                        SetDisplayFormat format = new SetDisplayFormat(ctx.attributes(x).attribute(y).display_format().getText());

                        format.variables = variables;

                        recordSourceInformation(format, ctx);
                        result.add(format);
                    }
                    if(ctx.attributes(x).attribute(y).INFORMAT() != null)
                    {
                        Unsupported unsupported = new Unsupported("Setting an informat is not currently supported by this application.");

                        recordSourceInformation(unsupported, ctx);
                        result.add(unsupported);
                    }
                    if(ctx.attributes(x).attribute(y).LABEL() != null)
                    {
                        String label_string = ctx.attributes(x).attribute(y).STRING().toString();

                        // remove single or double quotes from string
                        if ((label_string.charAt(0) == '\"') || (label_string.charAt(0) == '\''))
                            label_string = label_string.substring(1);
                        if ((label_string.charAt(label_string.length() - 1) == '\"') || (label_string.charAt(label_string.length() - 1) == '\''))
                            label_string = label_string.substring(0, label_string.length() - 1);

                        // make a separate Label statement for each variable because SDTL doesn't support multiple variables in one Label statement
                        for(VariableReferenceBase variable : variables)
                        {
                            SetVariableLabel label = new SetVariableLabel();
                            label.label = label_string;
                            label.variable = variable;

                            recordSourceInformation(label, ctx);
                            result.add(label);
                        }
                    }
                    if(ctx.attributes(x).attribute(y).LENGTH() != null)
                    {
                        Unsupported unsupported = new Unsupported("Setting a length option is not currently supported by this application.");

                        recordSourceInformation(unsupported, ctx);
                        result.add(unsupported);
                    }
                    if(ctx.attributes(x).attribute(y).transcode != null)
                    {
                        Unsupported unsupported = new Unsupported("Setting a transcode option is not currently supported by this application.");

                        recordSourceInformation(unsupported, ctx);
                        result.add(unsupported);
                    }
                }
            }
        }
        return result;
    }

    // Create Comment SDTL
    public TransformBase visitComment_statement(SASParser.Comment_statementContext ctx)
    {
        Comment result = new Comment();

        result.commentText = trimComment(ctx.getText());

        recordSourceInformation(result, ctx);
        return result;
    }

    // Don't actually create a separate SDTL statement for DATA, but parse and store the dataset name and options
    public TransformBase visitData_statement(SASParser.Data_statementContext ctx)
    {
        CommandList option_commands = new CommandList();
        for(int x=0; x < ctx.data_arguments().size(); x++)
        {
            TempData data;
            SASParser.Data_argumentsContext argCtx = ctx.data_arguments(x);
            if(argCtx.data_set_name().libref != null)
            {
                String filepath = getFileFromDSName(argCtx.data_set_name());
                if(filepath.equals("Active File"))
                {
                    // if the libref is WORK, then "WORK" should not be included in the dataset name
                    data = new TempData(argCtx.data_set_name().dataset.getText(), false);
                }
                else
                {
                    data = new TempData(filepath, false);
                }
            }
            else
            {
                data = new TempData(argCtx.data_set_name().getText(), false);
            }

            // handle dataset options, if present
            if(argCtx.data_set_options() != null)
            {
                for(int y=0; y < argCtx.data_set_options().size(); y++)
                {
                    data.has_options = true;
                    TransformBase option = parseDatasetOptions(argCtx.data_set_options(y));

                    if(option instanceof DropVariables)
                        data.delete = (DropVariables) option;
                    if(option instanceof KeepVariables)
                        data.keep = (KeepVariables) option;
                    if(option instanceof SetDatasetProperty)
                        data.properties.add((SetDatasetProperty) option);
                    if(option instanceof Rename)
                        data.rename = (Rename) option;
                    if(option instanceof KeepCases)
                        data.where = (KeepCases) option;
                }
            }

            option_commands.add(data);
        }
        return option_commands;
    }

    public TransformBase visitData_step_block(SASParser.Data_step_blockContext ctx)
    {
        // if there was a previous data step, record its name for future SET commands that don't have arguments
        if(data_step != null)
            last_data = data_step.getLastData();

        // perform initial parsing of dataset options and record dataset names
        CommandList option_commands = (CommandList) visitData_statement(ctx.data_statement());

        // instantiate or overwrite the current value of data_step with a new object to represent the current data step
        // (the method of instantiation depends on whether there are multiple output datasets in the DATA statement)
        if(option_commands.commands.size() == 1)
        {
            data_step = new TempDataStep(((TempData) option_commands.commands.get(0)).dataset_name, this.file_variables, this.dataset_variables);
        }
        else
        {
            ArrayList<String> output_names = new ArrayList<>();
            for(int x=0; x < option_commands.commands.size(); x++)
            {
                output_names.add(((TempData) option_commands.commands.get(x)).dataset_name);
            }
            data_step = new TempDataStep(output_names, this.file_variables, this.dataset_variables);
        }

        // parse each command in the data step and add it to the list of data commands
        for(int x=0; x < ctx.statement().size(); x++)
        {
            TransformBase child = visitChildren(ctx.statement(x));
            if(!child.isCommandList())
            {
                data_step.addCommandToTracker(child);
            }
            else
            {
                CommandList childCommands = (CommandList) child;
                for(TransformBase command : childCommands.commands)
                {
                    data_step.addCommandToTracker(command);
                }
            }
        }

        // add dataset option commands for each output dataset
        for(int x=0; x < option_commands.commands.size(); x++)
        {
            data_step.addCommandToTracker(option_commands.commands.get(x));
        }

        // add an Execute command if the data step has a RUN statement
        if(ctx.run_statement() != null)
        {
            Execute execute = new Execute();
            recordSourceInformation(execute, ctx.run_statement());
            data_step.addCommandToTracker(execute);
        }

        // make sure the last relevant command produces the output dataframes of the DATA statement
        data_step.produceOutputDataframes();

        // update the global variable lists
        this.dataset_variables = data_step.dataset_variables;
        this.file_variables = data_step.file_variables;

        // return all the SDTL commands resulting from this data step
        return new CommandList(data_step.commandTracker);
    }

    // process commands in a DO block
    public TransformBase visitDo_statement(SASParser.Do_statementContext ctx)
    {
        //TODO: handle WHILE and UNTIL options
        LoopOverList loop = new LoopOverList();

        String index_variable = ctx.index_variable.getText();

        // make an IteratorDescription for each item in the comma-separated list
        ArrayList<IteratorDescription> descriptions = new ArrayList<>();
        for(int x=0; x < ctx.do_spec().size(); x++)
        {
            IteratorDescription description = new IteratorDescription(index_variable);
            ArrayList<ExpressionBase> values = new ArrayList<>();

            // if STOP is included, the index is explicitly a numeric range
            if(ctx.do_spec(x).stop != null)
            {
                NumberRangeExpression range = new NumberRangeExpression();

                range.numberRangeStart = parseExpression(ctx.do_spec(x).start);
                range.numberRangeEnd = parseExpression(ctx.do_spec(x).stop);

                if(ctx.do_spec(x).increment != null)
                {
                    range.numberRangeIncrement = parseExpression(ctx.do_spec(x).increment);
                }
                else
                {
                    range.numberRangeIncrement = new NumericConstantExpression("1");
                }

                values.add(range);
            }
            // otherwise, this index could be a numeric constant, a character constant, or a variable
            else
            {
                ExpressionBase value = parseExpression(ctx.do_spec(x).start);
                values.add(value);
            }

            description.iteratorValues = values;

            descriptions.add(description);
        }

        loop.iterators = descriptions;

        // parse each command within the DO block
        //nests++;  // make sure subcommands aren't added to the command tracker separately
        ArrayList<TransformBase> commands = new ArrayList<>();
        for(int x=0; x < ctx.statement().size(); x++)
        {
            TransformBase child = visitChildren(ctx.statement(x));
            if(!child.isCommandList())
            {
                commands.add(child);
            }
            else
            {
                CommandList childCommands = (CommandList) child;
                commands.addAll(childCommands.commands);
            }
        }
        //nests--;

        loop.commands = commands;

        recordSourceInformation(loop, ctx);
        return loop;
    }

    // Create DropVariables SDTL
    public TransformBase visitDrop_statement(SASParser.Drop_statementContext ctx)
    {
        DropVariables result = new DropVariables();

        // check for a list of variables and add them one at a time
        for (int x=0; x < ctx.variable_or_list().size(); x++)
        {
            result.variables.add(parseVariableOrList(ctx.variable_or_list(x)));
        }

        // if no variables were added to the list, this is an invalid drop command in SAS
        if(result.variables.isEmpty())
        {
            return makeInvalid("DROP statement missing variable or variable-list", ctx);
        }

        recordSourceInformation(result, ctx);
        return result;
    }

    // Create Load STDL
    public TransformBase visitFilename_statement(SASParser.Filename_statementContext ctx)
    {
        if(ctx.fileref == null)
        {
            return makeInvalid("FILENAME statement missing fileref", ctx);
        }

        Load result = new Load(removeQuotes(ctx.filename.getText()));

        result.producesDataframe = ctx.fileref.getText();

        recordSourceInformation(result, ctx);
        return result;
    }

    // Create SetDisplayFormat SDTL
    public TransformBase visitFormat_statement(SASParser.Format_statementContext ctx)
    {
        // if this is a format list, return a CommandList of separate SetDisplayFormat commands
        if(ctx.format().size() > 0)
        {
            CommandList result = new CommandList();
            for(int x=0; x < ctx.format().size(); x++)
            {
                if(ctx.format(x).display_format() != null)
                {
                    SetDisplayFormat format = new SetDisplayFormat(ctx.format(x).display_format().getText());
                    ArrayList<VariableReferenceBase> variables = new ArrayList<>();

                    for(int y=0; y < ctx.format(x).variable_or_list().size(); y++)
                    {
                        variables.add(parseVariableOrList(ctx.format(x).variable_or_list(y)));
                    }

                    format.variables = variables;

                    recordSourceInformation(format, ctx);
                    result.add(format);
                }
                else
                {
                    result.add(new Unsupported("Formats set with the DEFAULT= option in SAS do not affect the output data and are thus ignored here."));
                }
            }
            return result;
        }

        // otherwise return a single SetDisplayFormat command
        else
        {
            if(ctx.display_format() != null)
            {
                ArrayList<VariableReferenceBase> variables = new ArrayList<>();

                for (int y = 0; y < ctx.variable_or_list().size(); y++)
                {
                    variables.add(parseVariableOrList(ctx.variable_or_list(y)));
                }

                SetDisplayFormat displayFormat = new SetDisplayFormat(ctx.display_format().getText());
                displayFormat.variables = variables;

                if(ctx.default_format() == null)
                {
                    recordSourceInformation(displayFormat, ctx);
                    return displayFormat;
                }
                else
                {
                    CommandList result = new CommandList();
                    result.add(displayFormat);
                    result.add(new Unsupported("Formats set with the DEFAULT= option in SAS do not affect the output data and are thus ignored here."));
                    return result;
                }
            }
            // if neither display format nor default format are present, this format statement is dissociative
            else
            {
                SetDisplayFormat result = new SetDisplayFormat("");

                ArrayList<VariableReferenceBase> variables = new ArrayList<>();

                for(int y=0; y < ctx.variable_or_list().size(); y++)
                {
                    variables.add(parseVariableOrList(ctx.variable_or_list(y)));
                }

                result.variables = variables;

                recordSourceInformation(result, ctx);
                return result;
            }
        }
    }

    // Create IfRows SDTL
    public TransformBase visitIf_statement(SASParser.If_statementContext ctx)
    {
        // determine whether the condition is a logical expression or just an ordinary expression
        ExpressionBase condition = ctx.implicit_condition == null ? parseLogicalExpression(ctx.condition)
                                                                  : parseImplicitCondition(ctx.implicit_condition);

        // if there's a then command, then this is a regular IF statement
        if(ctx.then_command != null)
        {
            IfRows rows = new IfRows(condition);

            List<TransformBase> then_commands = new ArrayList<>();
            List<TransformBase> else_commands = new ArrayList<>();

            //nests++;

            CommandList then_list = (CommandList) visitChildren(ctx.then_command);
            then_commands.addAll(then_list.commands);

            rows.thenCommands = then_commands;

            // add else command only if an else block is actually present
            if(ctx.else_command != null)
            {
                CommandList else_list = (CommandList) visitChildren(ctx.else_command);
                else_commands.addAll(else_list.commands);
                rows.elseCommands = else_commands;
            }
            //nests--;

            recordSourceInformation(rows, ctx);
            return rows;
        }

        // otherwise, it's a subsetting IF statement, which should be treated as a KeepCases
        return new KeepCases(condition);
    }

    // Create Keep SDTL
    public TransformBase visitKeep_statement(SASParser.Keep_statementContext ctx)
    {
        KeepVariables result = new KeepVariables();

        // check for a list of variables and add them one at a time
        for (int x=0; x < ctx.variable_or_list().size(); x++)
        {
            result.variables.add(parseVariableOrList(ctx.variable_or_list(x)));
        }

        // if no variables were added to the list, this is an invalid keep command in SAS
        if(result.variables.isEmpty())
        {
            return makeInvalid("KEEP statement missing variable or variable-list", ctx);
        }

        recordSourceInformation(result, ctx);
        return result;
    }

    // Create Label SDTL
    public TransformBase visitLabel_statement(SASParser.Label_statementContext ctx)
    {
        CommandList result = new CommandList();

        for (int x=0; x < ctx.label_part().size(); x++)
        {
            SetVariableLabel variableLabel = new SetVariableLabel();

            if(ctx.label_part(x).variable().isEmpty())
                return makeInvalid("LABEL statement missing variable", ctx);

            variableLabel.variable = new VariableSymbolExpression(ctx.label_part(x).variable(0).getText());

            if(ctx.label_part(x).STRING() == null && ctx.label_part(x).variable().size() < 2)
                return makeInvalid("LABEL statement missing label", ctx);

            TerminalNode stringNode = ctx.label_part(x).STRING();
            StringBuilder labelBuilder = new StringBuilder();
            for(int y = 1; y < ctx.label_part(x).variable().size(); y++)
            {
                labelBuilder.append(ctx.label_part(x).variable(y).getText());
                if(y < ctx.label_part(x).variable().size() - 1)
                    labelBuilder.append(" ");   // whitespace matters for these types of labels
            }
            String label = labelBuilder.toString();
            if(stringNode != null)
            {
                label = stringNode.toString();
                // remove single or double quotes from string
                if ((label.charAt(0) == '\"') || (label.charAt(0) == '\''))
                    label = label.substring(1);
                if ((label.charAt(label.length() - 1) == '\"') || (label.charAt(label.length() - 1) == '\''))
                    label = label.substring(0, label.length() - 1);
            }
            variableLabel.label = label;

            recordSourceInformation(variableLabel, ctx);
            result.add(variableLabel);
        }

        // if no commands were added to the list, then this label statement is invalid
        if(result.commands.size() == 0)
            return makeInvalid("LABEL statement missing variable and label", ctx);

        return result;
    }

    // associate a libname with a libref, or append datasets if multiple are listed
    // TODO: handle the USER libname
    public TransformBase visitLibname_statement(SASParser.Libname_statementContext ctx)
    {
        if(ctx.CLEAR() != null)
        {
            //TODO
        }
        else if(ctx.LIST() != null)
        {
            //TODO
        }
        else if(ctx.libref != null)
        {
            if(ctx.sas_library != null)
            {
                // TODO: handle libref options
                String libref = ctx.libref.getText().toUpperCase();         // SAS has case-insensitive library names
                String filepath = removeQuotes(ctx.sas_library.getText());

                librefs.put(libref, filepath);
            }
            else
            {
                // library concatenation is currently out of scope for this project
                // if it ever comes into scope, this is where it should be handled
            }
        }
        else
        {
            return makeInvalid("Error: LIBNAME statement missing libref", ctx);
        }
        return null;
    }

    // Create Merge SDTL
    public TransformBase visitMerge_statement(SASParser.Merge_statementContext ctx)
    {
        MergeDatasets merge = makeMergeStatement(ctx.set_arguments(),"merge",null,null, ctx.by_statement());
        recordSourceInformation(merge, ctx);
        return merge;
    }

    public TransformBase visitModify_statement(SASParser.Modify_statementContext ctx)
    {
        MergeDatasets merge = makeMergeStatement(ctx.set_arguments(),"modify", ctx.modify_options(), ctx.change_options(), ctx.by_statement());
        recordSourceInformation(merge, ctx);
        return merge;
    }

    // Create SetMissingValues SDTL
    public TransformBase visitMissing_statement(SASParser.Missing_statementContext ctx)
    {
        SetMissingValues result = new SetMissingValues();

        for (int x=0; x < ctx.CHARACTER().size(); x++)
        {
            MissingValueConstantExpression missing = new MissingValueConstantExpression(ctx.CHARACTER(x).getText());

            result.values.add(missing);
        }

        recordSourceInformation(result, ctx);
        return result;
    }

    public TransformBase visitProc_step(SASParser.Proc_stepContext ctx)
    {
        List<TransformBase> commands = new ArrayList<>();

        TransformBase child = visitChildren(ctx.proc_statement());
        if(child.isCommandList())
        {
            commands.addAll(((CommandList) child).commands);
        }
        else
        {
            commands.add(child);
        }

        if(ctx.run_statement() != null)
        {
            Execute execute = new Execute();
            recordSourceInformation(execute, ctx.run_statement());
            commands.add(execute);
        }

        return new CommandList(commands);
    }

    // PROC APPEND is *almost* the same as PROC DATASETS' APPEND statement, so we combine their parsing
    public TransformBase visitProc_append_statement(SASParser.Proc_append_statementContext ctx)
    {
        return makeAppendStatement(true, ctx.append_statement());
    }

    // PROC MEANS and PROC SUMMARY are the same except for how they treat output, so handle them together
    public TransformBase visitProc_means_statement(SASParser.Proc_means_statementContext ctx)
    {
        return makeCollapseStatement(true, ctx.proc_means_summary());
    }
    public TransformBase visitProc_summary_statement(SASParser.Proc_summary_statementContext ctx)
    {
        return makeCollapseStatement(false, ctx.proc_means_summary());
    }

    // Create Rename SDTL
    public TransformBase visitRename_statement(SASParser.Rename_statementContext ctx)
    {
        Rename result = new Rename();

        for (int i = 0; i < ctx.rename_pair().size(); i++)
        {
            RenamePair pair = new RenamePair();

            if(ctx.rename_pair(i).old_name == null)
                return makeInvalid("RENAME statement missing old name", ctx);
            if(ctx.rename_pair(i).new_name == null)
                return makeInvalid("RENAME statement missing new name", ctx);

            pair.oldVariable = new VariableSymbolExpression(ctx.rename_pair(i).old_name.getText());
            pair.newVariable = new VariableSymbolExpression(ctx.rename_pair(i).new_name.getText());

            result.renames.add(pair);
        }

        // if the list is empty, this is an invalid rename command
        if(result.renames.isEmpty())
            return makeInvalid("RENAME statement missing old name and new name", ctx);

        recordSourceInformation(result, ctx);
        return result;
    }

    // similar to visitData_statement, except the dataset name represents input data rather than output data
    public TransformBase visitSet_statement(SASParser.Set_statementContext ctx)
    {
        // if there are no arguments, the input dataset is assumed to be the previous output dataset
        if(ctx.set_arguments() == null)
        {
            return new TempData(last_data, true);
        }
        else if(ctx.set_arguments().size() == 1)
        {
            // do as much handling of dataset options as is possible without knowing whether it's a list
            CommandList option_commands = new CommandList();
            if(ctx.set_arguments(0).data_set_options() != null)
            {
                for(int x=0; x < ctx.set_arguments(0).data_set_options().size(); x++)
                {
                    TransformBase command = parseDatasetOptions(ctx.set_arguments(0).data_set_options(x));
                    option_commands.add(command);
                }
            }
            // branch based on whether this argument is a single dataset or a list of datasets
            SASParser.Data_set_nameContext data_set_name = ctx.set_arguments(0).data_set_name();
            if(data_set_name.variable_or_list() != null)
            {
                VariableReferenceBase dataset_names = parseVariableOrList(data_set_name.variable_or_list());
                // if there's one argument and it isn't a list, this argument is the name of the input dataset
                if(dataset_names instanceof VariableSymbolExpression)
                {
                    //TODO: handle sequential merge if previous statement was also SET
                    String name = ((VariableSymbolExpression) dataset_names).variableName;
                    TempData data = new TempData(name, true);
                    data.has_options = !option_commands.commands.isEmpty();

                    for(TransformBase option : option_commands.commands)
                    {
                        if(option instanceof DropVariables)
                            data.delete = (DropVariables) option;
                        if(option instanceof KeepVariables)
                            data.keep = (KeepVariables) option;
                        if(option instanceof SetDatasetProperty)
                            data.properties.add((SetDatasetProperty) option);
                        if(option instanceof Rename)
                            data.rename = (Rename) option;
                        if(option instanceof KeepCases)
                            data.where = (KeepCases) option;
                    }

                    return data;
                }
                // if the argument is a list (e.g. data1-data8), add each element of the list as a separate dataset
                if(dataset_names instanceof VariableListExpression)
                {
                    AppendDatasets datasets = new AppendDatasets();
                    ArrayList<String> dataframes_consumed = new ArrayList<>();

                    for(int y=0; y < ((VariableListExpression) dataset_names).variables.size(); y++)
                    {
                        String filename = ((VariableSymbolExpression) ((VariableListExpression) dataset_names).variables.get(y)).variableName;
                        dataframes_consumed.add(filename);
                        datasets.appendFile(filename, option_commands.commands);
                    }

                    datasets.consumesDataframe = dataframes_consumed;

                    recordSourceInformation(datasets, ctx);
                    return datasets;
                }
            }
            else if(data_set_name.libref != null)
            {
                String filepath = getFileFromDSName(data_set_name);
                if(!filepath.contains(".sas7bdat"))
                {
                    // if the libref is WORK, treat this statement as if no libref had been specified
                    TempData data = new TempData(data_set_name.dataset.getText(), true);
                    data.has_options = !option_commands.commands.isEmpty();

                    for(TransformBase option : option_commands.commands)
                    {
                        if(option instanceof DropVariables)
                            data.delete = (DropVariables) option;
                        if(option instanceof KeepVariables)
                            data.keep = (KeepVariables) option;
                        if(option instanceof SetDatasetProperty)
                            data.properties.add((SetDatasetProperty) option);
                        if(option instanceof Rename)
                            data.rename = (Rename) option;
                        if(option instanceof KeepCases)
                            data.where = (KeepCases) option;
                    }

                    return data;
                }
                else
                {
                    Load load = new Load(filepath);
                    recordSourceInformation(load, ctx);
                    return load;
                }
            }
            else
            {
                Load load = new Load(data_set_name.STRING().getText());

                recordSourceInformation(load, ctx);
                return load;
            }
        }
        // if there are 2 or more arguments, they are being concatenated (i.e. appended) into one dataset
        else
        {
            CommandList commands = new CommandList();
            AppendDatasets datasets = new AppendDatasets();
            ArrayList<String> dataframes_consumed = new ArrayList<>();

            // add each dataset to the list of files
            for(int x=0; x < ctx.set_arguments().size(); x++)
            {
                // create half-complete commands from the relevant dataset options, if present
                CommandList option_commands = new CommandList();
                if(ctx.set_arguments(0).data_set_options() != null)
                {
                    for(int y=0; y < ctx.set_arguments(x).data_set_options().size(); y++)
                    {
                        TransformBase command = parseDatasetOptions(ctx.set_arguments(x).data_set_options(y));
                        option_commands.add(command);
                    }
                }
                SASParser.Data_set_nameContext data_set_name = ctx.set_arguments(x).data_set_name();
                if(data_set_name.variable_or_list() != null)
                {
                    VariableReferenceBase dataset_names = parseVariableOrList(data_set_name.variable_or_list());
                    // if this is one dataset, add it to the list
                    if(dataset_names instanceof VariableSymbolExpression)
                    {
                        String filename = ((VariableSymbolExpression) dataset_names).variableName;
                        dataframes_consumed.add(filename);
                        datasets.appendFile(filename, option_commands.commands);
                    }
                    // otherwise, do the same dataset list processing as above
                    if(dataset_names instanceof VariableListExpression)
                    {
                        for(int y=0; y < ((VariableListExpression) dataset_names).variables.size(); y++)
                        {
                            String filename = ((VariableSymbolExpression) ((VariableListExpression) dataset_names).variables.get(y)).variableName;
                            dataframes_consumed.add(filename);
                            datasets.appendFile(filename, option_commands.commands);
                        }
                    }
                }
                else if(data_set_name.libref != null)
                {
                    String filepath = getFileFromDSName(data_set_name);
                    if(!filepath.contains(".sas7bdat"))
                    {
                        dataframes_consumed.add(data_set_name.dataset.getText());
                    }
                    datasets.appendFile(filepath, option_commands.commands);
                }
            }

            if(!dataframes_consumed.isEmpty())
                datasets.consumesDataframe = dataframes_consumed;

            recordSourceInformation(datasets, ctx);
            commands.add(datasets);

            // if there's a BY group, be sure to add the appropriate SortCases command
            // to reflect the fact that SAS sorts the dataset resulting from the append
            if(ctx.by_statement() != null)
            {
                SortCases cases = (SortCases) makeSortStatement(ctx.by_statement().sort_direction());

                recordSourceInformation(cases, ctx);
                commands.add(cases);
            }

            return commands;
        }

        return null; // this should be an unreachable state, but just in case, returning null signifies a bug
    }

    public TransformBase visitUpdate_statement(SASParser.Update_statementContext ctx)
    {
        List<SASParser.Set_argumentsContext> set_arguments = Arrays.asList(ctx.master, ctx.transaction);
        MergeDatasets merge = makeMergeStatement(set_arguments,"update", null, ctx.change_options(), ctx.by_statement());
        recordSourceInformation(merge, ctx);
        return merge;
    }

    // make KeepCases SDTL
    public TransformBase visitWhere_statement(SASParser.Where_statementContext ctx)
    {
        ExpressionBase condition = ctx.where_expression() != null ? parseWhereExpression(ctx.where_expression())
                                                                  : parseCompoundWhere(ctx.compound_where());
        data_step.last_where = condition;

        KeepCases keep = new KeepCases(condition);
        recordSourceInformation(keep, ctx);
        return keep;
    }

    // handles both PROC APPEND and PROC DATASETS' APPEND statement
    private TransformBase makeAppendStatement(boolean proc, SASParser.Append_statementContext ctx)
    {
        CommandList commands = new CommandList();
        AppendDatasets datasets = new AppendDatasets();
        ArrayList<String> dataframes_consumed = new ArrayList<>();
        String saved_dataset = null;
        List<String> active_variables = new ArrayList<>();

        if(ctx.base != null)
        {
            if(ctx.base.variable_or_list() != null)
            {
                VariableReferenceBase variable = parseVariableOrList(ctx.base.variable_or_list());
                if(variable instanceof VariableSymbolExpression)
                {
                    String filename = ((VariableSymbolExpression) variable).variableName;
                    datasets.appendFile(filename);
                    dataframes_consumed.add(filename);
                    active_variables.addAll(dataset_variables.get(filename));
                }
                else
                {
                    return makeInvalid("Error: variable lists are not allowed as input to an APPEND statement", ctx);
                }
            }
            else if(ctx.base.libref != null)
            {
                String filepath = getFileFromDSName(ctx.base);
                String filename = getFilenameFromPath(filepath);
                if(!filepath.contains(".sas7bdat"))
                {
                    dataframes_consumed.add(ctx.base.dataset.getText());
                    active_variables.addAll(dataset_variables.get(filename));
                }
                else
                {
                    saved_dataset = filepath;
                    active_variables.addAll(file_variables.get(filename));
                }
                datasets.appendFile(filepath);
            }
            else
            {
                saved_dataset = ctx.base.STRING().getText();
                datasets.appendFile(saved_dataset);
                active_variables.addAll(file_variables.get(getFilenameFromPath(saved_dataset)));
            }
            if(ctx.data != null)
            {
                //TODO: handle GETSORT and FORCE options, as well as the libref difference between PROC and not PROC
                if(ctx.data.variable_or_list() != null)
                {
                    VariableReferenceBase variable = parseVariableOrList(ctx.data.variable_or_list());
                    if(variable instanceof VariableSymbolExpression)
                    {
                        String filename = ((VariableSymbolExpression) variable).variableName;
                        datasets.appendFile(filename);
                        dataframes_consumed.add(filename);
                        for(String dataset_variable : dataset_variables.get(filename))
                        {
                            if(!active_variables.contains(dataset_variable))
                                active_variables.add(dataset_variable);
                        }
                    }
                    else
                    {
                        return makeInvalid("Error: variable lists are not allowed as input to an APPEND statement", ctx);
                    }
                }
                else if(ctx.data.libref != null)
                {
                    String filepath = getFileFromDSName(ctx.data);
                    String filename = getFilenameFromPath(filepath);
                    if(!filepath.contains(".sas7bdat"))
                    {
                        dataframes_consumed.add(ctx.data.dataset.getText());
                        for(String dataset_variable : dataset_variables.get(filename))
                        {
                            if(!active_variables.contains(dataset_variable))
                                active_variables.add(dataset_variable);
                        }
                    }
                    else
                    {
                        for(String file_variable : file_variables.get(filename))
                        {
                            if(!active_variables.contains(file_variable))
                                active_variables.add(file_variable);
                        }
                    }
                    datasets.appendFile(filepath);
                }
                else
                {
                    datasets.appendFile(ctx.data.STRING().getText());
                    for(String file_variable : file_variables.get(ctx.data.STRING().getText()))
                    {
                        if(!active_variables.contains(file_variable))
                            active_variables.add(file_variable);
                    }
                }
            }
            // if no dataset is specified in a DATA clause, the default is the output of the previous data step
            else if(last_data != null)
            {
                // if the output of the previous data step was a filepath, treat accordingly
                if(last_data.contains("/") || last_data.contains("\\"))
                {
                    datasets.appendFile(last_data);
                    for(String file_variable : file_variables.get(getFilenameFromPath(last_data)))
                    {
                        if(!active_variables.contains(file_variable))
                            active_variables.add(file_variable);
                    }
                }
                // otherwise, the file in AppendFileDescription should be "Active File"
                else
                {
                    datasets.appendFile(last_data);
                    dataframes_consumed.add(last_data);
                    for(String dataset_variable : dataset_variables.get(last_data))
                    {
                        if(!active_variables.contains(dataset_variable))
                            active_variables.add(dataset_variable);
                    }
                }
            }
            else
            {
                return makeInvalid("Error: most recently created dataframe cannot be inferred before a data step", ctx);
            }
        }
        else
        {
            return makeInvalid("Error: APPEND statement missing base dataframe", ctx);
        }

        datasets.consumesDataframe = dataframes_consumed;
        recordSourceInformation(datasets, ctx);

        if(saved_dataset != null)
        {
            datasets.producesDataframe = "_activeDataframe_0";
            recordSourceInformation(datasets, ctx);

            Save save = new Save();
            save.fileName = saved_dataset;
            save.consumesDataframe = "_activeDataframe_0";
            recordSourceInformation(save, ctx);

            file_variables.put(saved_dataset, active_variables);

            commands.add(datasets);
            commands.add(save);
        }
        else
        {
            datasets.producesDataframe = ctx.base.getText();    // TODO: check whether this contains "WORK"
            recordSourceInformation(datasets, ctx);

            dataset_variables.put(ctx.base.getText(), active_variables);

            commands.add(datasets);
        }

        return commands;
    }

    // TODO: implement having both BY and CLASS variables
    private TransformBase makeCollapseStatement(boolean means, SASParser.Proc_means_summaryContext ctx)
    {
        CommandList commands = new CommandList();
        Collapse collapse = new Collapse();

        List<String> active_variables = new ArrayList<>();

        String input_dataset = null;

        if(ctx.proc_means_options() != null)
        {
            for(int x=0; x < ctx.proc_means_options().size(); x++)
            {
                if(ctx.proc_means_options(x).dataset != null)
                {
                    input_dataset = ctx.proc_means_options(x).dataset.getText();
                }
            }
        }
        
        // shorthand variables to declutter the incessant null checking required later
        SASParser.By_statementContext by_statement = null;
        SASParser.Class_statementContext class_statement = null;
        SASParser.Output_statementContext output_statement = null;
        SASParser.Types_statementContext types_statement = null;
        SASParser.Var_statementContext var_statement = null;
        SASParser.Ways_statementContext ways_statement = null;
        
        for(int x=0; x < ctx.collapse_statements().size(); x++)
        {
            if(ctx.collapse_statements(x).by_statement() != null)
                by_statement = ctx.collapse_statements(x).by_statement();
            if(ctx.collapse_statements(x).class_statement() != null)
                class_statement = ctx.collapse_statements(x).class_statement();
            if(ctx.collapse_statements(x).output_statement() != null)
                output_statement = ctx.collapse_statements(x).output_statement();
            if(ctx.collapse_statements(x).types_statement() != null)
                types_statement = ctx.collapse_statements(x).types_statement();
            if(ctx.collapse_statements(x).var_statement() != null)
                var_statement = ctx.collapse_statements(x).var_statement();
            if(ctx.collapse_statements(x).ways_statement() != null)
                ways_statement = ctx.collapse_statements(x).ways_statement();
        }

        // parse analysis variables, if explicitly defined
        ArrayList<VariableSymbolExpression> analysis_variables = new ArrayList<>();
        if(var_statement != null)
        {
            for(int x=0; x < var_statement.variable().size(); x++)
            {
                analysis_variables.add(new VariableSymbolExpression(var_statement.variable(x).getText()));
            }
        }
        else
        {
            // TODO: handle the case where we need "all numeric analysis variables"
        }

        ArrayList<Compute> aggregations = new ArrayList<>();

        if(output_statement != null)
        {
            // if there's an output dataset specified, store it in the appropriate property
            if(output_statement.output_dataset() != null)
            {
                SASParser.Data_set_nameContext nameCtx = output_statement.output_dataset().data_set_name();
                if(nameCtx.variable_or_list() != null)
                {
                    VariableReferenceBase varx = parseVariableOrList(nameCtx.variable_or_list());
                    if(varx instanceof VariableSymbolExpression)
                        collapse.outputDatasetName = ((VariableSymbolExpression) varx).variableName;
                    else
                        ;   // TODO: handle what should be an unreachable state but probably isn't...
                }
            }

            // for each output specification, parse all of the assignments, variables, and function calls
            for(int x=0; x < output_statement.output_part().size(); x++)
            {
                // store the input variables in a list, if present
                ArrayList<String> input_variable_names = new ArrayList<>();
                if(output_statement.output_part(x).output_variables() != null)
                {
                    for(int y=0; y < output_statement.output_part(x).output_variables().variable().size(); y++)
                    {
                        input_variable_names.add(output_statement.output_part(x).output_variables().variable(y).getText());
                    }
                }

                // store the output variables in a list, if present
                ArrayList<String> output_variable_names = new ArrayList<>();
                if(output_statement.output_part(x).output_name() != null)
                {
                    for(int y=0; y < output_statement.output_part(x).output_name().size(); y++)
                    {
                        output_variable_names.add(output_statement.output_part(x).output_name(y).getText());
                    }
                }
                else
                {
                    if(var_statement != null)
                    {
                        for (VariableSymbolExpression analysis_variable : analysis_variables)
                        {
                            output_variable_names.add(analysis_variable.variableName);
                        }
                    }
                    // TODO: add the appropriate else block
                }

                // if this output specification has a statistic keyword, make the appropriate function call
                if(output_statement.output_part(x).statistic_keyword() != null)
                {
                    String function_name = output_statement.output_part(x).statistic_keyword().getText().toUpperCase();
                    String sdtl_function_name = "";
                    String exp2_value = null;
                    switch(function_name)
                    {
                        case "CSS":
                        case "CV":
                        case "LCLM":
                        case "MAX":
                        case "MEAN":
                        case "MIN":
                        case "MODE":
                        case "RANGE":
                        case "UCLM":
                        case "USS":
                        case "VAR":
                        case "MEDIAN":
                        case "KURT":
                        case "SKEW":
                        case "STD":
                        case "STDERR":
                        case "SUMWGT":
                        case "QRANGE":
                            sdtl_function_name = getSDTLFunctionName(function_name);
                            break;
                        case "KURTOSIS":
                            sdtl_function_name = "col_kurt";
                            break;
                        case "SKEWNESS":
                            sdtl_function_name = "col_skew";
                            break;
                        case "STDDEV":
                            sdtl_function_name = "col_sd";
                            break;
                        case "P1":
                        case "P5":
                        case "P10":
                        case "P25":
                        case "P50":
                        case "P75":
                        case "P90":
                        case "P95":
                        case "P99":
                            sdtl_function_name = "col_pctile";
                            exp2_value = function_name.substring(1);
                            break;
                        case "Q1":
                            sdtl_function_name = "col_pctile";
                            exp2_value = "25";
                            break;
                        case "Q3":
                            sdtl_function_name = "col_pctile";
                            exp2_value = "75";
                            break;
                        case "N":                   //UNKNOWN WHETHER WEIGHTED OR UNWEIGHTED
                            sdtl_function_name = "col_count";
                            break;
                        case "NMISS": break;        //UNKNOWN WHETHER WEIGHTED OR UNWEIGHTED
                        case "SUM": break;          //NO EQUIVALENT FOR WEIGHTED OPTION
                        case "CLM": break;          //NOT IN FUNCTION LIBRARY
                        case "PROBT": break;        //NOT IN FUNCTION LIBRARY
                        case "PRT": break;          //NOT IN FUNCTION LIBRARY
                        case "T": break;            //NOT IN FUNCTION LIBRARY
                    }
                    if(!analysis_variables.isEmpty())
                    {
                        for(int y=0; y < analysis_variables.size(); y++)
                        {
                            Compute compute = new Compute();

                            FunctionCallExpression function = new FunctionCallExpression(sdtl_function_name);
                            function.addArgument(analysis_variables.get(y));

                            if(exp2_value != null)
                            {
                                function.addArgument(new StringConstantExpression(exp2_value));
                            }

                            compute.expression = function;
                            compute.variable = new VariableSymbolExpression(output_variable_names.get(y));

                            active_variables.add(output_variable_names.get(y));

                            // we don't need source information because this isn't a normal compute command
                            // (the same can be said of consumesDataframe and producesDataframe,
                            //  but those are null by default so we don't need to explicitly set them to null)
                            compute.sourceInformation = null;

                            aggregations.add(compute);
                        }
                    }
                    else if(!input_variable_names.isEmpty())
                    {
                        for(int y=0; y < input_variable_names.size(); y++)
                        {
                            Compute compute = new Compute();

                            FunctionCallExpression function = new FunctionCallExpression(sdtl_function_name);
                            function.addArgument(new VariableSymbolExpression(input_variable_names.get(y)));

                            if(exp2_value != null)
                            {
                                function.addArgument(new StringConstantExpression(exp2_value));
                            }

                            compute.expression = function;
                            compute.variable = new VariableSymbolExpression(output_variable_names.get(y));
                            compute.sourceInformation = null;

                            active_variables.add(output_variable_names.get(y));

                            aggregations.add(compute);
                        }
                    }
                }
            }

            // if there is no output specification, then SAS calculates five default statistics: n, min, max, mean, std
            if(output_statement.output_part().isEmpty())
            {
                String[] default_stats_sas = { "N", "MIN", "MAX", "MEAN", "STD" };
                String[] default_stats_sdtl = { "col_count", "col_min", "col_max", "col_mean", "col_sd" };
                for(VariableSymbolExpression variable : analysis_variables)
                {
                    for(int x=0; x < 5; x++)
                    {
                        Compute aggregation = new Compute();
                        aggregation.variable = new VariableSymbolExpression(default_stats_sas[x]);

                        active_variables.add(default_stats_sas[x]);

                        FunctionCallExpression function = new FunctionCallExpression(default_stats_sdtl[x]);
                        function.addArgument(variable);

                        aggregation.expression = function;
                        aggregation.sourceInformation = null;

                        aggregations.add(aggregation);
                    }
                }
            }

            collapse.aggregateVariables = aggregations;
        }

        ArrayList<VariableReferenceBase> by_variables = new ArrayList<>();
        ArrayList<String> class_variables = new ArrayList<>();

        // if there are by groups, be sure to add the by variables to the appropriate properties
        if(by_statement != null)
        {
            for(int x=0; x < by_statement.sort_direction().size(); x++)
            {
                VariableSymbolExpression variable;
                if(by_statement.sort_direction(x).ascending() != null)
                    variable = new VariableSymbolExpression(by_statement.sort_direction(x).ascending().variable().getText());
                else
                    variable = new VariableSymbolExpression(by_statement.sort_direction(x).descending().variable().getText());

                by_variables.add(variable);
            }
        }
        // if there are class groups, handle the needlessly complicated mess that is n-way aggregation processing in SAS
        if(class_statement != null)
        {
            for(int x=0; x < class_statement.variable().size(); x++)
            {
                class_variables.add(class_statement.variable(x).getText());
            }

            TempTypeList typelist = new TempTypeList(class_variables);

            // if there's a WAYS statement, add each n-way aggregation for each value of n listed
            if(ways_statement != null)
            {
                ArrayList<Integer> ways = new ArrayList<>();
                for(int x=0; x < ways_statement.value_list().size(); x++)
                {
                    if(ways_statement.value_list(x).first_to != null)
                    {
                        int start = Integer.parseInt(ways_statement.value_list(x).first_to.getText());
                        int end = Integer.parseInt(ways_statement.value_list(x).last_to.getText());
                        int increment = Integer.parseInt(ways_statement.value_list(x).increment.getText());

                        for(int y=start; y < end; y += increment)
                        {
                            ways.add(y);
                        }
                    }
                    else
                    {
                        for(int y=0; y < ways_statement.value_list(x).NUMERIC().size(); y++)
                        {
                            int way = Integer.parseInt(ways_statement.value_list(x).NUMERIC(y).getSymbol().getText());
                            ways.add(way);
                        }
                    }
                }
                typelist.addFromWaysList(ways);
            }

            // if there's a TYPES statement, handle each individual type (or star grouping) listed
            if(types_statement != null)
            {
                for(int x=0; x < types_statement.variable().size(); x++)
                {
                    ArrayList<String> type = new ArrayList<>();
                    type.add(types_statement.variable(x).getText());
                    typelist.addType(type);
                }
                for(int x=0; x < types_statement.star_grouping().size(); x++)
                {
                    List<List<String>> groups = handleStarGrouping(types_statement.star_grouping(x));
                    for(List<String> group : groups)
                    {
                        typelist.addType(group);
                    }
                }

                // if the type list is empty, only generate overall aggregates
                if(typelist.types.isEmpty())
                {
                    // an empty type list is equivalent to WAYS 0, so we interpret them the same way
                    ArrayList<Integer> ways_list = new ArrayList<>();
                    ways_list.add(0);
                    typelist.addFromWaysList(ways_list);
                }
            }
            // if there's a CLASS statement but no TYPES statement, the default is to generate all possible aggregations
            else
            {
                if(ways_statement == null)
                {
                    typelist.generateAllTypes();
                }
            }

            List<List<String>> sorted_types = typelist.getSortedTypes();
            // if there is exactly one aggregation, we only need one Collapse statement to handle it
            if((sorted_types.size() == 1) && (by_statement == null))
            {
                ArrayList<VariableReferenceBase> variables = new ArrayList<>();
                for(int x=0; x < sorted_types.get(0).size(); x++)
                {
                    variables.add(new VariableSymbolExpression(sorted_types.get(0).get(x)));
                }
                if(variables.isEmpty())
                    collapse.groupByVariables = null;
                else
                    collapse.groupByVariables = new VariableListExpression(variables);

                collapse.consumesDataframe = input_dataset;
                collapse.producesDataframe = collapse.outputDatasetName;

                dataset_variables.put(collapse.outputDatasetName, active_variables);

                recordSourceInformation(collapse, ctx);
                commands.add(collapse);
            }
            // otherwise, make one Collapse statement for each aggregation and add an AppendDatasets at the end
            else
            {
                AppendDatasets datasets = new AppendDatasets();
                String original_name = collapse.outputDatasetName;
                dataset_variables.put(original_name, active_variables);

                for(int x=0; x < sorted_types.size(); x++)
                {
                    String output_name = "_" + original_name + "_" + x;

                    collapse = new Collapse();

                    ArrayList<VariableReferenceBase> variables = new ArrayList<>();
                    for(String class_variable : sorted_types.get(x))
                    {
                        variables.add(new VariableSymbolExpression(class_variable));
                    }
                    if(variables.isEmpty())
                        collapse.groupByVariables = null;
                    else
                        collapse.groupByVariables = new VariableListExpression(variables);
                    collapse.aggregateVariables = aggregations;

                    datasets.appendFile(output_name);

                    collapse.consumesDataframe = input_dataset;
                    collapse.producesDataframe = output_name;
                    collapse.outputDatasetName = output_name;

                    recordSourceInformation(collapse, ctx);
                    commands.add(collapse);
                }

                recordSourceInformation(datasets, ctx);
                commands.add(datasets);
            }
        }
        // if there's no CLASS statement, then aggregations are only made according to the BY group,
        // so only one Collapse statement is necessary
        else
        {
            ArrayList<VariableReferenceBase> variables = new ArrayList<>();
            if(by_statement != null)
            {
                variables.addAll(by_variables);
            }
            if(variables.isEmpty())
                collapse.groupByVariables = null;
            else
                collapse.groupByVariables = new VariableListExpression(variables);

            collapse.consumesDataframe = input_dataset;
            collapse.producesDataframe = collapse.outputDatasetName;

            dataset_variables.put(collapse.outputDatasetName, active_variables);

            recordSourceInformation(collapse, ctx);
            commands.add(collapse);
        }

        return commands;
    }

    private MergeDatasets makeMergeStatement(List<SASParser.Set_argumentsContext> set_arguments,
                                             String source_command,
                                             List<SASParser.Modify_optionsContext> modify_options,
                                             List<SASParser.Change_optionsContext> change_options,
                                             SASParser.By_statementContext by_statement)
    {
        MergeDatasets merge = new MergeDatasets();

        // create a list to hold any dataframes being merged from memory rather than from external files
        ArrayList<String> dataframes_consumed = new ArrayList<>();

        ArrayList<VariableReferenceBase> variables = new ArrayList<>();
        List<String> first_variable_names = new ArrayList<>();

        boolean has_by = false;
        if(by_statement != null)
        {
            has_by = true;
            for(int x=0; x < by_statement.sort_direction().size(); x++)
            {
                VariableSymbolExpression variable;
                String direction;// TODO: suggest a way to include sort direction in SDTL merge

                if(by_statement.sort_direction().get(x).ascending() != null)
                {
                    variable = new VariableSymbolExpression(by_statement.sort_direction().get(x).ascending().variable().getText());
                    direction = "Ascending";
                }
                else
                {
                    variable = new VariableSymbolExpression(by_statement.sort_direction().get(x).descending().variable().getText());
                    direction = "Descending";
                }

                variables.add(variable);
            }

            merge.mergeByVariables = variables;
        }

        for(int x=0; x < set_arguments.size(); x++)
        {
            // do as much handling of dataset options as is possible without knowing whether it's a list
            CommandList option_commands = new CommandList();
            if(set_arguments.get(0).data_set_options() != null)
            {
                for(int y=0; y < set_arguments.get(x).data_set_options().size(); y++)
                {
                    TransformBase command = parseDatasetOptions(set_arguments.get(x).data_set_options(y));
                    option_commands.add(command);
                }
            }

            // determine whether missing check option is present and enabled (default is disabled if it's not present)
            boolean missingcheck = false;
            if(change_options != null)
            {
                for(int y=0; y < change_options.size(); y++)
                {
                    if(change_options.get(y).update_option() != null)
                    {
                        missingcheck = change_options.get(y).update_option().MISSINGCHECK() != null;
                    }
                }
            }

            // determine merge type, overlap, and new row properties
            String merge_type;
            String overlap;
            boolean new_row;
            switch(source_command)
            {
                case "merge":
                    merge_type = has_by ? "SASmatchMerge" : "Sequential";
                    overlap = (x == 0) ? "Master" : "Replace";  // TODO: test with more than 2 files
                    new_row = true;
                    break;
                case "modify":
                    merge_type = missingcheck ? "Cartesian" : "OneToOne";
                    overlap = (x == 1) ? "Master" : (missingcheck ? "UpdateMissing" : "FillNew");
                    new_row = (x == 0);
                    break;
                    // TODO: add variables dropped due to modify not accepting new columns
                case "update":
                    merge_type = missingcheck ? "Cartesian" : "OneToOne";
                    overlap = (x == 1) ? "Master" : (missingcheck ? "UpdateMissing" : "FillNew");
                    new_row = true;
                    break;
                default:
                    merge_type = "Sequential";
                    overlap = "Master";
                    new_row = true;
            }
            List<VariableReferenceBase> dropVariables = new ArrayList<>();

            // branch based on whether this argument references a physical file or a dataset in memory
            SASParser.Data_set_nameContext data_set_name = set_arguments.get(x).data_set_name();
            if(data_set_name.variable_or_list() != null)
            {
                VariableReferenceBase dataset_name = parseVariableOrList(data_set_name.variable_or_list());
                if(dataset_name instanceof VariableSymbolExpression)
                {
                    String filename = ((VariableSymbolExpression) dataset_name).variableName;
                    dataframes_consumed.add(filename);

                    // if the source command is modify, then any new variables in the second dataset are dropped
                    if(source_command.equals("modify"))
                    {
                        List<String> variable_names = dataset_variables.get(filename);
                        if(x == 0)
                            first_variable_names = variable_names;
                        else
                        {
                            for(String name : variable_names)
                            {
                                if(!first_variable_names.contains(name))
                                    dropVariables.add(new VariableSymbolExpression(name));
                            }
                        }
                    }

                    merge.mergeFile(filename, merge_type, overlap, new_row, option_commands.commands, dropVariables);
                }
                else if(dataset_name instanceof VariableListExpression)
                {
                    for(int y=0; y < ((VariableListExpression) dataset_name).variables.size(); y++)
                    {
                        String filename = ((VariableSymbolExpression) ((VariableListExpression) dataset_name).variables.get(y)).variableName;
                        dataframes_consumed.add(filename);

                        merge.mergeFile(filename, merge_type, overlap, new_row, option_commands.commands, new ArrayList<>());
                    }
                }
                else
                {
                    // TODO
                }
            }
            else if(data_set_name.libref != null)
            {
                String file = getFileFromDSName(data_set_name);
                List<String> variable_names;
                if(!file.contains(".sas7bdat"))
                {
                    dataframes_consumed.add(data_set_name.dataset.getText());
                    variable_names = dataset_variables.get(data_set_name.dataset.getText());
                }
                else
                {
                    variable_names = file_variables.get(file);
                }
                if(source_command.equals("modify"))
                {
                    if(x == 0)
                        first_variable_names = variable_names;
                    else
                    {
                        for(String name : variable_names)
                        {
                            if(!first_variable_names.contains(name))
                                dropVariables.add(new VariableSymbolExpression(name));
                        }
                    }
                }

                merge.mergeFile(file, merge_type, overlap, new_row, option_commands.commands, dropVariables);
            }
            else
            {
                String file = data_set_name.STRING().getText();
                if(source_command.equals("modify"))
                {
                    List<String> variable_names = file_variables.get(file);
                    if (x == 0)
                        first_variable_names = variable_names;
                    else
                    {
                        for(String name : variable_names)
                        {
                            if(!first_variable_names.contains(name))
                                dropVariables.add(new VariableSymbolExpression(name));
                        }
                    }
                }
                merge.mergeFile(file, merge_type, overlap, new_row, option_commands.commands, dropVariables);
            }
        }

        if(!dataframes_consumed.isEmpty())
            merge.consumesDataframe = dataframes_consumed;

        return merge;
    }

    // handles both PROC SORT and any other statement that causes a sort after the previous one (e.g. BY after SET)
    private TransformBase makeSortStatement(List<SASParser.Sort_directionContext> sort_directions)
    {
        ArrayList<SortCriterion> criteria = new ArrayList<>();

        for(int x=0; x < sort_directions.size(); x++)
        {
            VariableSymbolExpression variable;
            String direction;

            if(sort_directions.get(x).ascending() != null)
            {
                variable = new VariableSymbolExpression(sort_directions.get(x).ascending().variable().getText());
                direction = "Ascending";
            }
            else
            {
                variable = new VariableSymbolExpression(sort_directions.get(x).descending().variable().getText());
                direction = "Descending";
            }

            SortCriterion criterion = new SortCriterion(variable, direction);
            criteria.add(criterion);
        }
        return new SortCases(criteria);
    }

    private ExpressionBase parseExpression(SASParser.ExpressionContext ctx)
    {
        // if the expression is something inside parentheses, with no operators outside the parentheses
        // that are within this expression's scope, remove the parentheses and parse what's inside.
        if(ctx.parenthetical() != null)
        {
            return parseParenthetical(ctx.parenthetical());
        }

        // if the expression has a prefix operator, apply it (if applicable) and parse the sub expression
        if(ctx.unary_expression() != null)
        {
            return parseUnaryExpression(ctx.unary_expression());
        }

        // if this expression has an infix operator, apply the necessary function and parse each sub expression if applicable
        if(ctx.binary_expression() != null)
        {
            return parseBinaryExpression(ctx.binary_expression());
        }

        // if this expression is a function call, apply the specified function
        if(ctx.function() != null)
        {
            return parseFunctionCall(ctx.function());
        }

        // if this expression is a constant, parse it as such
        if(ctx.constant() != null)
        {
            return parseConstant(ctx.constant());
        }

        // if this expression is a variable, yada yada
        if(ctx.variable() != null)
        {
            return parseVariable(ctx.variable());
        }

        return null;    // this should be an unreachable state

    }

    private ExpressionBase parseUnaryExpression(SASParser.Unary_expressionContext ctx)
    {
        // if this is a negative sign, parse it as a multiplication by negative one
        if(ctx.prefix_operator().SUBTRACTION() != null)
        {
            FunctionCallExpression function = new FunctionCallExpression("multiplication");

            function.addArgument(new NumericConstantExpression("-1"));

            if(ctx.expression() != null)
            {
                function.addArgument(parseExpression(ctx.expression()));
            }
            else
            {
                function.addArgument(parseOperand(ctx.operand()));
            }

            return function;
        }
        // if this is not a negative sign, it must be a positive sign and should therefore be ignored
        else
        {
            if(ctx.expression() != null)
            {
                return parseExpression(ctx.expression());
            }
            // if the operand is not explicitly an expression, treat it as a regular operand
            return parseOperand(ctx.operand());
        }
    }

    private ExpressionBase parseBinaryExpression(SASParser.Binary_expressionContext ctx)
    {
        FunctionCallExpression function = new FunctionCallExpression();

        if(ctx.left != null)
            function.addArgument(parseOperand(ctx.left));
        else if(ctx.left_unary != null)
            function.addArgument(parseUnaryExpression(ctx.left_unary));
        else if(ctx.left_binary != null)
            function.addArgument(parseBinaryExpression(ctx.left_binary));

        if(ctx.right != null)
            function.addArgument(parseOperand(ctx.right));
        else if(ctx.right_expr != null)
            function.addArgument(parseExpression(ctx.right_expr));

        if(ctx.infix_operator().SUBTRACTION() != null)
            function.setName("subtraction");
        else if(ctx.infix_operator().ADDITION() != null)
            function.setName("addition");
        else if(ctx.infix_operator().STAR() != null)
            function.setName("multiplication");
        else if(ctx.infix_operator().DIVISION() != null)
            function.setName("division");
        else if(ctx.infix_operator().STARSTAR() != null)
            function.setName("power");
        else if(ctx.infix_operator().MAXIMUM() != null)
            function.setName("maximum");    //not implemented yet
        else if(ctx.infix_operator().MINIMUM() != null)
            function.setName("minimum");    //not implemented yet

        return function;
    }

    private ExpressionBase parseOperand(SASParser.OperandContext ctx)
    {
        if(ctx.parenthetical() != null)
        {
            return parseParenthetical(ctx.parenthetical());
        }
        if(ctx.function() != null)
        {
            return parseFunctionCall(ctx.function());
        }
        if(ctx.constant() != null)
        {
            return parseConstant(ctx.constant());
        }
        return parseVariable(ctx.variable());
    }

    private VariableReferenceBase parseVariable(SASParser.VariableContext ctx)
    {
        return new VariableSymbolExpression(ctx.getText());
    }

    private ExpressionBase parseConstant(SASParser.ConstantContext ctx)
    {
        if(ctx.STRING() != null)
        {
            return new StringConstantExpression(removeQuotes(ctx.STRING().getText()));
        }

        if(ctx.NUMERIC() != null)
        {
            return new NumericConstantExpression(ctx.NUMERIC().getText());
        }

        // if STRING and NUMERIC are both null, this must be a missing value
        return new MissingValueConstantExpression(ctx.DOT().getText());
    }

    private GroupedExpression parseParenthetical(SASParser.ParentheticalContext ctx)
    {
        GroupedExpression group = new GroupedExpression();
        group.expression = parseExpression(ctx.expression());
        return group;
    }

    private GroupedExpression parseParentheticalLogic(SASParser.Parenthetical_logicContext ctx)
    {
        GroupedExpression group = new GroupedExpression();
        group.expression = parseLogicalExpression(ctx.logical_expression());
        return group;
    }

    private VariableReferenceBase parseVariableOrList(SASParser.Variable_or_listContext ctx)
    {
        // if this is a single variable, return a VariableSymbolExpression
        if(ctx.variable() != null)
        {
            return new VariableSymbolExpression(ctx.variable().getText());
        }

        // if this is a numbered variable range, make and return a VariableListExpression from the implied names
        if(ctx.variable_range_1() != null)
        {
            String first = ctx.variable_range_1().first.getText();
            String last = ctx.variable_range_1().last.getText();
            String base = intersection(first, last);

            List<String> names = expandVariableRange(base, first, last);

            return makeVariableList(names);
        }

        // if this is a non-numbered variable range, return it as a VariableRangeExpression
        if(ctx.variable_range_2() != null)
        {
            VariableRangeExpression range = new VariableRangeExpression();

            range.first = ctx.variable_range_2().first.getText();
            range.last = ctx.variable_range_2().last.getText();

            return range;
        }

        // if this is a reference to a SAS array, convert the reference back to a list of variables
        if(ctx.of_list() != null)
        {
            TempArray array = data_step.getArray(ctx.of_list().getText());

            return array.variables;
        }

        return null;
    }

    // return the appropriate half-formed command based on the dataset option in question
    private TransformBase parseDatasetOptions(SASParser.Data_set_optionsContext ctx)
    {
        if(ctx.drop_equals_option() != null)
        {
            DropVariables delete = new DropVariables();

            ArrayList<VariableReferenceBase> variables = new ArrayList<>();
            for(int y=0; y < ctx.drop_equals_option().variable_or_list().size(); y++)
            {
                variables.add(parseVariableOrList(ctx.drop_equals_option().variable_or_list(y)));
            }

            delete.variables = variables;
            return delete;
        }
        if(ctx.keep_equals_option() != null)
        {
            KeepVariables keep = new KeepVariables();

            ArrayList<VariableReferenceBase> variables = new ArrayList<>();
            for(int y=0; y < ctx.keep_equals_option().variable_or_list().size(); y++)
            {
                variables.add(parseVariableOrList(ctx.keep_equals_option().variable_or_list(y)));
            }

            keep.variables = variables;
            return keep;
        }
        if(ctx.label_equals_option() != null)
        {
            String label = ctx.label_equals_option().STRING().getText();
            return new SetDatasetProperty("label", label);
        }
        if(ctx.rename_equals_option() != null)
        {
            Rename rename = new Rename();

            for(int y=0; y < ctx.rename_equals_option().rename_pair().size(); y++)
            {
                SASParser.Rename_pairContext pairCtx = ctx.rename_equals_option().rename_pair(y);

                RenamePair pair = new RenamePair();
                pair.oldVariable = new VariableSymbolExpression(pairCtx.old_name.getText());
                pair.newVariable = new VariableSymbolExpression(pairCtx.new_name.getText());

                rename.renames.add(pair);
            }

            return rename;
        }
        if(ctx.where_equals_option() != null)
        {
            SASParser.Where_equals_optionContext where = ctx.where_equals_option();

            ExpressionBase condition = where.where_expression() != null ? parseWhereExpression(where.where_expression())
                                                                        : parseCompoundWhere(where.compound_where());
            return new KeepCases(condition);
        }

        return null;
    }


    private FunctionCallExpression parseFunctionCall(SASParser.FunctionContext ctx)
    {
        FunctionCallExpression function = new FunctionCallExpression(ctx.function_name.getText());

        // if the name of the function is the name of an array, then this is an array dereference, not a function call
        if(data_step.getArray(function.function) != null)
        {
            TempArray array = data_step.getArray(function.function);
            if(!ctx.arguments().argument().isEmpty())
            {
                SASParser.ArgumentContext argumentCtx = ctx.arguments().argument(0);

                if(argumentCtx.expression() != null)
                {
                    function.addArgument(parseExpression(argumentCtx.expression()));
                }
                else if(argumentCtx.variable_or_list() != null)
                {
                    if(argumentCtx.variable_or_list().variable() != null)
                    {
                        function.addArgument(new IteratorSymbolExpression(argumentCtx.variable_or_list().variable().getText()));
                    }
                    else
                    {
                        // TODO: this should be invalid because you can't dereference an array with a variable list
                    }
                }
                else // if argument.constant() != null
                {
                    function.addArgument(new IteratorSymbolExpression(argumentCtx.constant().getText()));
                }

                function.addArgument(array.variables);

                function.setName("variable_array_dereference");
            }
            else
            {
                // TODO: put an Invalid here or something
            }
        }
        else
        {
            if(getSDTLFunctionName(function.function) != null)
            {
                function.setName(getSDTLFunctionName(function.function));
            }
            else
            {
                function.makeUserDefined();
            }
            for (int x = 0; x < ctx.arguments().argument().size(); x++ )
            {
                if (ctx.arguments().argument(x) != null)
                {
                    SASParser.ArgumentContext argumentCtx = ctx.arguments().argument(x);

                    if(argumentCtx.variable_or_list() != null)
                        function.addArgument(parseVariableOrList(argumentCtx.variable_or_list()));
                    else if(argumentCtx.expression() != null)
                        function.addArgument(parseExpression(argumentCtx.expression()));
                    else
                        function.addArgument(parseConstant(argumentCtx.constant()));
                }
            }
        }

        return function;
    }

    private FunctionCallExpression parseImplicitCondition(SASParser.ExpressionContext ctx)
    {
        return parseImplicitCondition(parseExpression(ctx));
    }

    // an implicit condition is equivalent to saying "this expression is non-missing and non-zero"
    private FunctionCallExpression parseImplicitCondition(ExpressionBase expression)
    {
        FunctionCallExpression and_function = new FunctionCallExpression("and");
        FunctionCallExpression not_function = new FunctionCallExpression("not");
        FunctionCallExpression missing_function = new FunctionCallExpression("missing_var");

        missing_function.addArgument(expression);

        not_function.addArgument(missing_function);

        FunctionCallExpression ne_function = new FunctionCallExpression("ne");

        NumericConstantExpression constant = new NumericConstantExpression("0");

        ne_function.addArgument(expression);
        ne_function.addArgument(constant);

        and_function.addArgument(not_function);
        and_function.addArgument(ne_function);

        return and_function;
    }

    private ExpressionBase parseWhereExpression(SASParser.Where_expressionContext ctx)
    {
        if(ctx.logical_expression() != null)
        {
            return parseLogicalExpression(ctx.logical_expression());
        }

        if(ctx.expression() != null)
        {
            return parseImplicitCondition(ctx.expression());
        }

        if(ctx.where_operation() != null)
        {
            return parseWhereOperation(ctx.where_operation());
        }

        // the SAME AND operator results in a WHERE expression equal to
        // the previous one connected to the one specified via a logical AND
        if(ctx.where_expression() != null)
        {
            FunctionCallExpression function = new FunctionCallExpression("and");

            function.addArgument(data_step.last_where);
            function.addArgument(parseWhereExpression(ctx.where_expression()));

            return function;
        }

        return null;
    }

    private FunctionCallExpression parseWhereOperation(SASParser.Where_operationContext ctx)
    {
        FunctionCallExpression function = new FunctionCallExpression();

        // this is out here because every alternative has a variable as EXP1
        function.addArgument(parseVariable(ctx.variable()));

        if(ctx.BETWEEN() != null)
        {
            function.setName("between");

            function.addArgument(new NumericConstantExpression(ctx.start.getText()));
            function.addArgument(new NumericConstantExpression(ctx.end.getText()));
        }
        else if(ctx.CONTAINS() != null || ctx.QUESTION_MARK() != null)
        {
            function.setName("contains");

            function.addArgument(new StringConstantExpression(removeQuotes(ctx.STRING().getText())));
        }
        else if(ctx.NULL() != null || ctx.MISSING() != null)
        {
            function.setName("missing_var");
        }
        else if(ctx.LIKE() != null)
        {
            function.setName("like");

            function.addArgument(new StringConstantExpression(removeQuotes(ctx.STRING().getText())));
        }
        else if(ctx.EQUAL() != null)
        {
            function.setName("soundex");

            function.addArgument(new StringConstantExpression(removeQuotes(ctx.STRING().getText())));
        }

        return function;
    }

    private FunctionCallExpression parseCompoundWhere(SASParser.Compound_whereContext ctx)
    {
        FunctionCallExpression function = new FunctionCallExpression();

        function.addArgument(parseWhereExpression(ctx.first));

        if(ctx.LOGICAL_NOT() != null)
        {
            FunctionCallExpression not_function = new FunctionCallExpression("not");

            not_function.addArgument(ctx.second != null ? parseWhereExpression(ctx.second)
                                                        : parseCompoundWhere(ctx.next));

            function.addArgument(not_function);
        }
        else
        {
            function.addArgument(ctx.second != null ? parseWhereExpression(ctx.second)
                                                    : parseCompoundWhere(ctx.next));
        }

        SASParser.Binary_logicContext binary = ctx.binary_logic();
        if(binary.LOGICAL_AND() != null)
            function.setName("and");
        else
            function.setName("or");

        return function;
    }

    private ExpressionBase parseLogicalExpression(SASParser.Logical_expressionContext ctx)
    {
        // if this logical expression is inside parentheses, return a grouped expression around it
        if(ctx.parenthetical_logic() != null)
        {
            return parseParentheticalLogic(ctx.parenthetical_logic());
        }
        // if this is a unary logical expression, then it must be a NOT
        if(ctx.LOGICAL_NOT() != null)
        {
            FunctionCallExpression not_function = new FunctionCallExpression( "not");
            not_function.addArgument(parseLogicalExpression(ctx.right_expression));

            return not_function;
        }
        else
        {
            FunctionCallExpression function = new FunctionCallExpression();
            if(ctx.binary_logic() != null)
            {
                function.function = ctx.binary_logic().LOGICAL_AND() == null ? "or" : "and";

                function.addArgument(ctx.left_operand == null ? parseLogicalExpression(ctx.left_expression)
                                                              : parseImplicitCondition(parseOperand(ctx.left_operand)));

                function.addArgument(ctx.right_operand == null ? parseLogicalExpression(ctx.right_expression)
                                                               : parseImplicitCondition(parseOperand(ctx.right_operand)));
            }
            if(ctx.comparison() != null)
            {
                if(ctx.comparison().EQUAL() != null)                        { function.setName("eq"); }
                else if(ctx.comparison().NOT_EQUAL() != null)               { function.setName("ne"); }
                else if(ctx.comparison().GREATER_THAN() != null)            { function.setName("gt"); }
                else if(ctx.comparison().LESS_THAN() != null)               { function.setName("lt"); }
                else if(ctx.comparison().GREATER_THAN_OR_EQUAL() != null)   { function.setName("ge"); }
                else if(ctx.comparison().LESS_THAN_OR_EQUAL() != null)      { function.setName("le"); }
                else                                                        { function.setName("in"); } // not implemented yet



                function.addArgument(ctx.left_operand == null ? parseLogicalExpression(ctx.left_expression)
                                                              : parseOperand(ctx.left_operand));

                function.addArgument(ctx.right_operand == null ? parseLogicalExpression(ctx.right_expression)
                                                               : parseOperand(ctx.right_operand));
            }

            return function;
        }
    }


    private String getSDTLFunctionName(String functionName)
    {
        String lower = functionName.toLowerCase();
        String upper = functionName.toUpperCase();

        if (FunctionNameMap.SDTLMap.containsKey(lower))
        {
            return FunctionNameMap.SDTLMap.get(lower);
        }
        if(FunctionNameMap.SDTLMap.containsKey(upper))
        {
            return FunctionNameMap.SDTLMap.get(upper);
        }

        return null;
    }
    
    private Invalid makeInvalid(String message, ParserRuleContext ctx)
    {
        Invalid invalid = new Invalid(message);
        recordSourceInformation(invalid, ctx);
        return invalid;
    }

    private void recordSourceInformation(TransformBase command, ParserRuleContext ctx)
    {
        command.sourceInformation.sourceStartIndex = ctx.start.getStartIndex();
        command.sourceInformation.sourceStopIndex = ctx.stop.getStopIndex();
        command.sourceInformation.lineNumberStart = ctx.start.getLine();
        command.sourceInformation.lineNumberEnd = ctx.stop.getLine();
        command.sourceInformation.originalSourceText = ctx.start.getInputStream().toString().substring(ctx.start.getStartIndex(), ctx.stop.getStopIndex()).trim();
    }
}

