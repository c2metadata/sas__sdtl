/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified extensively by Alexander Mueller in 2019.
*/

package SASConverter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class FunctionNameMap {

    public static final Map<String, String> SDTLMap = new HashMap<>();

    static
    {
        /*SDTLMap.put("+", "addition");
        SDTLMap.put("-", "subtraction");
        SDTLMap.put("/", "division");
        SDTLMap.put("*", "multiplication");
        SDTLMap.put("**", "power");
        SDTLMap.put("trunc", "truncate");*/
        String filepath = "/home/alecastle/IdeaProjects/sas__sdtl/src/main/mappings/SDTL_Function_Library.json";
        try
        {
            String file = Files.readString(Paths.get(filepath));
            JSONArray function_library = new JSONArray(file);

            JSONArray horizontal = new JSONArray();
            for(int x=0; x < function_library.length(); x++)
            {
                if(function_library.getJSONObject(x).has("horizontal"))
                    horizontal = function_library.getJSONObject(x).getJSONArray("horizontal");
            }
            for(int x=0; x < horizontal.length(); x++)
            {
                JSONObject function = horizontal.getJSONObject(x);
                String sdtl_name = function.getString("SDTLname");
                String sas_name = function.getString("SASname");
                SDTLMap.put(sas_name, sdtl_name);
            }

            JSONArray collapse = new JSONArray();
            for(int x=0; x < function_library.length(); x++)
            {
                if(function_library.getJSONObject(x).has("collapse"))
                    collapse = function_library.getJSONObject(x).getJSONArray("collapse");
            }
            for(int x=0; x < collapse.length(); x++)
            {
                JSONObject function = collapse.getJSONObject(x);
                String sdtl_name = function.getString("SDTLname");
                String sas_name = function.getString("SASname");
                SDTLMap.put(sas_name, sdtl_name);
            }
        }
        catch (IOException | JSONException e)
        {
            e.printStackTrace();
        }

    }

}
