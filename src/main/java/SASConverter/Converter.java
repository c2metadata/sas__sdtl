/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified by Alexander Mueller in 2019.
*/

package SASConverter;

import Models.*;
import SASParser.SASLexer;
import SASParser.SASParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ibm.icu.util.VersionInfo;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.UnbufferedTokenStream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Converter
{
    public Program convertSAS(String sas_string, HashMap<String, List<String>> file_variables, HashMap<String, List<String>> dataset_variables)  throws IOException, NoSuchAlgorithmException
    {

        // Created Program Object. Will turn into JSON and then returned
        Program program = new Program();
        program.iD = "program-1";

        // TODO: figure out how to actually get the source file name
        // Separate the filename from the rest of the path in case an absolute path was given
        /*String[] path_array = sas_filepath.split("/");
        if(path_array.length > 1)
            program.SourceFileName = path_array[path_array.length - 1];
        else
            program.SourceFileName = sas_filepath;*/

        // Add Source Language
        program.sourceLanguage = "SAS";

        program.sourceFileSize = (long) sas_string.length();

        // Add General Information to Program
        program.lineCount = sas_string.split("\n").length;
        program.modelCreatedTime = new Date();
        program.modelVersion = VersionInfo.ICU_VERSION.toString();
        program.parser = "sas-to-sdtl";
        program.parserVersion = VersionInfo.ICU_VERSION.toString();

        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] stringMD5 = md.digest(sas_string.getBytes(StandardCharsets.UTF_8));
        StringBuilder sbMD5 = new StringBuilder();
        for (byte b : stringMD5) {
            sbMD5.append(String.format("%02x", b));
        }
        program.scriptMD5 = sbMD5.toString();

        md = MessageDigest.getInstance("SHA-1");
        byte[] stringSHA1 = md.digest(sas_string.getBytes(StandardCharsets.UTF_8));
        StringBuilder sbSHA1 = new StringBuilder();
        for (byte b : stringSHA1) {
            sbSHA1.append(String.format("%02x", b));
        }

        program.scriptSHA1 = sbSHA1.toString();

        // TODO: Check for improper syntax from input here

        InputStream stream = new ByteArrayInputStream(sas_string.getBytes(StandardCharsets.UTF_8));
        SASLexer saslexer = new SASLexer(CharStreams.fromStream(stream, StandardCharsets.UTF_8));
        CommonTokenStream commonTokenStream = new CommonTokenStream(saslexer);
        SASParser sasParser = new SASParser(commonTokenStream);

        // Get top of AST from parser
        SASParser.ParseContext tree = sasParser.parse();

        // TODO: Add messages from parser

        // Visit all nodes of tree in visitor

        TreeVisitor treeVisitor = new TreeVisitor(file_variables, dataset_variables);

        CommandList commandList = (CommandList) treeVisitor.visitParse(tree);

        //System.out.println("______________________________COMMAND TRACKER______________________________");
        //treeVisitor.print_Command_Tracker();
        //System.out.println("------------------------------COMMAND TRACKER------------------------------");

        for (TransformBase c : commandList.commands) {
            if (c != null && ! ((c instanceof TempData) || (c instanceof TempArray))) {
                program.commands.add(c);
            }
        }

        program.commandCount = program.commands.size();

        return program;
    }

}
