/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Alexander Mueller in 2019.
*/

package SASConverter;

import Models.Program;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class SASController
{
    @RequestMapping(method=POST, value="/api/sastosdtl")
    public Program getProgram(@RequestParam(name="parameters") String json_string) throws JSONException, IOException, NoSuchAlgorithmException
    {
        Converter converter = new Converter();

        JSONObject file = new JSONObject(json_string);
        JSONObject parameters = file.getJSONObject("parameters");

        String sas_code = parameters.getString("sas");
        HashMap<String, List<String>> file_variables = new HashMap<>();

        JSONArray data_file_descriptions = parameters.getJSONArray("data_file_descriptions");
        for(int x=0; x < data_file_descriptions.length(); x++)
        {
            String filename = data_file_descriptions.getJSONObject(x).getString("input_file_name");

            String variables_string = data_file_descriptions.getJSONObject(x).getString("variables");
            String[] variables = variables_string.split(" ");

            ArrayList<String> variable_list = new ArrayList<String>(Arrays.asList(variables));

            if(file_variables.containsKey(filename))
            {
                throw new IOException("Error: duplicate filename in input file list. Please rename your files.");
            }
            file_variables.put(filename, variable_list);
        }

        return converter.convertSAS(sas_code, file_variables, new HashMap<String, List<String>>());
    }
}
