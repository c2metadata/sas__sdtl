/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified extensively by Alexander Mueller in 2019.
*/

parser grammar SASParser;

options { tokenVocab = SASLexer; }

@parser::members
{
    public boolean if_flag = false;

    public void setIfFlag(boolean flag) { this.if_flag = flag; }

    public boolean isIf() { return this.if_flag; }
}
/*
    Outputting information from both on the way down and up
    Arrive at node, basic processing - visit children (get information), post processing, return to parent

    preprocessing - do information until you need information from children
*/

// Parser Rules

// SAS script is a list of lines
parse   : line* EOF
        ;

// SAS script has 2 possible steps: data step and proc step
line    : data_step
        | proc_step
        ;

// Data step involves data set creation or input information
data_step : data_step_block
          | libname_statement // In here since it is used only to supply information to data step. It makes for a slightly cleaner grammar too.
          | filename_statement
          ;

// A data step is a group of SAS statements that begin with a DATA statement
data_step_block : data_statement
                  statement*
                  run_statement? // Data step needs "run" if it doesn't contain datalines statement
                ;

run_statement : RUN SEMICOLON;

// need to separate out data statement for easier parsing of data set name
data_statement : DATA data_arguments* SEMICOLON;

data_arguments : data_set_name (OPEN_PAREN data_set_options+ CLOSE_PAREN)?;

/* data set names in SAS can be any of these types:

    'filepath'
    oneLevelName
    libref.twoLevelName
    name1-name8
    name1:      --> Expand with project scope
*/
data_set_name : STRING
              | libref=variable DOT dataset=variable
              | variable_or_list
              ;

// data set options -> expand with project scope
data_set_options : drop_equals_option
                 | keep_equals_option
                 | label_equals_option
                 | rename_equals_option
                 | where_equals_option
                 ;

drop_equals_option : DROP EQUAL variable_or_list+ ;

keep_equals_option : KEEP EQUAL variable_or_list+ ;

label_equals_option : LABEL EQUAL STRING ;

rename_equals_option : RENAME EQUAL OPEN_PAREN rename_pair+ CLOSE_PAREN;

where_equals_option : WHERE EQUAL OPEN_PAREN where_expression CLOSE_PAREN
                    | WHERE EQUAL OPEN_PAREN compound_where CLOSE_PAREN
                    ;

// List of possible sas statements  --> Expand with project scope
statement  : set_statement
           | rename_statement
           | select_statement
           | merge_statement
           | modify_statement
           | update_statement
           | label_statement
           | if_statement
           | where_statement
           | drop_statement
           | keep_statement
           | array_statement
           | do_statement
           | by_statement
           | assignment_statement
           | proc_freq_statement
           | exact_statement
           | output_statement
           | tables_statement
           | test_statement
           | weight_statement
           | proc_means_statement
           | class_statement
           | freq_statement
           | id_statement
           | var_statement
           | proc_summary_statement
           | proc_print_statement
           | proc_sort_statement
           | proc_format_statement
           | value_statement
           | missing_statement
           | comment_statement
           | delete_statement
           | format_statement
           | attrib_statement
           ;

/*
    Data SET Statement Syntax:

    SET <SAS-data-set(s) <(data-set-option(s))>> <options> ;

    Valid in: DATA step
*/
set_statement : SET set_arguments* set_options* SEMICOLON
                by_statement?;

// <SAS-data-set(s) <(data-set-option(s))>>
set_arguments: data_set_name (OPEN_PAREN data_set_options+ CLOSE_PAREN)? ;


// <options>  --> Expand with project scope
set_options : end_option
            | key_option
            | indsname_option
            | nobs_option
            | open_option
            | point_option
            ;

// options common to many commands --> Expand with project scope
end_option      : END EQUAL variable;
key_option      : KEY EQUAL key=NUMERIC (DIVISION UNIQUE)?;
indsname_option : INDSNAME EQUAL variable;
nobs_option     : NOBS EQUAL variable;
open_option     : OPEN EQUAL open=(IMMEDIATE | DEFER);
point_option    : POINT EQUAL variable;

/*
    Data RENAME Statement Syntax:

    RENAME old-name-1=new-name-1 ... <old-name-n=new-name-n> ;

    Valid in: DATA step
*/
rename_statement : RENAME rename_pair+ SEMICOLON ;

// old-name-n=new-name-n
rename_pair : old_name=variable EQUAL new_name=variable ;

/*
    Data SELECT Statement Syntax:

    SELECT <(select-expression)> ;
        WHEN-1(when-expression-1<..., when-expression-n>) statement ;
        <... WHEN-n(when-expression-1<..., when-expression-n>) statement ;>
        <OTHERWISE statement ;>
    END ;

    Valid in: DATA step
*/
select_statement : SELECT (OPEN_PAREN expression CLOSE_PAREN)? SEMICOLON
                   when_statement+ otherwise_statement?
                   END SEMICOLON
                 ;

// WHEN-1(when-expression-1<..., when-expression-n>) statement ;
when_statement : WHEN OPEN_PAREN expression+ CLOSE_PAREN statement ;

// <OTHERWISE statement ;>
otherwise_statement: OTHERWISE statement;



/*
    Data MERGE Statement Syntax:

    MERGE SAS-data-set-1 <(data-set-options)>
          SAS-data-set-2 <(data-set-options) >
          <...SAS-data-set-n<(data-set-options)>>
          <END=variable>;

    Valid in: DATA step
*/

merge_statement : MERGE set_arguments+ end_option? SEMICOLON
                  by_statement?;

/*
    Data MODIFY Statement Syntax:

    MODIFY master-data-set <(data-set-options)> transaction-data-set <(data-set-options)>
    <CUROBS=variable> <NOBS=variable> <END=variable>
    <UPDATEMODE=MISSINGCHECK | NOMISSINGCHECK>;
    BY by-variable;
    MODIFY master-data-set <(data-set-options)> KEY=index </ UNIQUE>
    <KEYRESET=variable> <NOBS=variable> <END=variable>;
    MODIFY master-data-set <(data-set-options)> <NOBS=variable> POINT=variable;
    MODIFY master-data-set <(data-set-options)> <NOBS=variable> <END=variable>;

    Valid in: DATA step
*/
modify_statement : MODIFY master=set_arguments transaction=set_arguments modify_options* change_options* SEMICOLON by_statement
                 | MODIFY master=set_arguments key_option (nobs_option | end_option)*
                 | MODIFY master=set_arguments nobs_option? point_option
                 | MODIFY master=set_arguments (nobs_option | end_option)*
                 ;

/*
    Data UPDATE Statement Syntax:

    UPDATE master-data-set <(data-set-options)> transaction-data-set <(data-set-options)>
    <END=variable>
    <UPDATEMODE=MISSINGCHECK | NOMISSINGCHECK>;
    BY by-variable;

    Valid in: DATA step
*/
update_statement : UPDATE master=set_arguments transaction=set_arguments change_options* SEMICOLON by_statement;

// options common to UPDATE and MODIFY
change_options : end_option
               | update_option
               ;

// options specific to MODIFY
modify_options : curobs_option
               | nobs_option
               ;

curobs_option : CUROBS EQUAL variable;
update_option : UPDATEMODE EQUAL (MISSINGCHECK | NOMISSINGCHECK);
key_reset_option : KEYRESET EQUAL variable;

/*
    WHERE statement Syntax:

    WHERE where-expression-1
    <logical-operator where-expression-n>;

    Valid in: Data step or Proc step
*/
where_statement : WHERE { this.setIfFlag(true); } where_expression { this.setIfFlag(false); } SEMICOLON
                | WHERE { this.setIfFlag(true); } compound_where { this.setIfFlag(false); } SEMICOLON
                ;

compound_where : first=where_expression binary_logic LOGICAL_NOT? second=where_expression
               | first=where_expression binary_logic LOGICAL_NOT? next=compound_where
               ;

where_expression : where_operation
                 | logical_expression
                 | expression
                 | SAME LOGICAL_AND where_expression
                 ;

where_operation : variable BETWEEN start=NUMERIC LOGICAL_AND end=NUMERIC
                | variable (CONTAINS | QUESTION_MARK) STRING
                | variable IS NULL
                | variable IS MISSING
                | variable LIKE STRING
                | variable EQUAL STAR STRING
                ;

/*
    Data assignment statement Syntax:

    variable=expression;

    Valid in: DATA step
*/
assignment_statement : { !this.isIf() }? variable EQUAL expression SEMICOLON ;

/*
    LABEL Statement Syntax:

    LABEL variable-1=label-1 . . . <variable-n=label-n>;

        OR

    LABEL variable-1=' ' ... <variable-n=' '>;  --> Expand with project scope

    Valid in: DATA step
*/

label_statement : LABEL label_part+ SEMICOLON ;

// Labels only have to be quoted if they contain ; or =
label_part : variable EQUAL STRING
           | variable EQUAL variable+
           ;

/*
    DROP Statement Syntax:

    DROP variable-list;

    Valid in: DATA step
*/

drop_statement : DROP variable_or_list+ SEMICOLON ;

/*

    KEEP Statement Syntax:

    KEEP variable-list;

    Valid in: DATA step

*/

keep_statement : KEEP variable_or_list+ SEMICOLON ;

/*
    ARRAY Statement Syntax:

    ARRAY array-name { subscript } <$><length>
    <array-elements> <(initial-value-list)>;

    Valid in: DATA step
*/

array_statement : ARRAY array_name=variable array_subscript DOLLAR_SIGN? (length=NUMERIC)?
                  array_elements? initial_value_list? SEMICOLON
                ;


// { subscript }
array_subscript : LEFT_BRACKET array_index RIGHT_BRACKET
                | OPEN_PAREN array_index CLOSE_PAREN
                | LEFT_SQUARE array_index RIGHT_SQUARE
                ;

array_index : variable
            | expression
            | STAR;

// <array-elements>
array_elements : variable+ ;

// <(initial-value-list)>
initial_value_list : constant_sublist
                   | OPEN_PAREN constant_iter_value=NUMERIC_NO_DOT STAR constant_value=constant CLOSE_PAREN
                   | OPEN_PAREN constant_iter_value=NUMERIC_NO_DOT STAR initial_value_list CLOSE_PAREN;

constant_sublist : OPEN_PAREN constant+ CLOSE_PAREN;

/*
    DO Statement, Iterative Syntax:

    DO index-variable=specification-1 <, . . . specification-n>;
    . . . more SAS statements . . .
    END;

    Valid in: DATA step
*/

do_statement :  DO index_variable=variable EQUAL do_spec (COMMA do_spec)* SEMICOLON
                statement+
                END SEMICOLON
             ;

// specification
do_spec : start=expression (TO  stop=expression)? (BY increment=expression)?
        ( (WHILE OPEN_PAREN while_cond=expression CLOSE_PAREN)
        | (UNTIL OPEN_PAREN until_cond=expression CLOSE_PAREN) )?
        ;

/*
    IF Statement Syntax:

    IF expression ;

        OR

   IF expression THEN statement;
   <ELSE statement;>

   Valid in: DATA step
*/

if_statement : IF { this.setIfFlag(true); } implicit_condition=expression { this.setIfFlag(false); } SEMICOLON
             | IF { this.setIfFlag(true); } condition=logical_expression { this.setIfFlag(false); } SEMICOLON
             | IF { this.setIfFlag(true); } implicit_condition=expression {this.setIfFlag(false); } THEN then_command=statement (ELSE else_command=statement)?
             | IF { this.setIfFlag(true); } condition=logical_expression { this.setIfFlag(false); } THEN then_command=statement (ELSE else_command=statement)?
             ;


/*
    BY Statement Syntax:

    BY <DESCENDING> variable-1
    <...<DESCENDING> variable-n > <NOTSORTED><GROUPFORMAT>;

    Valid in: DATA step or PROC step
*/

by_statement : BY sort_direction+ NOTSORTED? GROUPFORMAT? SEMICOLON ;

/*
    LIBNAME Statement Syntax:

    LIBNAME libref <engine> 'SAS-library'
    < options > <engine/host-options>;
    LIBNAME libref CLEAR | _ALL_ CLEAR;
    LIBNAME libref LIST | _ALL_ LIST;
    LIBNAME libref <engine> (library-specification-1 <. . . library-specification-n>)
    < options>;
*/
libname_statement : LIBNAME libref=variable (engine=variable)? sas_library=STRING libref_options* engine_options* SEMICOLON
                  | LIBNAME libref=variable CLEAR
                  | LIBNAME UNDERSCORE_ALL CLEAR
                  | LIBNAME libref=variable LIST
                  | LIBNAME UNDERSCORE_ALL LIST
                  | LIBNAME libref=variable (engine=variable)? first_libspec=data_set_name (next_libspec=data_set_name)+ libref_options* SEMICOLON
                  ;

libref_options : ACCESS EQUAL (READONLY | TEMP)
               | COMPRESS EQUAL (NO | YES | CHAR | BINARY)
               | CHARBYTES EQUAL charbytes=constant
               | (CVPENGINE | CVPENG) EQUAL engine=variable
               | (CVPMULTIPLIER | CVPMULT) EQUAL multiplier=constant
               | INENCODING EQUAL (ANY | ASCIIANY | EBCDICANY | IDENTIFIER)
               | OUTENCODING EQUAL (ANY | ASCIIANY | EBCDICANY | IDENTIFIER)
               | OUTREP EQUAL outrep=variable
               | REPEMPTY EQUAL (YES | NO)
               ;

engine_options : IDENTIFIER+; // --> Expand with project scope

/*
    PROC FREQ Statement Syntax:

    PROC FREQ <options> ;
*/

proc_freq_statement : PROC FREQ proc_freq_options* SEMICOLON ;

proc_freq_options : COMPRESS
                  | DATA EQUAL dataset=data_set_name
                  | FORMCHAR EQUAL IDENTIFIER
                  | NLEVELS
                  | NOPRINT
                  | ORDER EQUAL (DATA | FORMATTED | FREQ | INTERNAL)
                  | PAGE
                  ;

/*
    EXACT Statement Syntax:

    EXACT statistic-options </ computation-options> ;  --> Expand with project scope
*/

exact_statement : EXACT SEMICOLON ;

/*
    OUTPUT Statement Syntax:

    OUTPUT <OUT=SAS-data-set> <output-statistic-specification(s)>
    <id-group-specification(s)> <maximum-id-specification(s)>
    <minimum-id-specification(s)> </ option(s)>;

        OR

    OUTPUT <OUT= SAS-data-set> options ;   --> Expand with project scope
*/

output_statement : OUTPUT output_dataset? output_part* SEMICOLON ;

output_dataset : OUT EQUAL data_set_name;
/*
    <output-statistic-specification(s)> Syntax:

    statistic-keyword<(variable-list)>=<name(s)>
*/
output_part : statistic_keyword
            | statistic_keyword EQUAL output_name+
            | statistic_keyword output_variables EQUAL output_name+;
//output_part : statistic_keyword (output_variables? EQUAL output_names)? ;

output_variables : OPEN_PAREN variable+ CLOSE_PAREN;
output_name : variable
            | statistic_keyword;

// statistic-keyword
statistic_keyword : CLM
                  | CSS
                  | CV
                  | KURTOSIS
                  | KURT
                  | LCLM
                  | MAX
                  | MEAN
                  | MIN
                  | MODE
                  | N_KEYWORD
                  | NMISS
                  | RANGE
                  | SKEWNESS
                  | SKEW
                  | STDDEV
                  | STD
                  | STDERR
                  | SUM
                  | SUMWGT
                  | UCLM
                  | USS
                  | VAR
                  | MEDIAN
                  | P50
                  | P1
                  | P5
                  | P10
                  | P25
                  | P75
                  | P90
                  | P95
                  | P99
                  | Q1
                  | QRANGE
                  | PROBT
                  | PRT
                  | T_KEYWORD
                  ;

/*
   TABLES Statement Syntax:

   TABLES requests </ options> ;  --> Expand with project scope
*/

tables_statement : TABLES tables_requests+ SEMICOLON ;

// requests
tables_requests : variable
                | variable STAR variable
                ;

/*
    TEST Statement Syntax:

    TEST options ;  --> Expand with project scope
*/

test_statement : TEST SEMICOLON ;

/*
    WEIGHT Statement Syntax:

    WEIGHT variable </ option> ;  --> Expand with project scope
*/

weight_statement : WEIGHT variable SEMICOLON ;

/*
    PROC MEANS Statement Syntax:

    PROC MEANS <option(s)> <statistic-keyword(s)>;
*/

proc_means_statement   : PROC MEANS   proc_means_summary;
proc_summary_statement : PROC SUMMARY proc_means_summary;

proc_means_summary : proc_means_options* SEMICOLON
                     collapse_statements*;

collapse_statements : by_statement
                    | class_statement
                    | freq_statement
                    | id_statement
                    | output_statement
                    | types_statement
                    | var_statement
                    | ways_statement
                    | weight_statement
                    ;

// <option(s)>  --> Expand with project scope
proc_means_options : DATA EQUAL dataset=data_set_name
                   | CLASSDATA EQUAL class_data=variable
                   | COMPLETETYPES
                   | EXCLUSIVE
                   | MISSING
                   | NONOBS
                   | ORDER EQUAL order=(DATA | FORMATTED | FREQ | UNFORMATTED)
                   | CHARTYPE
                   | DESCENDTYPES
                   | IDMIN
                   | NWAY
                   | statistic_keyword
                   ;

/*
    TYPES statement syntax example:

    TYPES ()    -> overall aggregates only (i.e. ignore class variables)
    TYPES A B C -> 1-way aggregates over A, B, and C
    TYPES A*B C -> 2-way aggregates over AB, 1-way over C
    TYPES A*B*C -> 3-way aggregates over ABC

    (all overall and n-way aggregates from 1 to k when TYPES is omitted, where k is the number of CLASS variables)
*/
types_statement : TYPES ((variable | star_grouping)* | OPEN_PAREN CLOSE_PAREN) SEMICOLON
                ;

/*
    star group  -> equivalent grouping

    A*B         -> A*B
    A*(B C)     -> A*B A*C
    A*B*C       -> A*B*C
    (A B)*C     -> A*C B*C
    (A B)*(C D) -> A*C A*D B*C B*D
    (A B)*(C*D) -> A*C*D B*C*D
    (A*B)*C     -> A*B*C
    (A*B)*(C D) -> A*B*C A*B*D
    (A*B)*(C*D) -> A*B*C*D
*/
star_grouping : left_var=variable       STAR right_var=variable
              | left_var=variable       STAR right_type=type_grouping
              | left_var=variable       STAR right_star=star_grouping
              | left_type=type_grouping STAR right_var=variable
              | left_type=type_grouping STAR right_type=type_grouping
              | left_type=type_grouping STAR right_star=star_grouping
              | left_star=star_grouping STAR right_var=variable
              | left_star=star_grouping STAR right_type=type_grouping
              | left_star=star_grouping STAR right_star=star_grouping
              ;

type_grouping : OPEN_PAREN variable+ CLOSE_PAREN;

ways_statement : WAYS value_list+ SEMICOLON;

/*
    CLASS Statement Syntax:

    CLASS variable(s) </ options>; --> Expand with project scope

*/

class_statement : CLASS variable+ SEMICOLON;

/*
    FREQ Statement Syntax:

    FREQ variable ;
*/

freq_statement : FREQ variable SEMICOLON;

/*
    ID Statement Syntax:

    ID variable(s) ;
*/

id_statement : ID variable+ SEMICOLON ;

/*
    VAR Statement Syntax:

    VAR variable(s) </ WEIGHT=weight-variable>;
*/

var_statement : VAR variable+ (WEIGHT EQUAL weight=variable )? SEMICOLON ;

/*
    PROC PRINT Statement Syntax:

    PROC PRINT <option(s)>;
*/

proc_print_statement : PROC PRINT proc_print_options* SEMICOLON ;

// <option(s)>  --> Expand with project scope
proc_print_options : DATA EQUAL dataset=data_set_name ;

/*
    SORT Procedure Syntax:

    PROC SORT <collating-sequence-option> <other option(s)>;
        BY <DESCENDING> variable-1 <...<DESCENDING> variable-n>;

*/
proc_sort_statement : PROC SORT collating_sequence_option? proc_sort_options? SEMICOLON
    BY sort_direction+ SEMICOLON;

sort_direction : ascending | descending;
ascending : variable;
descending : DESCENDING variable;

// <collating-sequence-option>
collating_sequence_option : ASCII
                          | EBCDIC
                          | DANISH
                          | FINNISH
                          | NORWEGIAN
                          | POLISH
                          | SWEDISH
                          ; // --> Expand with project scope

// <other option(s)>
proc_sort_options : DATA EQUAL dataset=data_set_name
                  | OUT EQUAL dataset=data_set_name
                  ; // --> Expand with project scope

/*
    MISSING Statement Syntax:

    MISSING character(s);
*/
missing_statement : MISSING CHARACTER+ SEMICOLON;

//
//    Comment Statement Syntax:
//
//    *message;
//
//    or
//
//    *mess
//          age;
//    or
//
//    /*message*/
//
//    or
//
//    /*mess
//          age*/
comment_statement : COMMENT_TYPE_ONE
                  | COMMENT_TYPE_TWO
                  ;

/* Delete Statement Syntax:

    DELETE;

*/
delete_statement : DELETE SEMICOLON;

/*
    Format Statement syntax:

    FORMAT variable-1 <. . . variable-n> <format> <DEFAULT=default-format>;
    FORMAT variable-1 <. . . variable-n> format <DEFAULT=default-format>;
    FORMAT variable-1 <. . . variable-n> format variable-1 <. . . variable-n> format;
*/
format_statement : FORMAT variable_or_list+ default_format display_format SEMICOLON
                 | FORMAT variable_or_list+ display_format default_format SEMICOLON
                 | FORMAT variable_or_list+ display_format SEMICOLON
                 | FORMAT variable_or_list+ SEMICOLON
                 | FORMAT format+ SEMICOLON
                 ;

display_format : CHARACTER_FORMAT
               | DATE_TIME_FORMAT
               | NUMERIC_FORMAT
               ;

default_format: DEFAULT EQUAL first=display_format (second=display_format)?;

format : (variable_or_list+ display_format)
       | default_format
       ;

/*
    Attrib Statement syntax:

    ATTRIB variable-list(s) attribute-list(s)
*/
attrib_statement : ATTRIB attributes+ SEMICOLON
                 ;

// types of attributes
attribute : FORMAT EQUAL display_format
          | INFORMAT EQUAL display_format
          | LABEL EQUAL STRING
          | LENGTH EQUAL LENGTH_FORMAT
          | TRANSCODE EQUAL transcode=(YES | NO)
          ;

attributes : variable_or_list+ attribute+;

filename_statement : FILENAME fileref=variable device_type? filename=STRING encoding? filename_options? openv_options? SEMICOLON
                   | FILENAME fileref=variable device_type? filename_options? openv_options? SEMICOLON
                   | FILENAME ( fileref=variable | UNDERSCORE_ALL ) CLEAR SEMICOLON
                   | FILENAME ( fileref=variable | UNDERSCORE_ALL ) LIST SEMICOLON
                   ;

device_type : DISK
            | DUMMY
            | GTERM
            | PIPE
            | PLOTTER
            | PRINTER
            | TAPE
            | TEMP
            | TERMINAL
            | UPRINTER
            ;

encoding : ENCODING EQUAL STRING;

filename_options : RECFM EQUAL display_format;

// we probably shouldn't support this
openv_options : CHARACTER EQUAL STRING;

// A proc step is a group of SAS statements that call and execute a procedure
proc_step : proc_statement run_statement?;

// List of possible proc step procedures --> Expand with project scope
proc_statement : proc_append_statement
               | proc_datasets_statement
               | proc_format_statement
               | proc_freq_statement proc_freq_optional_statements*
               | proc_means_statement
               | proc_print_statement proc_print_optional_statements*
               | proc_sort_statement
               | proc_summary_statement
               | proc_transpose_statement
               ;

/*
    FREQ Procedure Syntax:

    PROC FREQ <options> ;
        BY variables ;
        EXACT statistic-options </ computation-options> ;
        OUTPUT <OUT=SAS-data-set> options ;
        TABLES requests </ options> ;
        TEST options ;
        WEIGHT variable </ option> ;
*/

proc_freq_optional_statements : by_statement
                              | exact_statement
                              | output_statement
                              | tables_statement
                              | test_statement
                              | weight_statement
                              ;

/*
    PRINT Procedure Syntax:

    PROC PRINT <option(s)>;
        BY <DESCENDING> variable-1 <...<DESCENDING> variable-n><NOTSORTED>;
        PAGEBY BY-variable;
        SUMBY BY-variable;
        ID variable(s) <option>;
        SUM variable(s) <option>;
        VAR variable(s) <option>;
*/

proc_print_optional_statements : by_statement
                               | id_statement
                               | var_statement
                               ; // --> Expand with project scope

/*----------------------------------------BEGIN PROC DATASETS MADNESS-------------------------------------------------*/

/*
    Dear future contributors,

    The vast majority of the statements and options present in PROC DATASETS are ridiculously out of scope for the
    current project. However, there are a couple that must be parsed because their actions are similar or identical to
    other procedures and/or statements that are firmly in scope.

    Given that the scope could always widen, the full syntax and grammar of the DATASETS procedure is provided below.
    Comments appear before each grammar item indicating how many layers of uselessness the rule is on, starting from
    zero at the top of the grammar tree for "proc_datasets_statement". Statements or options which are actually useful
    will instead have a comment indicating this.

    Should any future contributor see a good reason to keep an item which is currently marked as useless, feel free to
    replace the uselessness comment with a comment describing its usefulness for your purposes. Comments not related to
    usefulness are also welcome, and many already exist.

    Finally, should any unfortunate soul be tasked with implementing the visitor methods for n-dimensionally nested
    options currently marked useless, be advised that your code will be no more readable than a bowl of alphabet soup.

    You have been warned.

    Sincerely,

    A concerned citizen

    P.S. Feel free to remove *this* comment if the tables have miraculously turned to most of the rules being *useful*.
*/

/*
    DATASETS Procedure Syntax:

    PROC DATASETS <<option-1 <...option-n>>>;

        AGE current-name related-SAS-file-1 <...current-name related-SAS-file-n>
        </ <ALTER=alter-password>
        <MEMTYPE=mtype>>;

        APPEND BASE=<libref.>SAS-data-set
        <APPENDVER=V6>
        <DATA=<libref.>SAS-data-set>
        <FORCE>
        <GETSORT>
        <NOWARN>;

        AUDIT SAS-file <(SAS-password)>;
            INITIATE
                <AUDIT_ALL=NO|YES>;
                <LOG <ADMIN_IMAGE=YES|NO>
                <BEFORE_IMAGE=YES|NO>
                <DATA_IMAGE=YES|NO>
                <ERROR_IMAGE=YES|NO>>;
                <USER_VAR variable-1 <... variable-n>>;

        AUDIT SAS-file <(<SAS-password> <GENNUM= integer>)>;
        SUSPEND|RESUME|TERMINATE;

        CHANGE old-name-1=new-name-1
        <...old-name-n=new-name-n >
        </ <ALTER=alter-password>
        <GENNUM=ALL|integer>
        <MEMTYPE=mtype>>;

        CONTENTS <option-1 <...option-n>>;

        COPY OUT=libref-1
        <CLONE|NOCLONE>
        <CONSTRAINT=YES|NO>
        <DATECOPY>
        <FORCE>
        <IN=libref-2>
        <INDEX=YES|NO>
        <MEMTYPE=(mtype-1 <...mtype-n>)>
        <MOVE <ALTER=alter-password>>;

            EXCLUDE SAS-file-1 <...SAS-file-n> < / MEMTYPE=mtype>;

            SELECT SAS-file-1 <...SAS-file-n>
            </ <ALTER=alter-password>
            <MEMTYPE= mtype>>;

        DELETE SAS-file-1 <...SAS-file-n>
        </ <ALTER=alter-password>
        <GENNUM=ALL|HIST|REVERT|integer>
        <MEMTYPE=mtype>>;

        EXCHANGE name-1=other-name-1
        <...name-n=other-name-n>
        </ <ALTER=alter-password>
        <MEMTYPE=mtype> >;

        MODIFY SAS-file <(option-1 <...option-n>)>
        </ <CORRECTENCODING=encoding-value>
        <DTC=SAS-date-time>
        <GENNUM=integer>
        <MEMTYPE=mtype>>;

            ATTRIB variable list(s) attribute list(s);

            FORMAT variable-1 <format-1>
            <...variable-n <format-n>>;

            IC CREATE <constraint-name=> constraint
            <MESSAGE='message-string' <MSGTYPE=USER>>;

            IC DELETE constraint-name-1 <...constraint-name-n>| _ALL_;

            IC REACTIVATE foreign-key-name REFERENCES libref;

            INDEX CENTILES index-1 <...index-n>
            </ <REFRESH>
            <UPDATECENTILES= ALWAYS|NEVER|integer>>;

            INDEX CREATE index-specification-1 <...index-specification-n>
            </ <NOMISS>
            <UNIQUE>
            <UPDATECENTILES=ALWAYS|NEVER|integer>>;

            INDEX DELETE index-1 <...index-n> | _ALL_;

            INFORMAT variable-1 <informat-1>
            <...variable-n <informat-n>>;

            LABEL variable-1=<'label-1'|' '>
            <...variable-n=<'label-n'|' ' >>;

            RENAME old-name-1=new-name-1
            <...old-name-n=new-name-n>;

        REBUILD SAS-file </ ALTER=password GENNUM=n MEMTYPE=mtype NOINDEX>;

        REPAIR SAS-file-1 <...SAS-file-n>
        </ <ALTER=alter-password>
        <GENNUM=integer>
        <MEMTYPE=mtype>>;

        SAVE SAS-file-1 <...SAS-file-n> </ MEMTYPE=mtype>;
*/
proc_datasets_statement : PROC DATASETS proc_datasets_options* proc_datasets_statements*;

// uselessness layers: 1
proc_datasets_options : alter_option
                      | details_option
                      | FORCE
                      | gennum_option
                      | KILL
                      | LIBRARY EQUAL libref=variable
                      | memtype_option
                      | NOLIST
                      | NOWARN
                      | PW EQUAL password=IDENTIFIER
                      | READ EQUAL password=IDENTIFIER
                      ;

// options common to a lot of PROC statements
// uselessness layers are 1 more than the parent command
alter_option   : ALTER EQUAL alter=IDENTIFIER;
details_option : DETAILS | NODETAILS;
gennum_option  : GENNUM EQUAL (ALL | HIST | REVERT | NUMBER_KEYWORD);
memtype_option : MEMTYPE EQUAL mtype=variable;

// uselessness layers: 2
proc_datasets_statements : /*age_statement          // "age" is a very common variable name; conflict resolution needed
                         | */append_statement
                         | audit_statement
                         | change_statement
                         | contents_statement
                         | copy_statement
                         | proc_datasets_delete     // not the same as a DELETE statement in a Data step
                         | exchange_statement
                         | proc_datasets_modify     // not the same as a MODIFY statement in a Data step
                         | rebuild_statement
                         | repair_statement
                         | proc_datasets_save       // not the same as a SAVE statement in a Data step
                         ;

// uselessness layers: 3
//age_statement : AGE age_arguments+ alter_option? memtype_option? SEMICOLON;

// uselessness layers: 4
age_arguments : current=variable related=variable;

/*---------------------------USEFUL COMMAND------------------------------*/
proc_append_statement : PROC append_statement;

// separated out because it's the same in PROC APPEND and PROC DATASETS
append_statement : APPEND BASE EQUAL base=data_set_name
                   (APPENDVER EQUAL V6)?
                   (DATA EQUAL data=data_set_name)?
                   FORCE? GETSORT? NOWARN? SEMICOLON;
/*-------------------------END USEFUL COMMAND----------------------------*/

// uselessness layers: 3
audit_statement : AUDIT file=variable password=IDENTIFIER SEMICOLON INITIATE audit_options
                | AUDIT file=variable (password=IDENTIFIER)? gennum_option? (SUSPEND | RESUME | TERMINATE) SEMICOLON
                ;

// uselessness layers: 4
audit_options : AUDIT_ALL EQUAL (NO | YES) SEMICOLON
              | LOG log_option* SEMICOLON
              | USER_VAR variable+ SEMICOLON
              ;

// uselessness layers: 5
log_option : ADMIN_IMAGE EQUAL (YES | NO)
           | BEFORE_IMAGE EQUAL (YES | NO)
           | DATA_IMAGE EQUAL (YES | NO)
           | ERROR_IMAGE EQUAL (YES | NO)
           ;
// uselessness layers: 3
change_statement : CHANGE rename_pair+ alter_option? gennum_option? memtype_option? SEMICOLON;

// uselessness layers: 3
contents_statement : CONTENTS contents_options* SEMICOLON;

// uselessness layers: 4
contents_options : CENTILES
                 | DATA EQUAL data_arguments
                 | details_option
                 | DIRECTORY
                 | FMTLEN
                 | memtype_option
                 | NODS
                 | NOPRINT
                 | ORDER EQUAL (COLLATE | CASECOLLATE | IGNORECASE | VARNUM)
                 | OUT EQUAL data_arguments
                 | OUT2 EQUAL data_arguments
                 | SHORT
                 | VARNUM
                 ;

// uselessness layers: 3
copy_statement : COPY OUT EQUAL out=variable copy_options* copy_statements* SEMICOLON;

// uselessness layers: 4
copy_options : CLONE | NOCLONE
             | CONSTRAINT EQUAL (YES | NO)
             | DATECOPY
             | FORCE
             | IN EQUAL variable
             | INDEX EQUAL (YES | NO)
             | memtype_option
             | MOVE alter_option?
             ;

// uselessness layers: 4
copy_statements : EXCLUDE variable+ memtype_option? SEMICOLON
                | SELECT variable+ alter_option? memtype_option? SEMICOLON
                ;

// uselessness layers: 3
proc_datasets_delete : DELETE variable+ alter_option? memtype_option? gennum_option? SEMICOLON;

// uselessness layers: 3
exchange_statement : EXCHANGE rename_pair+ alter_option? memtype_option? SEMICOLON;

// uselessness layers: 3
proc_datasets_modify : MODIFY variable proc_modify_options* SEMICOLON modify_statements*;

// uselessness layers: 4
proc_modify_options : CORRECTENCODING EQUAL IDENTIFIER
                    | DTC EQUAL NUMERIC      // TODO: create a date/time lexer rule (not useless)
                    | GENMAX EQUAL NUMERIC
                    | gennum_option
                    | label_equals_option
                    | memtype_option
                    | password_option
                    | SORTEDBY EQUAL IDENTIFIER+    // documentation of this option is too vague; no examples provided
                    //| TYPE EQUAL IDENTIFIER       // conflict resolution needed
                    ;

// uselessness layers: 5
password_option : ALTER EQUAL password_modification
                | PW EQUAL password_modification
                | READ EQUAL password_modification
                | WRITE EQUAL password_modification
                ;

// uselessness layers: 6
password_modification : new_password=IDENTIFIER
                      | old_password=IDENTIFIER DIVISION new_password=IDENTIFIER
                      | DIVISION new_password=IDENTIFIER
                      | old_password=IDENTIFIER DIVISION
                      | DIVISION
                      ;

// uselessness layers: 4
modify_statements : attrib_statement
                  | format_statement
                  | IC CREATE constraint_name? constraint (MESSAGE EQUAL STRING (MSGTYPE EQUAL USER)?)? SEMICOLON
                  | IC DELETE (variable+) | UNDERSCORE_ALL SEMICOLON
                  | IC REACTIVATE foreign_key=variable REFERENCES libref=variable SEMICOLON
                  | INDEX CENTILES variable+ (REFRESH)? (UPDATECENTILES EQUAL (ALWAYS | NEVER | NUMERIC))? SEMICOLON
                  | INDEX CREATE index_spec+ (NOMISS)? (UNIQUE)? (UPDATECENTILES EQUAL (ALWAYS | NEVER | NUMERIC))? SEMICOLON
                  | INDEX DELETE (variable+) | UNDERSCORE_ALL SEMICOLON
                  | INFORMAT (variable_or_list+ display_format)+ SEMICOLON  // no point in making a separate informat rule
                  | label_statement
                  | rename_statement
                  ;

// uselessness layers: 5
constraint_name : name=variable EQUAL;

// uselessness layers: 5
constraint : NOT NULL OPEN_PAREN variable CLOSE_PAREN
           | (UNIQUE | DISTINCT) OPEN_PAREN variable+ CLOSE_PAREN
           | CHECK OPEN_PAREN where_expression CLOSE_PAREN
           | PRIMARY KEY OPEN_PAREN variable+ CLOSE_PAREN
           | FOREIGN KEY variable+ REFERENCES table_name=variable on_delete? on_update?
           ;

// SQL relics (uselessness layers: 6)
on_delete : ON DELETE (RESTRICT | SET NULL | CASCADE);
on_update : ON UPDATE (RESTRICT | SET NULL | CASCADE);

// uselessness layers: 5
index_spec : variable
           | INDEX EQUAL OPEN_PAREN variable+ CLOSE_PAREN
           ;

// uselessness layers: 3
rebuild_statement : REBUILD data_set_name alter_option? memtype_option? gennum_option? NOINDEX SEMICOLON;

// uselessness layers: 3
repair_statement : REPAIR data_set_name+ alter_option? memtype_option? gennum_option? SEMICOLON;

// uselessness layers: 3
proc_datasets_save : SAVE data_set_name+ memtype_option? SEMICOLON;

/*-------------------------------------------END PROC DATASETS MADNESS------------------------------------------------*/

/*
    FORMAT Procedure Syntax:

    PROC FORMAT <option(s)>;
        EXCLUDE entry(s);
        INVALUE <$>name <(informat-option(s))>
        value-range-set(s);
        PICTURE name <(format-option(s))>
        value-range-set-1 <(picture-1-option(s) )>
        <...value-range-set-n <(picture-n-option(s))>>;
        SELECT entry(s);
        VALUE <$>name <(format-option(s))>
        value-range-set(s);
*/

proc_format_statement : PROC FORMAT proc_format_options* format_statements* SEMICOLON;

// <option(s)>
proc_format_options : CASFMTLIB EQUAL STRING
                    | CNTLIN EQUAL data_set_name
                    | CNTLOUT EQUAL data_set_name
                    | FMTLIB
                    | LIBRARY EQUAL libref=variable (DOT catalog=variable)?
                    | LOCALE
                    | MAXLABLEN EQUAL NUMERIC
                    | MAXSELEN EQUAL NUMERIC
                    | NOREPLACE
                    | PAGE
                    ;

/*
note: the FORMAT procedure requires:

     1. All character formats must be preceded with a dollar sign ($)
     2. All informats must be preceded with an at sign (@)
     3. The at sign (@) precedes the dollar sign ($) when both are needed
*/
format_entry : AT_SIGN DOLLAR_SIGN? display_format;

format_statements : exclude_statement
                  | invalue_statement
                  //| picture_statement     // this is far too complicated to be in scope right now
                  | proc_format_select
                  | value_statement
                  ;

exclude_statement : EXCLUDE format_entry+;

/*
    INVALUE Statement Syntax:

    INVALUE <$>name <(informat-options)> <value-range-set(s)>;
*/
invalue_statement : INVALUE DOLLAR_SIGN? name=variable informat_options* value_range_set*;

informat_options : DEFAULT EQUAL length=NUMERIC
                 | FUZZ EQUAL fuzz=NUMERIC
                 | JUST
                 | MAX EQUAL length=NUMERIC
                 | MIN EQUAL length=NUMERIC
                 | NOTSORTED
                 | REGEXP
                 | REGEXPE
                 | UPCASE
                 ;

proc_format_select : SELECT format_entry+;

/*
    VALUE Statement Syntax:

    VALUE <$>name <(format-option(s))> <value-range-set(s)>;
*/

value_statement : VALUE DOLLAR_SIGN? name=variable format_options* value_range_set* SEMICOLON;

// <(format-option(s))>
format_options : DEFAULT EQUAL length=NUMERIC
               | FUZZ EQUAL fuzz=NUMERIC
               | MAX EQUAL length=NUMERIC
               | MIN EQUAL length=NUMERIC
               | MULTILABEL
               | NOTSORTED
               ;

// <value-range-set(s)>
value_range_set : value_or_range+ EQUAL (informatted_value | existing_format);

informatted_value : STRING
                  | NUMERIC
                  | UNDERSCORE_ERROR
                  | UNDERSCORE_SAME
                  ;

existing_format : LEFT_SQUARE display_format RIGHT_SQUARE
                | OPEN_PAREN VERTICAL_BAR display_format VERTICAL_BAR CLOSE_PAREN
                ;

/*
    TRANSPOSE Procedure Syntax:

    PROC TRANSPOSE <DATA=input-data-set> <DELIMITER=delimiter> <LABEL=label>
    <LET> <NAME=name> <OUT=output-data-set> <PREFIX=prefix> <SUFFIX=suffix>;
        BY <DESCENDING> variable-1
        <<DESCENDING>variable-2 ...>
        <NOTSORTED>;
        COPY variable(s);
        ID variable;
            IDLABEL variable;
        VAR variable(s);
*/
proc_transpose_statement : PROC TRANSPOSE transpose_options* SEMICOLON transpose_statements;

transpose_options : DATA EQUAL dataset=data_set_name
                  | DELIMITER EQUAL STRING
                  | LABEL EQUAL STRING
                  | LET
                  //| NAME EQUAL variable // name is a very common variable name; conflict resolution needed
                  | OUT EQUAL dataset=data_set_name
                  | PREFIX EQUAL STRING
                  | SUFFIX EQUAL STRING
                  ;

transpose_statements : by_statement
                     | copy_statement
                     | id_statement
                     | var_statement
                     ;

/*
    SAS Expression: A sequence of operands and operators
*/
expression : unary_expression
           | binary_expression
           | parenthetical
           | function
           | constant
           | variable
           ;

// Unary expression: that which has only one operand
unary_expression : prefix_operator operand
                 | prefix_operator expression;

// Binary expression: that which has two operands
binary_expression : left=operand                    infix_operator right=operand
                  | left=operand                    infix_operator right_expr=expression
                  | left_unary=unary_expression     infix_operator right=operand
                  | left_unary=unary_expression     infix_operator right_expr=expression
                  | left_binary=binary_expression   infix_operator right=operand
                  | left_binary=binary_expression   infix_operator right_expr=expression
                  ;

// Prefix operator: that which comes before an expression
prefix_operator : SUBTRACTION
                | ADDITION
                ;

// Infix operator: that which comes between two expressions
infix_operator : SUBTRACTION
               | ADDITION
               | DIVISION
               | STAR
               | STARSTAR
               | MAXIMUM
               | MINIMUM
               ;

// Operand: the atomic building blocks of expressions
operand : variable
        | constant
        | function
        | parenthetical
        ;

// Comparison: Symbols or mnemonic equivalents
comparison : EQUAL
           | NOT_EQUAL
           | GREATER_THAN
           | LESS_THAN
           | GREATER_THAN_OR_EQUAL
           | LESS_THAN_OR_EQUAL
           | IN
           ;

/*
array_dereference : array_name=variable OPEN_PAREN index=NUMERIC CLOSE_PAREN
                  | array_name=variable OPEN_PAREN variable_index=variable CLOSE_PAREN;
*/

/*
    Function Syntax:  --> Expand with project scope
        One of:
        1. function-name (argument-1<, ...argument-n>)
        2. function-name (OF variable-list)
        3. function-name (<argument | OF variable-list | OF array-name[*]><..., <argument |
                                      OF variable-list | OF array-name[*]>>)

        Only #1 has been implemented so far
*/
function : function_name=variable OPEN_PAREN arguments CLOSE_PAREN;

// List of function arguments
arguments : argument (COMMA argument)* ;

// Function Argument: Can be a variable name, constant, or SAS expression
argument : expression
         | variable_or_list
         | constant;

logical_expression : parenthetical_logic
                   | LOGICAL_NOT right_expression=logical_expression
                   | left_operand=operand               comparison   right_operand=operand
                   | left_operand=operand               comparison   right_expression=logical_expression
                   | left_operand=operand               binary_logic right_operand=operand
                   | left_operand=operand               binary_logic right_expression=logical_expression
                   | left_expression=logical_expression binary_logic right_operand=operand
                   | left_expression=logical_expression binary_logic right_expression=logical_expression
                   ;

// SAS Logical: Any logical operator
binary_logic : LOGICAL_AND
             | LOGICAL_OR
             ;

// SAS Grouping Parenthesis: One or more SAS expressions inside parenthesis
parenthetical : OPEN_PAREN expression CLOSE_PAREN ;

parenthetical_logic : OPEN_PAREN logical_expression CLOSE_PAREN;

// SAS Constants: Character, Numeric, Date / Time / Datetime, Bit testing
//  --> Expand with project scope
constant : STRING
         | NUMERIC
         | DOT
         ;

/*
    SAS Value List Syntax:

    m
    m1 m2 ... mn
    m1,m2,...,mn
    m TO n <BY increment>
*/
value_list : NUMERIC+
           | NUMERIC (COMMA NUMERIC)+
           | first_to=NUMERIC TO last_to=NUMERIC (BY increment=NUMERIC)?
           ;

value_or_range : constant
               | numeric_value_range
               | character_value_range
               ;

value_range_operation : SUBTRACTION
                      | exclude_bottom=LESS_THAN SUBTRACTION
                      | SUBTRACTION exclude_top=LESS_THAN
                      | exclude_bottom=LESS_THAN SUBTRACTION exclude_top=LESS_THAN
                      ;

numeric_value_range : first=NUMERIC value_range_operation last=NUMERIC
                    | first=NUMERIC value_range_operation HIGH
                    | LOW value_range_operation last=NUMERIC
                    | LOW SUBTRACTION HIGH
                    | OTHER
                    ;

character_value_range : first=STRING value_range_operation last=STRING
                      | first=STRING value_range_operation HIGH
                      | LOW value_range_operation last=STRING
                      | LOW SUBTRACTION HIGH
                      | OTHER
                      ;

// catch-all data structure for the many cases where either a variable or a variable range is a valid argument
variable_or_list : variable
                 | variable_range_1
                 | variable_range_2
                 | of_list
                 ;

// shorthand for a list of variables; assumes said list was previously stored in a SAS array
of_list : OF variable;

// SAS Variable Range: expression used to apply an operation to a range of variables with similar names
// for example, the variable range sales1-sales5 is equivalent to sales1 sales2 sales3 sales4 sales5
variable_range_1 : first=variable SUBTRACTION last=variable;

// Another type of SAS variable range whose output list value is dependent on the order of variables in the data
variable_range_2 : first=variable SUBTRACTION SUBTRACTION last=variable;

// SAS Variables: Set of data values of type "character" or "numeric"
variable : CHARACTER
         | IDENTIFIER
         ;