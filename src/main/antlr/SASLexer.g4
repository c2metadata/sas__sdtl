/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified extensively by Alexander Mueller in 2019.
*/

lexer grammar SASLexer;

@lexer::members
{
    public boolean single_string_flag = false;
    public boolean double_string_flag = false;
    public boolean comment_flag = false;
    public boolean expr_flag = false;
    public boolean format_flag = false;
    public boolean attrib_flag = false;
    public boolean number_flag = false;

    public int num_parens = 0;

    public void setSingleStringFlag(boolean flag) { this.single_string_flag = flag; }
    public void setDoubleStringFlag(boolean flag) { this.double_string_flag = flag; }
    public void setCommentFlag(boolean flag) { this.comment_flag = flag; }
    public void setExprFlag(boolean flag) { this.expr_flag = flag; }
    public void setFormatFlag(boolean flag) { this.format_flag = flag; }
    public void setAttribFlag(boolean flag) { this.attrib_flag = flag; }
    public void setNumberFlag(boolean flag) { this.number_flag = flag; }

    public void incrementParen() { this.num_parens += 1; }
    public void decrementParen() { this.num_parens -= 1; }

    public boolean isSingleString() { return this.single_string_flag; }
    public boolean isDoubleString() { return this.double_string_flag; }
    public boolean isString() { return this.single_string_flag | this.double_string_flag; }
    public boolean isComment() { return this.comment_flag; }
    public boolean isExpr() { return this.expr_flag; }
    public boolean isFormat() { return this.format_flag; }
    public boolean isAttrib() { return this.attrib_flag; }
    public boolean isParen() { return this.num_parens > 0; }
    public boolean isNumber() { return this.number_flag; }
}

// Lexer Rules

// language keywords
APPEND : A P P E N D;
ARRAY : A R R A Y;
ATTRIB : A T T R I B { this.setAttribFlag(true); };
BETWEEN : B E T W E E N;
BY : B Y;
CLASS : C L A S S;
DATA : D A T A;
DELETE : D E L E T E;
DO : D O;
DROP : D R O P;
ELSE : E L S E;
EXACT : E X A C T;
FILENAME : F I L E N A M E;
FORMAT : F O R M A T { this.setFormatFlag(true); };
ID : I D;
IF : I F;
IS : I S;
INFORMAT : I N F O R M A T;
INVALUE : I N V A L U E;
KEEP : K E E P;
LABEL : L A B E L;
LENGTH : L E N G T H;
LIBNAME : L I B N A M E;
MEANS : M E A N S;
MERGE : M E R G E;
MISSING : M I S S I N G;
MODIFY : M O D I F Y;
NULL : N U L L;
OF : O F;
OTHERWISE : O T H E R W I S E;
OUTPUT : O U T P U T;
PAGE : P A G E;
PRINT : P R I N T;
PROC : P R O C;
REMOVE : R E M O V E;
RENAME : R E N A M E;
REPLACE : R E P L A C E;
RUN : R U N;
SELECT : S E L E C T;
SET : S E T;
SORT : S O R T;
TABLES : T A B L E S;
TEST : T E S T;
THEN : T H E N;
TO : T O;
TRANSPOSE : T R A N S P O S E;
TYPES : T Y P E S;
UNTIL : U N T I L;
UPDATE : U P D A T E;
VALUE : V A L U E;
VAR : V A R;
WAYS : W A Y S;
WEIGHT : W E I G H T;
WHEN : W H E N;
WHERE : W H E R E;
WHILE : W H I L E;

// statistic keywords
CLM : C L M;
CV : C V;
CSS : C S S;
KURT : K U R T;
KURTOSIS : K U R T O S I S;
LCLM : L C L M;
MAX : M A X;
MEAN : M E A N;
MEDIAN : M E D I A N;
MIN : M I N;
MODE : M O D E;
N_KEYWORD : N;
NMISS : N M I S S;
P1 : P '1';
P5 : P '5';
P10 : P '10';
P25 : P '25';
P50 : P '50';
P75 : P '75';
P90 : P '90';
P95 : P '95';
P99 : P '99';
PROBT : P R O B T;
PRT : P R T;
Q1 : Q '1';
Q2 : Q '2';
Q3 : Q '3';
QRANGE : Q R A N G E;
RANGE : R A N G E;
STDDEV : S T D D E V;
STD : S T D;
STDERR : S T D E R R;
SKEW : S K E W;
SKEWNESS : S K E W N E S S;
SUM : S U M;
SUMMARY : S U M M A R Y;
SUMWGT : S U M W G T;
T_KEYWORD : T;
UCLM : U C L M;
USS : U S S;

// option keywords
ACCESS : A C C E S S;
ADMIN_IMAGE : A D M I N '_' I M A G E;
//AGE : A G E;
ALL : A L L;
ALTER : A L T E R;
ALWAYS : A L W A Y S;
ANY : A N Y;
APPENDVER : A P P E N D V E R;
ASCII : A S C I I;
ASCIIANY : A S C I I A N Y;
AUDIT : A U D I T;
AUDIT_ALL : A U D I T '_' A L L;
BASE : B A S E;
BEFORE_IMAGE : B E F O R E '_' I M A G E;
BINARY : B I N A R Y;
CASCADE : C A S C A D E;
CASECOLLATE : C A S E C O L L A T E;
CASFMTLIB : C A S F M T L I B;
CENTILES : C E N T I L E S;
CHANGE : C H A N G E;
CHAR : C H A R;
CHARBYTES : C H A R B Y T E S;
CHARTYPE : C H A R T Y P E;
CHECK : C H E C K;
CLASSDATA : C L A S S D A T A;
CLEAR: C L E A R;
CLONE : C L O N E;
CNTLIN : C N T L I N;
CNTLOUT : C N T L O U T;
COLLATE : C O L L A T E;
COMPLETETYPES : C O M P L E T E T Y P E S;
COMPRESS : C O M P R E S S;
CONSTRAINT : C O N S T R A I N T;
CONTAINS : C O N T A I N S;
CONTENTS : C O N T E N T S;
COPY : C O P Y;
CORRECTENCODING : C O R R E C T E N C O D I N G;
CREATE : C R E A T E;
CUROBS : C U R O B S;
CVPENG : C V P E N G;
CVPENGINE : C V P E N G I N E;
CVPMULT : C V P M U L T;
CVPMULTIPLIER : C V P M U L T I P L I E R;
DANISH : D A N I S H;
DATASETS : D A T A S E T S;
DATA_IMAGE : D A T A '_' I M A G E;
DATECOPY : D A T E C O P Y;
DEFAULT : D E F A U L T;
DEFER : D E F E R;
DELIMITER : D E L I M I T E R;
DESCENDING : D E S C E N D I N G;
DESCENDTYPES : D E S C E N D T Y P E S;
DETAILS : D E T A I L S;
DIRECTORY : D I R E C T O R Y;
DISK : D I S K;
DISTINCT : D I S T I N C T;
DTC : D T C;
DUMMY : D U M M Y;
EBCDIC : E B C D I C;
EBCDICANY : E B C D I C A N Y;
ENCODING : E N C O D I N G;
END : E N D;
ERROR_IMAGE : E R R O R '_' I M A G E;
EXCHANGE : E X C H A N G E;
EXCLUDE : E X C L U D E;
EXCLUSIVE : E X C L U S I V E;
FINNISH : F I N N I S H;
FMTLEN : F M T L E N;
FMTLIB : F M T L I B;
FORCE : F O R C E;
FOREIGN : F O R E I G N;
FORMATTED : F O R M A T T E D;
FORMCHAR : F O R M C H A R;
FREQ : F R E Q;
FUZZ : F U Z Z;
GENMAX : G E N M A X;
GENNUM : G E N N U M;
GETSORT : G E T S O R T;
GROUPFORMAT : G R O U P F O R M A T;
GTERM : G T E R M;
HIGH : H I G H;
HIST : H I S T;
IC : I C;
IDMIN : I D M I N;
IGNORECASE : I G N O R E C A S E;
IMMEDIATE : I M M E D I A T E;
INDEX : I N D E X;
INDSNAME : I N D S N A M E;
INENCODING : I N E N C O D I N G;
INITIATE : I N I T I A T E;
INTERNAL : I N T E R N A L;
JUST : J U S T;
KEY: K E Y;
KEYRESET : K E Y R E S E T;
KILL : K I L L;
LET : L E T;
LIBRARY : L I B R A R Y;
LIKE : L I K E;
LIST: L I S T;
LOCALE : L O C A L E;
LOG : L O G;
LOW : L O W;
MAXLABLEN : M A X L A B L E N;
MAXSELEN : M A X S E L E N;
MEMTYPE : M E M T Y P E;
MESSAGE : M E S S A G E;
MISSINGCHECK : M I S S I N G C H E C K;
MOVE : M O V E;
MSGTYPE : M S G T Y P E;
MULTILABEL : M U L T I L A B E L;
//NAME : N A M E;
NEVER : N E V E R;
NLEVELS : N L E V E L S;
NOBS : N O B S;
NOCLONE : N O C L O N E;
NODETAILS : N O D E T A I L S;
NODS : N O D S;
NOINDEX : N O I N D E X;
NOLIST : N O L I S T;
NOMISS : N O M I S S;
NOMISSINGCHECK : N O M I S S I N G C H E C K;
NONOBS : N O N O B S;
NOPRINT : N O P R I N T;
NOREPLACE : N O R E P L A C E;
NORWEGIAN : N O R W E G I A N;
NOTSORTED : N O T S O R T E D;
NOWARN : N O W A R N;
NUMBER_KEYWORD : N U M B E R;
NWAY : N W A Y;
ON : O N;
OPEN : O P E N;
ORDER : O R D E R;
OTHER : O T H E R;
OUT2 : O U T '2';
OUT : O U T;
OUTENCODING : O U T E N C O D I N G;
OUTREP : O U T R E P;
PIPE : P I P E;
PLOTTER : P L O T T E R;
POINT : P O I N T;
POLISH : P O L I S H;
PREFIX : P R E F I X;
PRIMARY : P R I M A R Y;
PRINTER : P R I N T E R;
PW : P W;
REACTIVATE : R E A C T I V A T E;
READ : R E A D;
READONLY : R E A D O N L Y;
REBUILD : R E B U I L D;
RECFM : R E C F M { this.setFormatFlag(true); };
REFERENCES : R E F E R E N C E S;
REFRESH : R E F R E S H;
REGEXP : R E G E X P;
REGEXPE : R E G E X P E;
REPAIR : R E P A I R;
REPEMPTY : R E P E M P T Y;
RESTRICT : R E S T R I C T;
RESUME : R E S U M E;
REVERT : R E V E R T;
SAME : S A M E;
SAVE : S A V E;
SHORT : S H O R T;
SORTEDBY : S O R T E D B Y;
SUFFIX : S U F F I X;
SUSPEND : S U S P E N D;
SWEDISH : S W E D I S H;
TAPE : T A P E;
TEMP : T E M P;
TERMINAL : T E R M I N A L;
TERMINATE : T E R M I N A T E;
TRANSCODE : T R A N S C O D E;
//TYPE : T Y P E;
UNDERSCORE_ALL : '_' A L L '_';
UNDERSCORE_ERROR : '_' E R R O R '_';
UNDERSCORE_SAME : '_' S A M E '_';
UNFORMATTED : U N F O R M A T T E D;
UNIQUE : U N I Q U E;
UPCASE : U P C A S E;
UPDATECENTILES : U P D A T E C E N T I L E S;
UPDATEMODE : U P D A T E M O D E;
UPRINTER : U P R I N T E R;
USER : U S E R;
USER_VAR : U S E R '_' V A R;
V6 : V '6';
VARNUM : V A R N U M;
WRITE : W R I T E;


// Mnemonics
EQ : E Q;
NE : N E;
GT : G T;
LT : L T;
GE : G E;
LE : L E;
IN : I N;
YES: Y E S;
NO: N O;

// Comparison Operators
EQUAL : { this.setNumberFlag(false); } ( '=' | EQ );
NOT_EQUAL : ('^='| '~=' | NE );
GREATER_THAN : ( '>' | GT );
LESS_THAN : ( '<' | LT );
GREATER_THAN_OR_EQUAL : ( '>=' | GE );
LESS_THAN_OR_EQUAL : ( '<=' | LE );

// Comments cannot occur inside strings or espressions
COMMENT_TYPE_ONE: { !this.isString() && (!this.isExpr() && !this.isParen()) }? STAR .+? (('\r' | '\n' | '\r' '\n') .*?)*? SEMICOLON { this.setCommentFlag(false); };
COMMENT_TYPE_TWO: { !this.isString() && (!this.isExpr() && !this.isParen()) }? DIVISION STAR .*? (('\r' | '\n' | '\r' '\n') .*?)*? STAR DIVISION { this.setCommentFlag(false); };

// Arithmetic Operators
STARSTAR : STAR STAR;
STAR : {
            this.setNumberFlag(false);
            if((!this.isString() && !this.isExpr()) && !this.isParen())
                this.setCommentFlag(true);
       } '*';
DIVISION : '/';
ADDITION : '+';
SUBTRACTION : '-';
MAXIMUM : '<>';
MINIMUM : '><';
CONCATENATION : '||';

// Logial Operators
LOGICAL_AND : ('&' | AND );
LOGICAL_OR : ('|' | '!' | OR );
LOGICAL_NOT : ('¬' | '~' | NOT );
AND : A N D;
OR : O R;
NOT : N O T;

// Punctuation
OPEN_PAREN : '(' { this.incrementParen(); };
CLOSE_PAREN : ')' { this.decrementParen(); };
DOT : '.';
SEMICOLON : ';' {
                    this.setExprFlag(false);
                    this.setFormatFlag(false);
                    this.setAttribFlag(false);
                    this.setNumberFlag(false);
                };
COMMA : ',';
VERTICAL_BAR : '|' ;
LEFT_BRACKET : '{' ;
RIGHT_BRACKET : '}' ;
LEFT_SQUARE : '[' ;
RIGHT_SQUARE : ']' ;
DOLLAR_SIGN : '$' ;
AT_SIGN : '@' ;
QUESTION_MARK : '?';
DOUBLE_QUOTE : {
                   this.setNumberFlag(false);
                   if (this.isDoubleString())
                       this.setDoubleStringFlag(false);
                   else
                       this.setDoubleStringFlag(true);
               } '"' ;
SINGLE_QUOTE : {
                   this.setNumberFlag(false);
                   if (this.isSingleString())
                       this.setSingleStringFlag(false);
                   else
                       this.setSingleStringFlag(true);
               } '\'' ;

// display formats
CHARACTER_FORMAT: { this.isFormat() }? DOLLAR_SIGN CHARACTER+? NUMERIC_NO_DOT DOT;
DATE_TIME_FORMAT: { this.isFormat() }? ISO_FORMAT
                                     | CHARACTER+ NUMERIC_NO_DOT DOT
                                     | CHARACTER+ NUMERIC_NO_DOT DOT NUMERIC_NO_DOT;
ISO_FORMAT: { this.isFormat() }? B8601_FORMAT | E8601_FORMAT | N8601_FORMAT;
NUMERIC_FORMAT: { this.isFormat() }? NUMERIC_NO_DOT DOT NUMERIC_NO_DOT
                                   | CHARACTER+ NUMERIC_NO_DOT DOT NUMERIC_NO_DOT?;

B8601_FORMAT: { this.isFormat() }? 'B8601' CHARACTER+ NUMERIC_NO_DOT DOT NUMERIC_NO_DOT?;
E8601_FORMAT: { this.isFormat() }? 'E8601' CHARACTER+ NUMERIC_NO_DOT DOT NUMERIC_NO_DOT?;
N8601_FORMAT: { this.isFormat() }? DOLLAR_SIGN 'N8601' CHARACTER+ NUMERIC_NO_DOT DOT NUMERIC_NO_DOT;

LENGTH_FORMAT: { this.isAttrib() }? DOLLAR_SIGN? NUMERIC_NO_DOT;

NUMERIC_NO_DOT: { this.isFormat() }? { this.setNumberFlag(true); } [0-9]+;

// Other SAS Definitions
CHARACTER : [a-zA-Z_@#]+ {
                             this.setExprFlag(true);
                             this.setNumberFlag(false);
                         };
NUMERIC : [0-9]+ (DOT [0-9]+)? { this.setExprFlag(true); }; // Definition will expand as project scope increases
STRING : DOUBLE_QUOTE ~('\r' | '\n' | '"')* DOUBLE_QUOTE
       | SINGLE_QUOTE ~('\r' | '\n' | '\'')* SINGLE_QUOTE;
//NEWLINE : '\r' | '\n' | '\r' '\n';
//MISSING_VALUE : { (!this.isNumber() && !this.isFormat()) && (!this.isString() && !this.isComment()) }? DOT;
//MISSING_VALUE : DOT;

// Catches any other string not caught by above lexer rules
IDENTIFIER: [a-zA-Z_@#] [a-zA-Z_@0-9]* { this.setExprFlag(true); };

// Fragments are reusable building blocks for lexer rules
fragment A : ('A' | 'a');
fragment B : ('B' | 'b');
fragment C : ('C' | 'c');
fragment D : ('D' | 'd');
fragment E : ('E' | 'e');
fragment F : ('F' | 'f');
fragment G : ('G' | 'g');
fragment H : ('H' | 'h');
fragment I : ('I' | 'i');
fragment J : ('J' | 'j');
fragment K : ('K' | 'k');
fragment L : ('L' | 'l');
fragment M : ('M' | 'm');
fragment N : ('N' | 'n');
fragment O : ('O' | 'o');
fragment P : ('P' | 'p');
fragment Q : ('Q' | 'q');
fragment R : ('R' | 'r');
fragment S : ('S' | 's');
fragment T : ('T' | 't');
fragment U : ('U' | 'u');
fragment V : ('V' | 'v');
fragment W : ('W' | 'w');
fragment X : ('X' | 'x');
fragment Y : ('Y' | 'y');
fragment Z : ('Z' | 'z');

// Skip whitespace
WS: [ \n\t\r]+ -> skip;
