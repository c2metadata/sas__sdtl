SAS:

data gss3;
   set gss2;
age_comp=10*int(age/10);
run;

SDTL:
[{
    "Expression": {
        "arguments": [
            {
                "argumentName": "EXP1",
                "argumentValue": {"value": 10}
            },
            {
                "argumentName": "EXP2",
                "argumentValue": {
                    "arguments": [{
                        "argumentName": "EXP1",
                        "argumentValue": {
                            "arguments": [
                                {
                                    "argumentName": "EXP1",
                                    "argumentValue": {"variableName": "age"}
                                },
                                {
                                    "argumentName": "EXP2",
                                    "argumentValue": {"value": 10}
                                }
                            ],
                            "function": "division",
                            "isSdtlName": true
                        }
                    }],
                    "function": "int",
                    "isSdtlName": false
                }
            }
        ],
        "function": "multiplication",
        "isSdtlName": true
    },
    "Variable": "age_comp",
    "command": "compute"
}]


TreeVisitor Outline:
Note: "Variables" uses VariableReferenceBase -> VariableSymbolExpression

"visitAssignment_statement":

    - Set "Variable", "variableName" using sas_variable
    - Set "Expression" by traversing expression tree in same method as SPSS
