## SAS Parser endpoints

* POST **/api/sastosdtl** - pass in SAS code and metadata (see request body format below) and expect an SDTL JSON string in return.

## Request body format
```javascript
{
  "parameters": {
    "data_file_descriptions": 
    [
      {
        "input_file_name": "inputfile1.sas7bdat",
        "DDI_XML_file": "DDI_file.xml",
        "variables": "V1 V2 V3",
        "file_name_DDI": "datafile1"
      },
      {
        "input_file_name": "inputfile2.sas7bdat",
        "DDI_XML_file": "DDI_file.xml",
        "variables": "V4 V5 V6",
        "file_name_DDI": "datafile2"
      }
    ],
    "sas": "sas_code"
  }
}
```
where `sas_code` is the entire sas script. This parser does not currently support having multiple input scripts.

## Use the docker image
```
docker pull registry.gitlab.com/c2metadata/sas__sdtl:latest
docker run -d -p 8082:80 registry.gitlab.com/c2metadata/sas__sdtl
```
Warning: this port configuration has not been tested.

## SAS Support
The following operations are fully supported by the parser:
* Appending datasets with a `set` statement
* Single and multi-line comments
* Assignment statements
* Dropping, Keeping, and Renaming variables using `drop`/`keep`/`rename` statements or `drop=`/`keep=`/`rename=` dataset options
* Conditional execution of commands inside If/Then/Else blocks
* Conditional keeping of rows using a subsetting `if` statement or a `where=` dataset option
* Loading a dataset by specifying a file path in the `set` or `filename` statements
* Saving a dataset by specifying a file path in the `data` statement
* Setting a display format with a `format` statement or an `attrib` statement
* Defining values that should be interpreted as missing with a `missing` statement
* Attaching labels to variables with a `label` statement or an `attrib` statement
* Sorting rows by specific variables with a `proc sort` procedure

The following operations are supported for common uses, but may not parse correctly with certain option combinations:
* Collapsing a dataset using `proc means` or `proc summary` procedures
* Merging datasets with a `merge` statement
* Appending datasets with a `proc append` procedure or an `append` statement within a `proc datasets` procedure
* Loading or saving a dataset using a procedure
* Using a `libname` statement to specify the directory from which data files were loaded and should be saved to

The following operations are in progress, in many cases awaiting a decision regarding changes to the SDTL model:
* Looping over lists of variables or lists of values with `do...end` style loops (it is possible to have a `do...end` block without it being a loop, but this is not currently supported)

The following operations will be implemented in the future:
* Looping over lists of variables or lists of values with `do...while` and `do...until` style loops
* Merging datasets with two adjacent `set` statements
* Updating/Modifying a dataset with an `update` or `modify` command
* Conditional dropping of rows by including a `delete` statement inside an If/Then block
* Anything relevant that can be accomplished with `proc format`
* Reshaping datasets using `proc transpose`

The following will only be implemented if there is time:
* `select...when` blocks
* The SAS macro language

## Updates

### 01/16/2020
* Updated the WAR file
* Created this README
