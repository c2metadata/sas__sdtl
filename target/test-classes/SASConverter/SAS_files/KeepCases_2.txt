data case2;
    set case;
    rename A=Y;
    keep name A C D;
    A=B;
    where A=1;
    A=A+1;

data case3(where=(name^='sr2') drop=Y rename=(D=Y)) case4(where=(name^='e') rename=(G=A) keep=name G Y D);
    merge case(where=(B>1) rename=(D=G)) case2(where=(D<8));
    by name;
    drop B C;
    where B<7;

data case5;
    set case4(rename=(D=G)) case3;
    by name;
    if G and A;
