// Generated from SASParser.g4 by ANTLR 4.4
package main.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SASParser}.
 */
public interface SASParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_modify_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_modify_options(@NotNull SASParser.Proc_modify_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_modify_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_modify_options(@NotNull SASParser.Proc_modify_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#exclude_statement}.
	 * @param ctx the parse tree
	 */
	void enterExclude_statement(@NotNull SASParser.Exclude_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#exclude_statement}.
	 * @param ctx the parse tree
	 */
	void exitExclude_statement(@NotNull SASParser.Exclude_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_means_summary}.
	 * @param ctx the parse tree
	 */
	void enterProc_means_summary(@NotNull SASParser.Proc_means_summaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_means_summary}.
	 * @param ctx the parse tree
	 */
	void exitProc_means_summary(@NotNull SASParser.Proc_means_summaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#informatted_value}.
	 * @param ctx the parse tree
	 */
	void enterInformatted_value(@NotNull SASParser.Informatted_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#informatted_value}.
	 * @param ctx the parse tree
	 */
	void exitInformatted_value(@NotNull SASParser.Informatted_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#binary_logic}.
	 * @param ctx the parse tree
	 */
	void enterBinary_logic(@NotNull SASParser.Binary_logicContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#binary_logic}.
	 * @param ctx the parse tree
	 */
	void exitBinary_logic(@NotNull SASParser.Binary_logicContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#character_value_range}.
	 * @param ctx the parse tree
	 */
	void enterCharacter_value_range(@NotNull SASParser.Character_value_rangeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#character_value_range}.
	 * @param ctx the parse tree
	 */
	void exitCharacter_value_range(@NotNull SASParser.Character_value_rangeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#informat_options}.
	 * @param ctx the parse tree
	 */
	void enterInformat_options(@NotNull SASParser.Informat_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#informat_options}.
	 * @param ctx the parse tree
	 */
	void exitInformat_options(@NotNull SASParser.Informat_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#when_statement}.
	 * @param ctx the parse tree
	 */
	void enterWhen_statement(@NotNull SASParser.When_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#when_statement}.
	 * @param ctx the parse tree
	 */
	void exitWhen_statement(@NotNull SASParser.When_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#set_options}.
	 * @param ctx the parse tree
	 */
	void enterSet_options(@NotNull SASParser.Set_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#set_options}.
	 * @param ctx the parse tree
	 */
	void exitSet_options(@NotNull SASParser.Set_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#key_option}.
	 * @param ctx the parse tree
	 */
	void enterKey_option(@NotNull SASParser.Key_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#key_option}.
	 * @param ctx the parse tree
	 */
	void exitKey_option(@NotNull SASParser.Key_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#where_statement}.
	 * @param ctx the parse tree
	 */
	void enterWhere_statement(@NotNull SASParser.Where_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#where_statement}.
	 * @param ctx the parse tree
	 */
	void exitWhere_statement(@NotNull SASParser.Where_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#sort_direction}.
	 * @param ctx the parse tree
	 */
	void enterSort_direction(@NotNull SASParser.Sort_directionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#sort_direction}.
	 * @param ctx the parse tree
	 */
	void exitSort_direction(@NotNull SASParser.Sort_directionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_transpose_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_transpose_statement(@NotNull SASParser.Proc_transpose_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_transpose_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_transpose_statement(@NotNull SASParser.Proc_transpose_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#data_step}.
	 * @param ctx the parse tree
	 */
	void enterData_step(@NotNull SASParser.Data_stepContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#data_step}.
	 * @param ctx the parse tree
	 */
	void exitData_step(@NotNull SASParser.Data_stepContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#drop_statement}.
	 * @param ctx the parse tree
	 */
	void enterDrop_statement(@NotNull SASParser.Drop_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#drop_statement}.
	 * @param ctx the parse tree
	 */
	void exitDrop_statement(@NotNull SASParser.Drop_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_statement(@NotNull SASParser.Proc_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_statement(@NotNull SASParser.Proc_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#password_option}.
	 * @param ctx the parse tree
	 */
	void enterPassword_option(@NotNull SASParser.Password_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#password_option}.
	 * @param ctx the parse tree
	 */
	void exitPassword_option(@NotNull SASParser.Password_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#engine_options}.
	 * @param ctx the parse tree
	 */
	void enterEngine_options(@NotNull SASParser.Engine_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#engine_options}.
	 * @param ctx the parse tree
	 */
	void exitEngine_options(@NotNull SASParser.Engine_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_summary_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_summary_statement(@NotNull SASParser.Proc_summary_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_summary_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_summary_statement(@NotNull SASParser.Proc_summary_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#star_grouping}.
	 * @param ctx the parse tree
	 */
	void enterStar_grouping(@NotNull SASParser.Star_groupingContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#star_grouping}.
	 * @param ctx the parse tree
	 */
	void exitStar_grouping(@NotNull SASParser.Star_groupingContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#indsname_option}.
	 * @param ctx the parse tree
	 */
	void enterIndsname_option(@NotNull SASParser.Indsname_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#indsname_option}.
	 * @param ctx the parse tree
	 */
	void exitIndsname_option(@NotNull SASParser.Indsname_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#tables_statement}.
	 * @param ctx the parse tree
	 */
	void enterTables_statement(@NotNull SASParser.Tables_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#tables_statement}.
	 * @param ctx the parse tree
	 */
	void exitTables_statement(@NotNull SASParser.Tables_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_print_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_print_options(@NotNull SASParser.Proc_print_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_print_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_print_options(@NotNull SASParser.Proc_print_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#do_spec}.
	 * @param ctx the parse tree
	 */
	void enterDo_spec(@NotNull SASParser.Do_specContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#do_spec}.
	 * @param ctx the parse tree
	 */
	void exitDo_spec(@NotNull SASParser.Do_specContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#statistic_keyword}.
	 * @param ctx the parse tree
	 */
	void enterStatistic_keyword(@NotNull SASParser.Statistic_keywordContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#statistic_keyword}.
	 * @param ctx the parse tree
	 */
	void exitStatistic_keyword(@NotNull SASParser.Statistic_keywordContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#append_statement}.
	 * @param ctx the parse tree
	 */
	void enterAppend_statement(@NotNull SASParser.Append_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#append_statement}.
	 * @param ctx the parse tree
	 */
	void exitAppend_statement(@NotNull SASParser.Append_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#parenthetical}.
	 * @param ctx the parse tree
	 */
	void enterParenthetical(@NotNull SASParser.ParentheticalContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#parenthetical}.
	 * @param ctx the parse tree
	 */
	void exitParenthetical(@NotNull SASParser.ParentheticalContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#alter_option}.
	 * @param ctx the parse tree
	 */
	void enterAlter_option(@NotNull SASParser.Alter_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#alter_option}.
	 * @param ctx the parse tree
	 */
	void exitAlter_option(@NotNull SASParser.Alter_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#change_statement}.
	 * @param ctx the parse tree
	 */
	void enterChange_statement(@NotNull SASParser.Change_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#change_statement}.
	 * @param ctx the parse tree
	 */
	void exitChange_statement(@NotNull SASParser.Change_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#audit_statement}.
	 * @param ctx the parse tree
	 */
	void enterAudit_statement(@NotNull SASParser.Audit_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#audit_statement}.
	 * @param ctx the parse tree
	 */
	void exitAudit_statement(@NotNull SASParser.Audit_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#nobs_option}.
	 * @param ctx the parse tree
	 */
	void enterNobs_option(@NotNull SASParser.Nobs_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#nobs_option}.
	 * @param ctx the parse tree
	 */
	void exitNobs_option(@NotNull SASParser.Nobs_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_sort_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_sort_statement(@NotNull SASParser.Proc_sort_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_sort_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_sort_statement(@NotNull SASParser.Proc_sort_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#collapse_statements}.
	 * @param ctx the parse tree
	 */
	void enterCollapse_statements(@NotNull SASParser.Collapse_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#collapse_statements}.
	 * @param ctx the parse tree
	 */
	void exitCollapse_statements(@NotNull SASParser.Collapse_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#libref_options}.
	 * @param ctx the parse tree
	 */
	void enterLibref_options(@NotNull SASParser.Libref_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#libref_options}.
	 * @param ctx the parse tree
	 */
	void exitLibref_options(@NotNull SASParser.Libref_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#keep_equals_option}.
	 * @param ctx the parse tree
	 */
	void enterKeep_equals_option(@NotNull SASParser.Keep_equals_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#keep_equals_option}.
	 * @param ctx the parse tree
	 */
	void exitKeep_equals_option(@NotNull SASParser.Keep_equals_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#output_variables}.
	 * @param ctx the parse tree
	 */
	void enterOutput_variables(@NotNull SASParser.Output_variablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#output_variables}.
	 * @param ctx the parse tree
	 */
	void exitOutput_variables(@NotNull SASParser.Output_variablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#memtype_option}.
	 * @param ctx the parse tree
	 */
	void enterMemtype_option(@NotNull SASParser.Memtype_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#memtype_option}.
	 * @param ctx the parse tree
	 */
	void exitMemtype_option(@NotNull SASParser.Memtype_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#set_arguments}.
	 * @param ctx the parse tree
	 */
	void enterSet_arguments(@NotNull SASParser.Set_argumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#set_arguments}.
	 * @param ctx the parse tree
	 */
	void exitSet_arguments(@NotNull SASParser.Set_argumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#change_options}.
	 * @param ctx the parse tree
	 */
	void enterChange_options(@NotNull SASParser.Change_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#change_options}.
	 * @param ctx the parse tree
	 */
	void exitChange_options(@NotNull SASParser.Change_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement(@NotNull SASParser.If_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement(@NotNull SASParser.If_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#openv_options}.
	 * @param ctx the parse tree
	 */
	void enterOpenv_options(@NotNull SASParser.Openv_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#openv_options}.
	 * @param ctx the parse tree
	 */
	void exitOpenv_options(@NotNull SASParser.Openv_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#modify_options}.
	 * @param ctx the parse tree
	 */
	void enterModify_options(@NotNull SASParser.Modify_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#modify_options}.
	 * @param ctx the parse tree
	 */
	void exitModify_options(@NotNull SASParser.Modify_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#array_subscript}.
	 * @param ctx the parse tree
	 */
	void enterArray_subscript(@NotNull SASParser.Array_subscriptContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#array_subscript}.
	 * @param ctx the parse tree
	 */
	void exitArray_subscript(@NotNull SASParser.Array_subscriptContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#do_statement}.
	 * @param ctx the parse tree
	 */
	void enterDo_statement(@NotNull SASParser.Do_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#do_statement}.
	 * @param ctx the parse tree
	 */
	void exitDo_statement(@NotNull SASParser.Do_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#ways_statement}.
	 * @param ctx the parse tree
	 */
	void enterWays_statement(@NotNull SASParser.Ways_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#ways_statement}.
	 * @param ctx the parse tree
	 */
	void exitWays_statement(@NotNull SASParser.Ways_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#value_range_set}.
	 * @param ctx the parse tree
	 */
	void enterValue_range_set(@NotNull SASParser.Value_range_setContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#value_range_set}.
	 * @param ctx the parse tree
	 */
	void exitValue_range_set(@NotNull SASParser.Value_range_setContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#drop_equals_option}.
	 * @param ctx the parse tree
	 */
	void enterDrop_equals_option(@NotNull SASParser.Drop_equals_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#drop_equals_option}.
	 * @param ctx the parse tree
	 */
	void exitDrop_equals_option(@NotNull SASParser.Drop_equals_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#on_delete}.
	 * @param ctx the parse tree
	 */
	void enterOn_delete(@NotNull SASParser.On_deleteContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#on_delete}.
	 * @param ctx the parse tree
	 */
	void exitOn_delete(@NotNull SASParser.On_deleteContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#of_list}.
	 * @param ctx the parse tree
	 */
	void enterOf_list(@NotNull SASParser.Of_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#of_list}.
	 * @param ctx the parse tree
	 */
	void exitOf_list(@NotNull SASParser.Of_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#select_statement}.
	 * @param ctx the parse tree
	 */
	void enterSelect_statement(@NotNull SASParser.Select_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#select_statement}.
	 * @param ctx the parse tree
	 */
	void exitSelect_statement(@NotNull SASParser.Select_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#where_expression}.
	 * @param ctx the parse tree
	 */
	void enterWhere_expression(@NotNull SASParser.Where_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#where_expression}.
	 * @param ctx the parse tree
	 */
	void exitWhere_expression(@NotNull SASParser.Where_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#open_option}.
	 * @param ctx the parse tree
	 */
	void enterOpen_option(@NotNull SASParser.Open_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#open_option}.
	 * @param ctx the parse tree
	 */
	void exitOpen_option(@NotNull SASParser.Open_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_means_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_means_options(@NotNull SASParser.Proc_means_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_means_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_means_options(@NotNull SASParser.Proc_means_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#age_arguments}.
	 * @param ctx the parse tree
	 */
	void enterAge_arguments(@NotNull SASParser.Age_argumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#age_arguments}.
	 * @param ctx the parse tree
	 */
	void exitAge_arguments(@NotNull SASParser.Age_argumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#data_set_options}.
	 * @param ctx the parse tree
	 */
	void enterData_set_options(@NotNull SASParser.Data_set_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#data_set_options}.
	 * @param ctx the parse tree
	 */
	void exitData_set_options(@NotNull SASParser.Data_set_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#parenthetical_logic}.
	 * @param ctx the parse tree
	 */
	void enterParenthetical_logic(@NotNull SASParser.Parenthetical_logicContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#parenthetical_logic}.
	 * @param ctx the parse tree
	 */
	void exitParenthetical_logic(@NotNull SASParser.Parenthetical_logicContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#exact_statement}.
	 * @param ctx the parse tree
	 */
	void enterExact_statement(@NotNull SASParser.Exact_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#exact_statement}.
	 * @param ctx the parse tree
	 */
	void exitExact_statement(@NotNull SASParser.Exact_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#set_statement}.
	 * @param ctx the parse tree
	 */
	void enterSet_statement(@NotNull SASParser.Set_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#set_statement}.
	 * @param ctx the parse tree
	 */
	void exitSet_statement(@NotNull SASParser.Set_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#audit_options}.
	 * @param ctx the parse tree
	 */
	void enterAudit_options(@NotNull SASParser.Audit_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#audit_options}.
	 * @param ctx the parse tree
	 */
	void exitAudit_options(@NotNull SASParser.Audit_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_print_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_print_statement(@NotNull SASParser.Proc_print_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_print_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_print_statement(@NotNull SASParser.Proc_print_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#attribute}.
	 * @param ctx the parse tree
	 */
	void enterAttribute(@NotNull SASParser.AttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#attribute}.
	 * @param ctx the parse tree
	 */
	void exitAttribute(@NotNull SASParser.AttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#data_arguments}.
	 * @param ctx the parse tree
	 */
	void enterData_arguments(@NotNull SASParser.Data_argumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#data_arguments}.
	 * @param ctx the parse tree
	 */
	void exitData_arguments(@NotNull SASParser.Data_argumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#modify_statement}.
	 * @param ctx the parse tree
	 */
	void enterModify_statement(@NotNull SASParser.Modify_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#modify_statement}.
	 * @param ctx the parse tree
	 */
	void exitModify_statement(@NotNull SASParser.Modify_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_datasets_modify}.
	 * @param ctx the parse tree
	 */
	void enterProc_datasets_modify(@NotNull SASParser.Proc_datasets_modifyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_datasets_modify}.
	 * @param ctx the parse tree
	 */
	void exitProc_datasets_modify(@NotNull SASParser.Proc_datasets_modifyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_freq_optional_statements}.
	 * @param ctx the parse tree
	 */
	void enterProc_freq_optional_statements(@NotNull SASParser.Proc_freq_optional_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_freq_optional_statements}.
	 * @param ctx the parse tree
	 */
	void exitProc_freq_optional_statements(@NotNull SASParser.Proc_freq_optional_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#rename_pair}.
	 * @param ctx the parse tree
	 */
	void enterRename_pair(@NotNull SASParser.Rename_pairContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#rename_pair}.
	 * @param ctx the parse tree
	 */
	void exitRename_pair(@NotNull SASParser.Rename_pairContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#id_statement}.
	 * @param ctx the parse tree
	 */
	void enterId_statement(@NotNull SASParser.Id_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#id_statement}.
	 * @param ctx the parse tree
	 */
	void exitId_statement(@NotNull SASParser.Id_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(@NotNull SASParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(@NotNull SASParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_datasets_delete}.
	 * @param ctx the parse tree
	 */
	void enterProc_datasets_delete(@NotNull SASParser.Proc_datasets_deleteContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_datasets_delete}.
	 * @param ctx the parse tree
	 */
	void exitProc_datasets_delete(@NotNull SASParser.Proc_datasets_deleteContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#format_entry}.
	 * @param ctx the parse tree
	 */
	void enterFormat_entry(@NotNull SASParser.Format_entryContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#format_entry}.
	 * @param ctx the parse tree
	 */
	void exitFormat_entry(@NotNull SASParser.Format_entryContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#assignment_statement}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_statement(@NotNull SASParser.Assignment_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#assignment_statement}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_statement(@NotNull SASParser.Assignment_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#value_list}.
	 * @param ctx the parse tree
	 */
	void enterValue_list(@NotNull SASParser.Value_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#value_list}.
	 * @param ctx the parse tree
	 */
	void exitValue_list(@NotNull SASParser.Value_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#ascending}.
	 * @param ctx the parse tree
	 */
	void enterAscending(@NotNull SASParser.AscendingContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#ascending}.
	 * @param ctx the parse tree
	 */
	void exitAscending(@NotNull SASParser.AscendingContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_datasets_statements}.
	 * @param ctx the parse tree
	 */
	void enterProc_datasets_statements(@NotNull SASParser.Proc_datasets_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_datasets_statements}.
	 * @param ctx the parse tree
	 */
	void exitProc_datasets_statements(@NotNull SASParser.Proc_datasets_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#password_modification}.
	 * @param ctx the parse tree
	 */
	void enterPassword_modification(@NotNull SASParser.Password_modificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#password_modification}.
	 * @param ctx the parse tree
	 */
	void exitPassword_modification(@NotNull SASParser.Password_modificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#format_statements}.
	 * @param ctx the parse tree
	 */
	void enterFormat_statements(@NotNull SASParser.Format_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#format_statements}.
	 * @param ctx the parse tree
	 */
	void exitFormat_statements(@NotNull SASParser.Format_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#invalue_statement}.
	 * @param ctx the parse tree
	 */
	void enterInvalue_statement(@NotNull SASParser.Invalue_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#invalue_statement}.
	 * @param ctx the parse tree
	 */
	void exitInvalue_statement(@NotNull SASParser.Invalue_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#where_operation}.
	 * @param ctx the parse tree
	 */
	void enterWhere_operation(@NotNull SASParser.Where_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#where_operation}.
	 * @param ctx the parse tree
	 */
	void exitWhere_operation(@NotNull SASParser.Where_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#types_statement}.
	 * @param ctx the parse tree
	 */
	void enterTypes_statement(@NotNull SASParser.Types_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#types_statement}.
	 * @param ctx the parse tree
	 */
	void exitTypes_statement(@NotNull SASParser.Types_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#display_format}.
	 * @param ctx the parse tree
	 */
	void enterDisplay_format(@NotNull SASParser.Display_formatContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#display_format}.
	 * @param ctx the parse tree
	 */
	void exitDisplay_format(@NotNull SASParser.Display_formatContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#copy_statement}.
	 * @param ctx the parse tree
	 */
	void enterCopy_statement(@NotNull SASParser.Copy_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#copy_statement}.
	 * @param ctx the parse tree
	 */
	void exitCopy_statement(@NotNull SASParser.Copy_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#attributes}.
	 * @param ctx the parse tree
	 */
	void enterAttributes(@NotNull SASParser.AttributesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#attributes}.
	 * @param ctx the parse tree
	 */
	void exitAttributes(@NotNull SASParser.AttributesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#constraint}.
	 * @param ctx the parse tree
	 */
	void enterConstraint(@NotNull SASParser.ConstraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#constraint}.
	 * @param ctx the parse tree
	 */
	void exitConstraint(@NotNull SASParser.ConstraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#update_option}.
	 * @param ctx the parse tree
	 */
	void enterUpdate_option(@NotNull SASParser.Update_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#update_option}.
	 * @param ctx the parse tree
	 */
	void exitUpdate_option(@NotNull SASParser.Update_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#run_statement}.
	 * @param ctx the parse tree
	 */
	void enterRun_statement(@NotNull SASParser.Run_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#run_statement}.
	 * @param ctx the parse tree
	 */
	void exitRun_statement(@NotNull SASParser.Run_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#data_statement}.
	 * @param ctx the parse tree
	 */
	void enterData_statement(@NotNull SASParser.Data_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#data_statement}.
	 * @param ctx the parse tree
	 */
	void exitData_statement(@NotNull SASParser.Data_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#array_index}.
	 * @param ctx the parse tree
	 */
	void enterArray_index(@NotNull SASParser.Array_indexContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#array_index}.
	 * @param ctx the parse tree
	 */
	void exitArray_index(@NotNull SASParser.Array_indexContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#operand}.
	 * @param ctx the parse tree
	 */
	void enterOperand(@NotNull SASParser.OperandContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#operand}.
	 * @param ctx the parse tree
	 */
	void exitOperand(@NotNull SASParser.OperandContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#variable_range_2}.
	 * @param ctx the parse tree
	 */
	void enterVariable_range_2(@NotNull SASParser.Variable_range_2Context ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#variable_range_2}.
	 * @param ctx the parse tree
	 */
	void exitVariable_range_2(@NotNull SASParser.Variable_range_2Context ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_step}.
	 * @param ctx the parse tree
	 */
	void enterProc_step(@NotNull SASParser.Proc_stepContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_step}.
	 * @param ctx the parse tree
	 */
	void exitProc_step(@NotNull SASParser.Proc_stepContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_datasets_save}.
	 * @param ctx the parse tree
	 */
	void enterProc_datasets_save(@NotNull SASParser.Proc_datasets_saveContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_datasets_save}.
	 * @param ctx the parse tree
	 */
	void exitProc_datasets_save(@NotNull SASParser.Proc_datasets_saveContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#variable_range_1}.
	 * @param ctx the parse tree
	 */
	void enterVariable_range_1(@NotNull SASParser.Variable_range_1Context ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#variable_range_1}.
	 * @param ctx the parse tree
	 */
	void exitVariable_range_1(@NotNull SASParser.Variable_range_1Context ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#index_spec}.
	 * @param ctx the parse tree
	 */
	void enterIndex_spec(@NotNull SASParser.Index_specContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#index_spec}.
	 * @param ctx the parse tree
	 */
	void exitIndex_spec(@NotNull SASParser.Index_specContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#argument}.
	 * @param ctx the parse tree
	 */
	void enterArgument(@NotNull SASParser.ArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#argument}.
	 * @param ctx the parse tree
	 */
	void exitArgument(@NotNull SASParser.ArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(@NotNull SASParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(@NotNull SASParser.ConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_format_select}.
	 * @param ctx the parse tree
	 */
	void enterProc_format_select(@NotNull SASParser.Proc_format_selectContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_format_select}.
	 * @param ctx the parse tree
	 */
	void exitProc_format_select(@NotNull SASParser.Proc_format_selectContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#data_set_name}.
	 * @param ctx the parse tree
	 */
	void enterData_set_name(@NotNull SASParser.Data_set_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#data_set_name}.
	 * @param ctx the parse tree
	 */
	void exitData_set_name(@NotNull SASParser.Data_set_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#existing_format}.
	 * @param ctx the parse tree
	 */
	void enterExisting_format(@NotNull SASParser.Existing_formatContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#existing_format}.
	 * @param ctx the parse tree
	 */
	void exitExisting_format(@NotNull SASParser.Existing_formatContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_means_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_means_statement(@NotNull SASParser.Proc_means_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_means_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_means_statement(@NotNull SASParser.Proc_means_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#device_type}.
	 * @param ctx the parse tree
	 */
	void enterDevice_type(@NotNull SASParser.Device_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#device_type}.
	 * @param ctx the parse tree
	 */
	void exitDevice_type(@NotNull SASParser.Device_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#transpose_statements}.
	 * @param ctx the parse tree
	 */
	void enterTranspose_statements(@NotNull SASParser.Transpose_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#transpose_statements}.
	 * @param ctx the parse tree
	 */
	void exitTranspose_statements(@NotNull SASParser.Transpose_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void enterUnary_expression(@NotNull SASParser.Unary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void exitUnary_expression(@NotNull SASParser.Unary_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#descending}.
	 * @param ctx the parse tree
	 */
	void enterDescending(@NotNull SASParser.DescendingContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#descending}.
	 * @param ctx the parse tree
	 */
	void exitDescending(@NotNull SASParser.DescendingContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#format_statement}.
	 * @param ctx the parse tree
	 */
	void enterFormat_statement(@NotNull SASParser.Format_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#format_statement}.
	 * @param ctx the parse tree
	 */
	void exitFormat_statement(@NotNull SASParser.Format_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_freq_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_freq_options(@NotNull SASParser.Proc_freq_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_freq_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_freq_options(@NotNull SASParser.Proc_freq_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#output_statement}.
	 * @param ctx the parse tree
	 */
	void enterOutput_statement(@NotNull SASParser.Output_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#output_statement}.
	 * @param ctx the parse tree
	 */
	void exitOutput_statement(@NotNull SASParser.Output_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#test_statement}.
	 * @param ctx the parse tree
	 */
	void enterTest_statement(@NotNull SASParser.Test_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#test_statement}.
	 * @param ctx the parse tree
	 */
	void exitTest_statement(@NotNull SASParser.Test_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_datasets_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_datasets_options(@NotNull SASParser.Proc_datasets_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_datasets_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_datasets_options(@NotNull SASParser.Proc_datasets_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#by_statement}.
	 * @param ctx the parse tree
	 */
	void enterBy_statement(@NotNull SASParser.By_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#by_statement}.
	 * @param ctx the parse tree
	 */
	void exitBy_statement(@NotNull SASParser.By_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(@NotNull SASParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(@NotNull SASParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#where_equals_option}.
	 * @param ctx the parse tree
	 */
	void enterWhere_equals_option(@NotNull SASParser.Where_equals_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#where_equals_option}.
	 * @param ctx the parse tree
	 */
	void exitWhere_equals_option(@NotNull SASParser.Where_equals_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#type_grouping}.
	 * @param ctx the parse tree
	 */
	void enterType_grouping(@NotNull SASParser.Type_groupingContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#type_grouping}.
	 * @param ctx the parse tree
	 */
	void exitType_grouping(@NotNull SASParser.Type_groupingContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#filename_statement}.
	 * @param ctx the parse tree
	 */
	void enterFilename_statement(@NotNull SASParser.Filename_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#filename_statement}.
	 * @param ctx the parse tree
	 */
	void exitFilename_statement(@NotNull SASParser.Filename_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#logical_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_expression(@NotNull SASParser.Logical_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#logical_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_expression(@NotNull SASParser.Logical_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#gennum_option}.
	 * @param ctx the parse tree
	 */
	void enterGennum_option(@NotNull SASParser.Gennum_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#gennum_option}.
	 * @param ctx the parse tree
	 */
	void exitGennum_option(@NotNull SASParser.Gennum_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#data_step_block}.
	 * @param ctx the parse tree
	 */
	void enterData_step_block(@NotNull SASParser.Data_step_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#data_step_block}.
	 * @param ctx the parse tree
	 */
	void exitData_step_block(@NotNull SASParser.Data_step_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#value_or_range}.
	 * @param ctx the parse tree
	 */
	void enterValue_or_range(@NotNull SASParser.Value_or_rangeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#value_or_range}.
	 * @param ctx the parse tree
	 */
	void exitValue_or_range(@NotNull SASParser.Value_or_rangeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#contents_statement}.
	 * @param ctx the parse tree
	 */
	void enterContents_statement(@NotNull SASParser.Contents_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#contents_statement}.
	 * @param ctx the parse tree
	 */
	void exitContents_statement(@NotNull SASParser.Contents_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_append_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_append_statement(@NotNull SASParser.Proc_append_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_append_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_append_statement(@NotNull SASParser.Proc_append_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#comparison}.
	 * @param ctx the parse tree
	 */
	void enterComparison(@NotNull SASParser.ComparisonContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#comparison}.
	 * @param ctx the parse tree
	 */
	void exitComparison(@NotNull SASParser.ComparisonContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#delete_statement}.
	 * @param ctx the parse tree
	 */
	void enterDelete_statement(@NotNull SASParser.Delete_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#delete_statement}.
	 * @param ctx the parse tree
	 */
	void exitDelete_statement(@NotNull SASParser.Delete_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#label_equals_option}.
	 * @param ctx the parse tree
	 */
	void enterLabel_equals_option(@NotNull SASParser.Label_equals_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#label_equals_option}.
	 * @param ctx the parse tree
	 */
	void exitLabel_equals_option(@NotNull SASParser.Label_equals_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#constraint_name}.
	 * @param ctx the parse tree
	 */
	void enterConstraint_name(@NotNull SASParser.Constraint_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#constraint_name}.
	 * @param ctx the parse tree
	 */
	void exitConstraint_name(@NotNull SASParser.Constraint_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#variable_or_list}.
	 * @param ctx the parse tree
	 */
	void enterVariable_or_list(@NotNull SASParser.Variable_or_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#variable_or_list}.
	 * @param ctx the parse tree
	 */
	void exitVariable_or_list(@NotNull SASParser.Variable_or_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#collating_sequence_option}.
	 * @param ctx the parse tree
	 */
	void enterCollating_sequence_option(@NotNull SASParser.Collating_sequence_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#collating_sequence_option}.
	 * @param ctx the parse tree
	 */
	void exitCollating_sequence_option(@NotNull SASParser.Collating_sequence_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#label_statement}.
	 * @param ctx the parse tree
	 */
	void enterLabel_statement(@NotNull SASParser.Label_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#label_statement}.
	 * @param ctx the parse tree
	 */
	void exitLabel_statement(@NotNull SASParser.Label_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#format}.
	 * @param ctx the parse tree
	 */
	void enterFormat(@NotNull SASParser.FormatContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#format}.
	 * @param ctx the parse tree
	 */
	void exitFormat(@NotNull SASParser.FormatContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#array_statement}.
	 * @param ctx the parse tree
	 */
	void enterArray_statement(@NotNull SASParser.Array_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#array_statement}.
	 * @param ctx the parse tree
	 */
	void exitArray_statement(@NotNull SASParser.Array_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#output_part}.
	 * @param ctx the parse tree
	 */
	void enterOutput_part(@NotNull SASParser.Output_partContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#output_part}.
	 * @param ctx the parse tree
	 */
	void exitOutput_part(@NotNull SASParser.Output_partContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#log_option}.
	 * @param ctx the parse tree
	 */
	void enterLog_option(@NotNull SASParser.Log_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#log_option}.
	 * @param ctx the parse tree
	 */
	void exitLog_option(@NotNull SASParser.Log_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#encoding}.
	 * @param ctx the parse tree
	 */
	void enterEncoding(@NotNull SASParser.EncodingContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#encoding}.
	 * @param ctx the parse tree
	 */
	void exitEncoding(@NotNull SASParser.EncodingContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#point_option}.
	 * @param ctx the parse tree
	 */
	void enterPoint_option(@NotNull SASParser.Point_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#point_option}.
	 * @param ctx the parse tree
	 */
	void exitPoint_option(@NotNull SASParser.Point_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#otherwise_statement}.
	 * @param ctx the parse tree
	 */
	void enterOtherwise_statement(@NotNull SASParser.Otherwise_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#otherwise_statement}.
	 * @param ctx the parse tree
	 */
	void exitOtherwise_statement(@NotNull SASParser.Otherwise_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#rename_equals_option}.
	 * @param ctx the parse tree
	 */
	void enterRename_equals_option(@NotNull SASParser.Rename_equals_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#rename_equals_option}.
	 * @param ctx the parse tree
	 */
	void exitRename_equals_option(@NotNull SASParser.Rename_equals_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#keep_statement}.
	 * @param ctx the parse tree
	 */
	void enterKeep_statement(@NotNull SASParser.Keep_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#keep_statement}.
	 * @param ctx the parse tree
	 */
	void exitKeep_statement(@NotNull SASParser.Keep_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#copy_statements}.
	 * @param ctx the parse tree
	 */
	void enterCopy_statements(@NotNull SASParser.Copy_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#copy_statements}.
	 * @param ctx the parse tree
	 */
	void exitCopy_statements(@NotNull SASParser.Copy_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#prefix_operator}.
	 * @param ctx the parse tree
	 */
	void enterPrefix_operator(@NotNull SASParser.Prefix_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#prefix_operator}.
	 * @param ctx the parse tree
	 */
	void exitPrefix_operator(@NotNull SASParser.Prefix_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#value_range_operation}.
	 * @param ctx the parse tree
	 */
	void enterValue_range_operation(@NotNull SASParser.Value_range_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#value_range_operation}.
	 * @param ctx the parse tree
	 */
	void exitValue_range_operation(@NotNull SASParser.Value_range_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#transpose_options}.
	 * @param ctx the parse tree
	 */
	void enterTranspose_options(@NotNull SASParser.Transpose_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#transpose_options}.
	 * @param ctx the parse tree
	 */
	void exitTranspose_options(@NotNull SASParser.Transpose_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#copy_options}.
	 * @param ctx the parse tree
	 */
	void enterCopy_options(@NotNull SASParser.Copy_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#copy_options}.
	 * @param ctx the parse tree
	 */
	void exitCopy_options(@NotNull SASParser.Copy_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#weight_statement}.
	 * @param ctx the parse tree
	 */
	void enterWeight_statement(@NotNull SASParser.Weight_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#weight_statement}.
	 * @param ctx the parse tree
	 */
	void exitWeight_statement(@NotNull SASParser.Weight_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_print_optional_statements}.
	 * @param ctx the parse tree
	 */
	void enterProc_print_optional_statements(@NotNull SASParser.Proc_print_optional_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_print_optional_statements}.
	 * @param ctx the parse tree
	 */
	void exitProc_print_optional_statements(@NotNull SASParser.Proc_print_optional_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#compound_where}.
	 * @param ctx the parse tree
	 */
	void enterCompound_where(@NotNull SASParser.Compound_whereContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#compound_where}.
	 * @param ctx the parse tree
	 */
	void exitCompound_where(@NotNull SASParser.Compound_whereContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#missing_statement}.
	 * @param ctx the parse tree
	 */
	void enterMissing_statement(@NotNull SASParser.Missing_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#missing_statement}.
	 * @param ctx the parse tree
	 */
	void exitMissing_statement(@NotNull SASParser.Missing_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#default_format}.
	 * @param ctx the parse tree
	 */
	void enterDefault_format(@NotNull SASParser.Default_formatContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#default_format}.
	 * @param ctx the parse tree
	 */
	void exitDefault_format(@NotNull SASParser.Default_formatContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#initial_value_list}.
	 * @param ctx the parse tree
	 */
	void enterInitial_value_list(@NotNull SASParser.Initial_value_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#initial_value_list}.
	 * @param ctx the parse tree
	 */
	void exitInitial_value_list(@NotNull SASParser.Initial_value_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#format_options}.
	 * @param ctx the parse tree
	 */
	void enterFormat_options(@NotNull SASParser.Format_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#format_options}.
	 * @param ctx the parse tree
	 */
	void exitFormat_options(@NotNull SASParser.Format_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#binary_expression}.
	 * @param ctx the parse tree
	 */
	void enterBinary_expression(@NotNull SASParser.Binary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#binary_expression}.
	 * @param ctx the parse tree
	 */
	void exitBinary_expression(@NotNull SASParser.Binary_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#rebuild_statement}.
	 * @param ctx the parse tree
	 */
	void enterRebuild_statement(@NotNull SASParser.Rebuild_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#rebuild_statement}.
	 * @param ctx the parse tree
	 */
	void exitRebuild_statement(@NotNull SASParser.Rebuild_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#libname_statement}.
	 * @param ctx the parse tree
	 */
	void enterLibname_statement(@NotNull SASParser.Libname_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#libname_statement}.
	 * @param ctx the parse tree
	 */
	void exitLibname_statement(@NotNull SASParser.Libname_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#line}.
	 * @param ctx the parse tree
	 */
	void enterLine(@NotNull SASParser.LineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#line}.
	 * @param ctx the parse tree
	 */
	void exitLine(@NotNull SASParser.LineContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_freq_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_freq_statement(@NotNull SASParser.Proc_freq_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_freq_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_freq_statement(@NotNull SASParser.Proc_freq_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#end_option}.
	 * @param ctx the parse tree
	 */
	void enterEnd_option(@NotNull SASParser.End_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#end_option}.
	 * @param ctx the parse tree
	 */
	void exitEnd_option(@NotNull SASParser.End_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#details_option}.
	 * @param ctx the parse tree
	 */
	void enterDetails_option(@NotNull SASParser.Details_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#details_option}.
	 * @param ctx the parse tree
	 */
	void exitDetails_option(@NotNull SASParser.Details_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_datasets_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_datasets_statement(@NotNull SASParser.Proc_datasets_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_datasets_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_datasets_statement(@NotNull SASParser.Proc_datasets_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#curobs_option}.
	 * @param ctx the parse tree
	 */
	void enterCurobs_option(@NotNull SASParser.Curobs_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#curobs_option}.
	 * @param ctx the parse tree
	 */
	void exitCurobs_option(@NotNull SASParser.Curobs_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_format_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_format_options(@NotNull SASParser.Proc_format_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_format_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_format_options(@NotNull SASParser.Proc_format_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#numeric_value_range}.
	 * @param ctx the parse tree
	 */
	void enterNumeric_value_range(@NotNull SASParser.Numeric_value_rangeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#numeric_value_range}.
	 * @param ctx the parse tree
	 */
	void exitNumeric_value_range(@NotNull SASParser.Numeric_value_rangeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#output_name}.
	 * @param ctx the parse tree
	 */
	void enterOutput_name(@NotNull SASParser.Output_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#output_name}.
	 * @param ctx the parse tree
	 */
	void exitOutput_name(@NotNull SASParser.Output_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#freq_statement}.
	 * @param ctx the parse tree
	 */
	void enterFreq_statement(@NotNull SASParser.Freq_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#freq_statement}.
	 * @param ctx the parse tree
	 */
	void exitFreq_statement(@NotNull SASParser.Freq_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#attrib_statement}.
	 * @param ctx the parse tree
	 */
	void enterAttrib_statement(@NotNull SASParser.Attrib_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#attrib_statement}.
	 * @param ctx the parse tree
	 */
	void exitAttrib_statement(@NotNull SASParser.Attrib_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#filename_options}.
	 * @param ctx the parse tree
	 */
	void enterFilename_options(@NotNull SASParser.Filename_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#filename_options}.
	 * @param ctx the parse tree
	 */
	void exitFilename_options(@NotNull SASParser.Filename_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#var_statement}.
	 * @param ctx the parse tree
	 */
	void enterVar_statement(@NotNull SASParser.Var_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#var_statement}.
	 * @param ctx the parse tree
	 */
	void exitVar_statement(@NotNull SASParser.Var_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#contents_options}.
	 * @param ctx the parse tree
	 */
	void enterContents_options(@NotNull SASParser.Contents_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#contents_options}.
	 * @param ctx the parse tree
	 */
	void exitContents_options(@NotNull SASParser.Contents_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#key_reset_option}.
	 * @param ctx the parse tree
	 */
	void enterKey_reset_option(@NotNull SASParser.Key_reset_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#key_reset_option}.
	 * @param ctx the parse tree
	 */
	void exitKey_reset_option(@NotNull SASParser.Key_reset_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_sort_options}.
	 * @param ctx the parse tree
	 */
	void enterProc_sort_options(@NotNull SASParser.Proc_sort_optionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_sort_options}.
	 * @param ctx the parse tree
	 */
	void exitProc_sort_options(@NotNull SASParser.Proc_sort_optionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(@NotNull SASParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(@NotNull SASParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#merge_statement}.
	 * @param ctx the parse tree
	 */
	void enterMerge_statement(@NotNull SASParser.Merge_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#merge_statement}.
	 * @param ctx the parse tree
	 */
	void exitMerge_statement(@NotNull SASParser.Merge_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#update_statement}.
	 * @param ctx the parse tree
	 */
	void enterUpdate_statement(@NotNull SASParser.Update_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#update_statement}.
	 * @param ctx the parse tree
	 */
	void exitUpdate_statement(@NotNull SASParser.Update_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#array_elements}.
	 * @param ctx the parse tree
	 */
	void enterArray_elements(@NotNull SASParser.Array_elementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#array_elements}.
	 * @param ctx the parse tree
	 */
	void exitArray_elements(@NotNull SASParser.Array_elementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#tables_requests}.
	 * @param ctx the parse tree
	 */
	void enterTables_requests(@NotNull SASParser.Tables_requestsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#tables_requests}.
	 * @param ctx the parse tree
	 */
	void exitTables_requests(@NotNull SASParser.Tables_requestsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#repair_statement}.
	 * @param ctx the parse tree
	 */
	void enterRepair_statement(@NotNull SASParser.Repair_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#repair_statement}.
	 * @param ctx the parse tree
	 */
	void exitRepair_statement(@NotNull SASParser.Repair_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#rename_statement}.
	 * @param ctx the parse tree
	 */
	void enterRename_statement(@NotNull SASParser.Rename_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#rename_statement}.
	 * @param ctx the parse tree
	 */
	void exitRename_statement(@NotNull SASParser.Rename_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#constant_sublist}.
	 * @param ctx the parse tree
	 */
	void enterConstant_sublist(@NotNull SASParser.Constant_sublistContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#constant_sublist}.
	 * @param ctx the parse tree
	 */
	void exitConstant_sublist(@NotNull SASParser.Constant_sublistContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#comment_statement}.
	 * @param ctx the parse tree
	 */
	void enterComment_statement(@NotNull SASParser.Comment_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#comment_statement}.
	 * @param ctx the parse tree
	 */
	void exitComment_statement(@NotNull SASParser.Comment_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#infix_operator}.
	 * @param ctx the parse tree
	 */
	void enterInfix_operator(@NotNull SASParser.Infix_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#infix_operator}.
	 * @param ctx the parse tree
	 */
	void exitInfix_operator(@NotNull SASParser.Infix_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(@NotNull SASParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(@NotNull SASParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#exchange_statement}.
	 * @param ctx the parse tree
	 */
	void enterExchange_statement(@NotNull SASParser.Exchange_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#exchange_statement}.
	 * @param ctx the parse tree
	 */
	void exitExchange_statement(@NotNull SASParser.Exchange_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#modify_statements}.
	 * @param ctx the parse tree
	 */
	void enterModify_statements(@NotNull SASParser.Modify_statementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#modify_statements}.
	 * @param ctx the parse tree
	 */
	void exitModify_statements(@NotNull SASParser.Modify_statementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#label_part}.
	 * @param ctx the parse tree
	 */
	void enterLabel_part(@NotNull SASParser.Label_partContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#label_part}.
	 * @param ctx the parse tree
	 */
	void exitLabel_part(@NotNull SASParser.Label_partContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#on_update}.
	 * @param ctx the parse tree
	 */
	void enterOn_update(@NotNull SASParser.On_updateContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#on_update}.
	 * @param ctx the parse tree
	 */
	void exitOn_update(@NotNull SASParser.On_updateContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#value_statement}.
	 * @param ctx the parse tree
	 */
	void enterValue_statement(@NotNull SASParser.Value_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#value_statement}.
	 * @param ctx the parse tree
	 */
	void exitValue_statement(@NotNull SASParser.Value_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#proc_format_statement}.
	 * @param ctx the parse tree
	 */
	void enterProc_format_statement(@NotNull SASParser.Proc_format_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#proc_format_statement}.
	 * @param ctx the parse tree
	 */
	void exitProc_format_statement(@NotNull SASParser.Proc_format_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(@NotNull SASParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(@NotNull SASParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#class_statement}.
	 * @param ctx the parse tree
	 */
	void enterClass_statement(@NotNull SASParser.Class_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#class_statement}.
	 * @param ctx the parse tree
	 */
	void exitClass_statement(@NotNull SASParser.Class_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#arguments}.
	 * @param ctx the parse tree
	 */
	void enterArguments(@NotNull SASParser.ArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#arguments}.
	 * @param ctx the parse tree
	 */
	void exitArguments(@NotNull SASParser.ArgumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SASParser#output_dataset}.
	 * @param ctx the parse tree
	 */
	void enterOutput_dataset(@NotNull SASParser.Output_datasetContext ctx);
	/**
	 * Exit a parse tree produced by {@link SASParser#output_dataset}.
	 * @param ctx the parse tree
	 */
	void exitOutput_dataset(@NotNull SASParser.Output_datasetContext ctx);
}